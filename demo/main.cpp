#include <await/executors/impl/fibers/pool.hpp>
#include <await/timers/impl/queue.hpp>

#include <await/fibers/sched/yield.hpp>

#include <await/futures/make/execute.hpp>
#include <await/futures/make/after.hpp>

#include <await/futures/syntax/or.hpp>
#include <await/futures/syntax/bang.hpp>
#include <await/futures/syntax/sequence.hpp>

#include <await/futures/run/await.hpp>

#include <wheels/core/defer.hpp>

#include <fmt/core.h>

using namespace await;
using namespace std::chrono_literals;

int main() {
  // Runtime
  executors::fibers::Pool pool{/*threads=*/4};
  timers::Queue timers;

  {
    // NB: bang (!) operator
    auto loop = !futures::Execute(pool, []() -> int {
      wheels::Defer log([] {
        fmt::println("Cancelled");
      });

      while (true) {
        fmt::println("Spinning");
        fibers::Yield();  // I am a Fiber
      }

      std::abort();  // Unreachable
    });

    loop.IAmEager();

    futures::Future<int> auto compute = futures::After(timers.Delay(1s)) >>
                                        futures::Execute(pool, [] {
                                          return 42;
                                        });

    auto result = (std::move(loop) or std::move(compute)) | futures::Await();

    fmt::println("FirstOf -> {}", result.ExpectValue());
  }

  // Shutdown

  pool.WaitIdle();

  pool.Stop();
  timers.Stop();

  return 0;
}
