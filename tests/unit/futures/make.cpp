#include <await/futures/make/execute.hpp>
#include <await/futures/make/after.hpp>
#include <await/futures/make/value.hpp>
#include <await/futures/make/fail.hpp>
#include <await/futures/make/just.hpp>
#include <await/futures/make/never.hpp>
#include <await/futures/make/contract.hpp>

#include <await/futures/combine/seq/map.hpp>

#include <await/futures/run/unwrap.hpp>
#include <await/futures/run/go.hpp>

#include <await/futures/syntax/go.hpp>
#include <await/futures/syntax/bang.hpp>
#include <await/futures/syntax/unwrap.hpp>

#include <await/await.hpp>

#include <await/executors/impl/manual.hpp>
#include <await/timers/impl/queue.hpp>

#include <wheels/test/framework.hpp>

#include <thread>

using namespace await;

using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

static fallible::Error TimeoutError() {
  return fallible::Err(fallible::ErrorCodes::TimedOut)
      .Domain("Test")
      .Reason("Test")
      .Done();
}

//////////////////////////////////////////////////////////////////////

TEST_SUITE(FutureMake) {
  SIMPLE_TEST(Execute) {
    executors::ManualExecutor manual;

    {
      // Ok

      bool done = false;

      auto f = futures::Execute(manual, [&]() {
        done = true;
      });

      ~std::move(f);

      manual.Drain();

      ASSERT_TRUE(done);
    }

    {
      // Error

      auto f = !futures::Execute(manual, [&]() -> int {
        throw std::runtime_error("Test");
      });

      manual.Drain();

      auto result = std::move(f) | futures::Unwrap();

      ASSERT_TRUE(result.Failed());
    }
  }

  SIMPLE_TEST(Just) {
    {
      // Just

      auto f = !futures::Just();

      ASSERT_TRUE(f.IsValid());
      ASSERT_TRUE(f.HasResult());

      auto result = std::move(f) | futures::Unwrap();

      ASSERT_TRUE(result.IsOk());
    }

    {
      // JustValue

      auto f = !futures::Value<int64_t>(7);

      ASSERT_TRUE(f.HasResult());
      auto result = std::move(f) | futures::Unwrap();

      ASSERT_EQ(result.ExpectValue(), 7);
    }

    {
      // JustError

      auto f = !futures::Fail<int64_t>(TimeoutError());

      ASSERT_TRUE(f.HasResult());
      auto result = std::move(f) | futures::Unwrap();

      ASSERT_TRUE(result.Failed());
      auto err = result.Error();
      ASSERT_EQ(err.Code(), fallible::ErrorCodes::TimedOut);
    }

    {
      // Map

      auto f = futures::Just();

      bool done = false;

      std::move(f) |
          futures::Map([&done]() {
            done = true;
          }) |
          futures::Start() |
          futures::Go();

      ASSERT_TRUE(done);
    }
  }

  SIMPLE_TEST(After) {
    timers::Queue timers;

    auto after = futures::After(timers.Delay(1s));

    Await(std::move(after)).ExpectOk();
  }

  SIMPLE_TEST(Never) {
#if !__has_feature(address_sanitizer)
    // Memory leak
    auto never = !futures::Never();
    ASSERT_FALSE(never.HasResult());
#endif
  }

  SIMPLE_TEST(Contract) {
    {
      auto [f, p] = futures::Contract<int>();

      ASSERT_FALSE(f.HasResult());

      std::move(p).SetValue(1);

      ASSERT_TRUE(f.HasResult());
      auto result = std::move(f) | futures::Unwrap();
      ASSERT_EQ(*result, 1);
    }

    {
      auto [f, p] = futures::Contract<int>();

      ASSERT_FALSE(f.HasResult());

      std::move(p).SetError(TimeoutError());

      ASSERT_TRUE(f.HasResult());
      auto result = std::move(f) | futures::Unwrap();
      ASSERT_TRUE(result.Failed());
    }
  }
}
