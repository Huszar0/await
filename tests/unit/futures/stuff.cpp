#include <wheels/test/framework.hpp>
#include <wheels/test/util/cpu_timer.hpp>
#include <wheels/test/util/cpu_time_budget.hpp>

#include <await/futures/make/contract.hpp>
#include <await/futures/make/execute.hpp>

#include <await/futures/combine/seq/map.hpp>
#include <await/futures/combine/seq/recover.hpp>
#include <await/futures/combine/seq/flatten.hpp>
#include <await/futures/combine/seq/handle.hpp>

#include <await/futures/syntax/go.hpp>
#include <await/futures/syntax/unwrap.hpp>
#include <await/futures/syntax/bang.hpp>

#include <await/await.hpp>

#include <await/executors/impl/manual.hpp>
#include <await/executors/impl/thread_pool.hpp>
#include <await/executors/impl/strand.hpp>

#include <await/timers/impl/queue.hpp>

#include <thread>

using namespace await;

//////////////////////////////////////////////////////////////////////

static fallible::Error TimeoutError() {
  return fallible::Err(fallible::ErrorCodes::TimedOut)
      .Domain("Test")
      .Reason("Test")
      .Done();
}

//////////////////////////////////////////////////////////////////////

struct MoveOnly {
  explicit MoveOnly(std::string data_) : data(data_) {
  }

  MoveOnly(const MoveOnly& that) = delete;
  MoveOnly& operator=(const MoveOnly& that) = delete;

  MoveOnly(MoveOnly&& that) = default;

  std::string data;
};

struct NonDefaultConstructable {
  explicit NonDefaultConstructable(int v) : value(v) {
  }

  int value{0};
};

//////////////////////////////////////////////////////////////////////

TEST_SUITE(Eager) {
//  SIMPLE_TEST(Invalid) {
//    auto f = futures::EagerFuture<std::string>::Invalid();
//    ASSERT_FALSE(f.IsValid());
//  }

  SIMPLE_TEST(SetValue) {
    auto [f, p] = futures::Contract<std::string>();

    ASSERT_FALSE(f.HasResult());

    std::move(p).SetValue("Hello");

    ASSERT_TRUE(f.HasResult());

    auto result = *std::move(f);
    ASSERT_TRUE(result.IsOk());
    ASSERT_EQ(*result, "Hello");
  }

  SIMPLE_TEST(SetMoveOnlyValue) {
    auto [f, p] = futures::Contract<MoveOnly>();

    ASSERT_FALSE(f.HasResult());

    std::move(p).SetValue(MoveOnly{"Hello"});

    ASSERT_TRUE(f.HasResult());

    auto result = *std::move(f);
    ASSERT_TRUE(result.IsOk());
    ASSERT_EQ(result->data, "Hello");
  }

  SIMPLE_TEST(SetError) {
    auto [f, p] = futures::Contract<int>();

    ASSERT_FALSE(f.HasResult());

    std::move(p).SetError(TimeoutError());

    ASSERT_TRUE(f.HasResult());

    auto result = *std::move(f);
    ASSERT_TRUE(result.Failed());

    std::cout << "Error = " << result.Error().Describe() << std::endl;
  }

  SIMPLE_TEST(SetException) {
    auto [f, p] = futures::Contract<int>();

    try {
      throw std::runtime_error("Test");
    } catch (...) {
      std::move(p).SetError(
          fallible::Err(1)
              .Domain("Test")
              .Reason(wheels::CurrentExceptionMessage())
              .Done());
    }

    auto result = *std::move(f);

    ASSERT_TRUE(result.Failed());
    ASSERT_THROW(result.ValueOrThrow(), std::runtime_error);
  }

  SIMPLE_TEST(ExecuteValue) {
    executors::ManualExecutor manual;

    auto f = !futures::Execute(manual, []() {
      return 7;
    });

    manual.RunNext();

    ASSERT_TRUE(f.HasResult());

    auto result = *std::move(f);
    ASSERT_EQ(result.ExpectValue(), 7);
  }

  SIMPLE_TEST(ExecuteError) {
    executors::ManualExecutor manual;

    auto f = !futures::Execute(manual, []() {
      throw std::runtime_error("Test");
    });

    manual.RunNext();

    ASSERT_TRUE(f.HasResult());

    auto result = *std::move(f);

    ASSERT_TRUE(result.Failed());
    ASSERT_THROW(result.ThrowIfError(), std::runtime_error);
  }

  SIMPLE_TEST(BlockingGet1) {
    auto [f, p] = futures::Contract<int>();

    std::move(p).SetValue(3);

    auto result = Await(std::move(f));
    ASSERT_EQ(result.ExpectValue(), 3);
  }

  SIMPLE_TEST(BlockingGet2) {
    auto [f, p] = futures::Contract<int>();

    std::thread producer([p = std::move(p)]() mutable {
      std::this_thread::sleep_for(2s);
      std::move(p).SetValue(17);
    });

    {
      wheels::test::CpuTimeBudgetGuard cpu_time_budget{256ms};

      auto result = Await(std::move(f));
      ASSERT_EQ(result.ExpectValue(), 17);
    }

    producer.join();
  }

  SIMPLE_TEST(Map1) {
    auto [f, p] = futures::Contract<int>();

    auto g = std::move(f) | futures::Map([](int value) {
      return value + 1;
    }) | futures::Start();

    std::move(p).SetValue(3);

    auto result = *std::move(g);
    ASSERT_EQ(result.ExpectValue(), 4);
  }

  SIMPLE_TEST(Map2) {
    wheels::test::CpuTimeBudgetGuard cpu_time_budget{100ms};

    executors::ThreadPool pool{4};

    auto f1 = !futures::Execute(pool, []() {
      std::this_thread::sleep_for(1s);
      return 42;
    });

    auto f2 = std::move(f1) | futures::Map([](int value) {
      return value + 1;
    }) | futures::Start();

    ASSERT_EQ(Await(std::move(f2)).ExpectValue(), 43);

    pool.Stop();
  }

  SIMPLE_TEST(Map3) {
    wheels::test::CpuTimeBudgetGuard cpu_time_budget{100ms};

    executors::ThreadPool pool{4};

    auto [f, p] = futures::Contract<int>();

    std::move(p).SetValue(11);

    auto f2 = std::move(f) | futures::Via(pool) | futures::Map([&](int value) {
      ASSERT_TRUE(executors::ThreadPool::Current() == &pool);
      return value + 1;
    }) | futures::Start();

    int value = Await(std::move(f2)).ExpectValue();

    ASSERT_EQ(value, 12);

    pool.Stop();
  }

  SIMPLE_TEST(Map4) {
    auto [f, p] = futures::Contract<int>();

    auto g = std::move(f) | futures::Map([](int /*value*/) -> int {
      FAIL_TEST("Skip continuation if error");
      return 7;  // Make compiler happy
    }) | futures::Start();

    std::move(p).SetError(TimeoutError());

    auto result = *std::move(g);
    ASSERT_TRUE(result.Failed());
  }

  // Map chaining

  SIMPLE_TEST(Pipeline) {
    executors::ManualExecutor manual;

    bool done = false;

    futures::Execute(manual,
                     []() {
                       return 1;
                     })
         | futures::Map([](int value) {
          return value + 1;
        })
         | futures::Map([](int value) {
          return value + 2;
        })
         | futures::Map([](int value) {
          return value + 3;
        })
         | futures::Start()
         | futures::Map([&done](int value) {
          std::cout << "Value = " << value << std::endl;
          done = true;
        })
         | futures::Start()
         | futures::Go();

     manual.Drain();
    //ASSERT_EQ(tasks, 5);

    ASSERT_TRUE(done);
  }

  // Map chaining

  SIMPLE_TEST(PipelineError) {
    executors::ManualExecutor manual;

    bool done = false;

    futures::Execute(manual,
                     []() {
                       return 1;
                     })
         | futures::Map([](int) -> int {
          throw std::runtime_error("Fail");
        })
         | futures::Map([](int) -> int {
          std::abort();  // Skipped
        })
         | futures::Map([](int) -> int {
          std::abort();  // Skipped
        })
         | futures::Start()
         | futures::Map([&done](fallible::Result<int> result) {
          ASSERT_TRUE(result.Failed());
          done = true;
        })
         | futures::Start() | futures::Go();

    manual.Drain();
    // ASSERT_EQ(tasks, 5);

    ASSERT_TRUE(done);
  }

  // Map + Recover

  SIMPLE_TEST(PipelineRecover) {
    wheels::test::CpuTimeBudgetGuard cpu_time_budget{100ms};

    executors::ManualExecutor manual;

    bool done = false;

    futures::Execute(manual,
                     []() {
                       return 1;
                     })
         | futures::Map([](int) -> int {
          throw std::runtime_error("Fail");
        })
         | futures::Map([](int) -> int {
          std::abort();  // Skipped
        })
         | futures::Recover([](fallible::Error) {
          return fallible::Ok(7);
        })
         | futures::Map([](int value) {
          return value + 1;
        })
         | futures::Start()
         | futures::Map([&done](fallible::Result<int> result) {
          ASSERT_EQ(result.ExpectValue(), 8);
          done = true;
        })
         | futures::Start() | futures::Go();

    manual.Drain();
    // ASSERT_EQ(tasks, 6);

    ASSERT_TRUE(done);
  }

  // Via + Map

  SIMPLE_TEST(Via) {
    executors::ManualExecutor manual1;
    executors::ManualExecutor manual2;

    auto [f, p] = futures::Contract<int>();

    int step = 0;

    auto f2 = std::move(f)
                   | futures::Via(manual1)
                   | futures::Map([&](int value) {
                    step = 1;
                    return value + 1;
                  })
                   | futures::Map([&](int value) {
                    step = 2;
                    return value + 2;
                  })
                   | futures::Via(manual2)
                   | futures::Map([&](int value) {
                    step = 3;
                    return value + 3;
                  }) | futures::Start();

    // Launch pipeline
    std::move(p).SetValue(0);

    // ASSERT_EQ(manual1.Drain(), 2);
    manual1.Drain();
    ASSERT_EQ(step, 2);
    // ASSERT_EQ(manual2.Drain(), 1);
    manual2.Drain();
    ASSERT_EQ(step, 3);

    auto f3 = std::move(f2)
                   | futures::Map([&](int value) {
                    step = 4;
                    return value + 4;
                  })
                   | futures::Via(manual1)
                   | futures::Map([&](int value) {
                    step = 5;
                    return value + 5;
                  }) | futures::Start();

    // ASSERT_EQ(manual2.Drain(), 1);
    manual2.Drain();
    ASSERT_EQ(step, 4);

    // ASSERT_EQ(manual1.Drain(), 1);
    manual1.Drain();
    ASSERT_EQ(step, 5);

    ASSERT_TRUE(f3.HasResult());

    auto result = *std::move(f3);
    auto value = result.ExpectValue();

    ASSERT_EQ(value, 1 + 2 + 3 + 4 + 5);
  }

  SIMPLE_TEST(OnCancel) {
    auto [f, p] = futures::Contract<int>();

    bool handler_invoked = false;

    auto g = std::move(f) | futures::OnCancel([&]() {
      handler_invoked = true;
    }) | futures::Start();

    std::move(p).Cancel();

    ASSERT_TRUE(g.IsCancelled());
    ASSERT_TRUE(handler_invoked);
  }
}
