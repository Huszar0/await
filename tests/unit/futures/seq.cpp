#include <wheels/test/framework.hpp>

#include <await/fibers/sched/yield.hpp>
#include <await/fibers/sync/wait_group.hpp>

#include <await/executors/impl/manual.hpp>
#include <await/executors/impl/fibers/pool.hpp>
#include <await/executors/impl/thread_pool.hpp>

#include <await/futures/make/execute.hpp>
#include <await/futures/make/value.hpp>
#include <await/futures/make/fail.hpp>
#include <await/futures/make/just.hpp>

#include <await/futures/combine/seq/map.hpp>
#include <await/futures/combine/seq/recover.hpp>
#include <await/futures/combine/seq/flatten.hpp>
#include <await/futures/combine/seq/box.hpp>
#include <await/futures/combine/seq/yield.hpp>
#include <await/futures/combine/seq/anyway.hpp>
#include <await/futures/combine/seq/clone.hpp>

#include <await/futures/run/unwrap.hpp>
#include <await/futures/run/go.hpp>

#include <await/futures/syntax/go.hpp>
#include <await/futures/syntax/unwrap.hpp>

#include <await/await.hpp>

#include <carry/new.hpp>
#include <carry/current.hpp>

#include <exception>
#include <vector>

using namespace await;

TEST_SUITE(FutureSeq) {
  SIMPLE_TEST(JustWorks) {
    auto l = futures::Just() | futures::Map([]() {
        return 42;
    });

    auto result = *std::move(l);

    ASSERT_TRUE(result.IsOk());
    ASSERT_EQ(*result, 42);
  }

  SIMPLE_TEST(IsValid) {
    auto l2 = futures::Value(7);

    auto result = *std::move(l2);

    ASSERT_TRUE(result.IsOk());
    ASSERT_EQ(*result, 7);
  }

  SIMPLE_TEST(MoveCtor) {
    auto l1 = futures::Value(7);

    auto l2 = std::move(l1);

    auto result = *std::move(l2);

    ASSERT_TRUE(result.IsOk());
    ASSERT_EQ(*result, 7);
  }

  SIMPLE_TEST(MoveAssignmentOperator) {
    auto l1 = futures::Value(7) | futures::Box();

    auto l2 = futures::Value(8) | futures::Box();

    l2 = std::move(l1);

    auto result = *std::move(l2);

    ASSERT_TRUE(result.IsOk());
    ASSERT_EQ(*result, 7);
  }

  SIMPLE_TEST(ToEager) {
    auto l = futures::Just() |
             futures::Map([]() { return 42; });

    auto f = std::move(l) | futures::Start();

    ASSERT_TRUE(f.HasResult());
    ASSERT_EQ((*std::move(f)).ExpectValue(), 42);
  }

  SIMPLE_TEST(ToEagerToLazy) {
    // Lazy<Thunk>
    auto l = futures::Invoke([]() {
        return 42;
    });

    // Eager<int>
    auto f = std::move(l) | futures::Start();

    ASSERT_TRUE(f.HasResult());

    futures::BoxedFuture<int> boxed = std::move(f) | futures::Box();
  }

//  SIMPLE_TEST(OperatorEager) {
//    futures::EagerFuture<int> f = futures::Value(17);
//
//    ASSERT_TRUE(f.HasResult());
//  }

//  SIMPLE_TEST(OperatorLazy) {
//    auto f = !futures::Invoke([]() {
//        return 42;
//      });
//
//    futures::BoxedFuture<int> l = std::move(f);
//  }

  SIMPLE_TEST(ActuallyLazy) {
    executors::ManualExecutor manual;

    // Prepare
    auto l = futures::Just() |
             futures::Via(manual) |
             futures::Map([]() { return 42; });

    // Empty
    ASSERT_EQ(manual.Drain(), 0);

    // Start
    auto f = std::move(l) | futures::Start();

    // Run
    ASSERT_EQ(manual.Drain(), 1);

    // Check
    ASSERT_EQ((*std::move(f)).ExpectValue(), 42);
  }

  SIMPLE_TEST(Context) {
    auto ctx = carry::New().SetString("key", "test").Done();

    auto l = futures::Just() |
             futures::With(std::move(ctx)) |
             futures::Map([]() {
               auto ctx = carry::Current();
               ASSERT_EQ(ctx.GetString("key"), "test");
               return 1;
             });

    {
      auto l_ctx = l.UserContext();
      ASSERT_EQ(l_ctx.GetString("key"), "test");
    }

    auto result = *std::move(l);

    ASSERT_EQ(result.ExpectValue(), 1);
  }

  SIMPLE_TEST(Mappers) {
    {
      // ValueMapper

      auto l = futures::Value(1) |
               futures::Map([](int value) { return value + 1; });

      auto result = *std::move(l);

      ASSERT_EQ(*result, 2);
    }

    {
      // FaultyMapper

      auto l = futures::Value(1) | futures::Map([](int value) {
        return fallible::Ok(value + 2);
      });

      auto result = *std::move(l);
      ASSERT_EQ(*result, 3);
    }

    {
      // ResultMapper

      auto l = futures::Value(7) | futures::Map([](fallible::Result<int> input) {
        return input;
      });

      auto result = *std::move(l);
      ASSERT_EQ(*result, 7);
    }

    {
      // ValueEater

      bool done = false;
      auto l = futures::Value(7) | futures::Map([&](int value) {
        done = true;
        std::cout << "Eat " << value << std::endl;
      });

      auto status = *std::move(l);

      ASSERT_TRUE(status.IsOk());
      ASSERT_TRUE(done);
    }

    {
      // ResultEater

      bool done = false;
      auto l = futures::Value(7) | futures::Map([&](fallible::Result<int> result) {
        done = true;
        std::cout << "Eat " << result.ExpectValue() << std::endl;
      });

      auto status = *std::move(l);

      ASSERT_TRUE(status.IsOk());
      ASSERT_TRUE(done);
    }

    {
      // VoidMapper

      auto l = futures::Just() | futures::Map([]() {
        return 7;
      });

      auto result = *std::move(l);

      ASSERT_TRUE(result.IsOk());
      ASSERT_EQ(*result, 7);
    }

    {
      // Worker (Success)

      bool done = false;

      auto l = futures::Just() |
               futures::Map([&done]() { done = true; });

      ASSERT_FALSE(done);

      fallible::Status status = *std::move(l);

      ASSERT_TRUE(done);
      ASSERT_TRUE(status.IsOk());
    }
  }

  fallible::Error TestError() {
    return fallible::Err(1).Domain("Test").Reason("Test").Done();
  }

  SIMPLE_TEST(Recover) {
    {
      auto l = futures::Value(7) |
               futures::Recover([](fallible::Error) {
                 return fallible::Ok<int>(2);
               });

      auto result = *std::move(l);

      ASSERT_TRUE(result.IsOk());
      ASSERT_EQ(*result, 7);
    }

    {
      auto l = futures::Fail<int>(TestError()) |
          futures::Recover([](fallible::Error) {
                 return fallible::Ok<int>(2);
               });

      auto result = *std::move(l);

      ASSERT_TRUE(result.IsOk());
      ASSERT_EQ(*result, 2);
    }

    {
      auto l = futures::Fail<int>(TestError()) |
          futures::Map([](int value) {
            return value + 1;  // Ignored
          }) |
          futures::Recover([](fallible::Error) {
            return fallible::Ok<int>(2);
          });

      auto result = *std::move(l);

      ASSERT_TRUE(result.IsOk());
      ASSERT_EQ(*result, 2);
    }
  }

  SIMPLE_TEST(OnComplete) {
    auto f = futures::Value(7) |
             futures::OnComplete([](const fallible::Result<int> f) {
               ASSERT_TRUE(f.IsOk());
             }) |
             futures::Unwrap();
  }

  SIMPLE_TEST(OnCancel) {

  }

  SIMPLE_TEST(Anyway) {
    {
      // Value

      executors::ManualExecutor manual;

      bool ok = false;

      futures::Execute(manual, []() { return 3; }) |
          futures::Anyway([&]() { ok = true; }) |
          futures::Go();

      manual.Drain();

      ASSERT_TRUE(ok);
    }

    {
      // Error

      executors::ManualExecutor manual;

      bool ok = false;

      futures::Fail<int>(TestError()) |
          futures::Anyway([&]() { ok = true; }) |
          futures::Go();

      manual.Drain();

      ASSERT_TRUE(ok);
    }

    {
      // Error

      executors::ManualExecutor manual;

      bool ok = false;

      auto l = futures::Execute(manual, []() { return 3; }) |
               futures::Anyway([&]() { ok = true; });

      auto f = std::move(l) | futures::Start();
      f.RequestCancel();

      manual.Drain();

      ASSERT_TRUE(ok);
    }
  }

  SIMPLE_TEST(BoxedLazy) {
    auto l = futures::Value(1);

    futures::BoxedFuture<int> lf = std::move(l) | futures::Box();

    auto result = *std::move(lf);

    ASSERT_TRUE(result.IsOk());
    ASSERT_EQ(*result, 1);
  }

  SIMPLE_TEST(Go) {
    executors::ManualExecutor manual;

    int output = 0;

    futures::Just() |
        futures::Via(manual) |
        futures::Map([]() { return 1; }) |
        futures::Map([&](int value) {
          output = value;
          return true;
        }) |
        futures::Go();

    ASSERT_EQ(output, 0);

    manual.Drain();

    ASSERT_EQ(output, 1);
  }

  SIMPLE_TEST(WaitGroup) {
    executors::fibers::Pool pool{4};

    auto f = futures::Execute(pool, []() {
      fibers::WaitGroup wg;

      wg.Add(futures::Spawn([]() {
        while (true) {
          fibers::Yield();
        }
      }));

      wg.Wait();

      FAIL_TEST("Not cancelled");
    }) | futures::Start();

    std::move(f).RequestCancel();

    pool.WaitIdle();
    pool.Stop();
  }

  SIMPLE_TEST(BangOperator) {
    executors::ManualExecutor manual;

    auto lazy = futures::Value(1) |
                futures::Via(manual) |
                futures::Map([](int value) {
                  return value + 1;
                });

    ASSERT_EQ(manual.Drain(), 0);

    // Bang!
    auto future = std::move(lazy) |
                  futures::Map([](int value) { return value + 1; }) |
                  futures::Start();

    ASSERT_TRUE(manual.Drain() > 0);

    ASSERT_TRUE(future.HasResult());

    auto result = std::move(future) | futures::Unwrap();

    ASSERT_TRUE(result.IsOk());
    ASSERT_EQ(*result, 3);
  }

  SIMPLE_TEST(GoOperator) {
    executors::ManualExecutor manual;

    futures::Execute(manual, []() { return 1; }) |
        futures::Map([](int value) {
          std::cout << "Value = " << value << std::endl;
        }) |
        futures::Go();

    ASSERT_EQ(manual.Drain(), 2);
  }

  SIMPLE_TEST(Inline) {
    executors::ManualExecutor manual;

    int step = 0;

    futures::Execute(manual, [&]() {
      step = 1;
    }) | futures::Inline() | futures::Map([&]() {
      step = 2;
    }) | futures::Map([&]() {
      step = 3;
    }) | futures::Via(manual)
       | futures::Map([&]() {
      step = 4;
    }) | futures::Map([&]() {
      step = 5;
    }) | futures::Go();

    manual.RunAtMost(1);

    ASSERT_EQ(step, 3);

    ASSERT_EQ(manual.RunAtMost(1), 1);
    ASSERT_EQ(step, 4);

    manual.Drain();
    ASSERT_EQ(step, 5);
  }

  SIMPLE_TEST(Yield) {
    executors::pools::fast::ThreadPool pool{4};

    futures::Execute(pool, [](){}) | futures::Yield() | futures::Map([](){}) | futures::Go();

    pool.WaitIdle();
    pool.Stop();
  }

  SIMPLE_TEST(Flatten) {
    auto exe = futures::Invoke([]() {
      return futures::Spawn([]() {
        return 7;
      });
    });

    auto flat = std::move(exe) | futures::Flatten();

    auto result = *std::move(flat);

    ASSERT_TRUE(result.IsOk());
    ASSERT_EQ(*result, 7);
  }

  SIMPLE_TEST(ErrorFlatMap) {
    auto err = futures::Fail<int>(TestError());

    auto and_then = std::move(err) | futures::Map([](int) {
      std::abort();  // Unexpected
      return futures::Fail<int>(TestError());
    });

    auto f = std::move(and_then) |
             futures::Flatten() |
             futures::Map([](int) {
               std::abort();  // Unexpected
             });

    ASSERT_TRUE(Await(std::move(f)).Failed());
  }

  SIMPLE_TEST(ExecuteInline) {
    bool done = false;

    auto exe = futures::Invoke([&]() { done = true; });

    ASSERT_FALSE(done);

    auto f = std::move(exe) | futures::Start();

    ASSERT_TRUE(done);
  }

//  SIMPLE_TEST(EagerOperatorBox) {
//    futures::EagerFuture<int> f = futures::Invoke([](){ return 1; });
//    {
//      futures::Future<int> l = std::move(f);
//    }
//  }

  SIMPLE_TEST(FlatLifetime) {
    executors::fibers::Pool pool{4};

    auto l = futures::Execute(pool, []() {
      std::string data = "FlatLifetime";

      return futures::Spawn([data]() {
        std::cout << data << std::endl;
      }) | futures::Box();

    }) | futures::Flatten();

    Await(std::move(l)).ExpectOk();

    pool.Stop();
  }

  SIMPLE_TEST(AutoBoxing) {
    futures::BoxedFuture<int> f = futures::Value(7);
  }

  SIMPLE_TEST(FailAutoBoxing) {
    futures::BoxedFuture<int> f = futures::Fail(
        fallible::errors::TimedOut());
  }

  SIMPLE_TEST(Fork) {
    executors::ThreadPool pool{2};

    auto [l, r] = futures::Execute(pool, [] {
      return 42;
    }) | futures::Clone();

    ASSERT_EQ(Await(std::move(l)).ExpectValue(), 42);
    ASSERT_EQ(Await(std::move(r)).ExpectValue(), 42);

    pool.Stop();
  }
}
