#include <await/futures/make/execute.hpp>
#include <await/futures/make/after.hpp>

#include <await/futures/make/contract.hpp>

#include <await/futures/combine/par/all.hpp>
#include <await/futures/combine/par/join.hpp>
#include <await/futures/combine/par/first_of.hpp>
#include <await/futures/combine/par/quorum.hpp>
#include <await/futures/combine/par/interrupt.hpp>

#include <await/futures/combine/seq/map.hpp>

#include <await/futures/run/unwrap.hpp>
#include <await/futures/run/go.hpp>

#include <await/futures/syntax/unwrap.hpp>
#include <await/futures/syntax/bang.hpp>
#include <await/futures/syntax/go.hpp>

#include <await/await.hpp>

#include <await/executors/impl/manual.hpp>

#include <await/timers/impl/queue.hpp>

#include <wheels/test/framework.hpp>

using namespace await;

//////////////////////////////////////////////////////////////////////

static fallible::Error TimeoutError() {
  return fallible::Err(fallible::ErrorCodes::TimedOut)
      .Domain("Test")
      .Reason("Test")
      .Done();
}

//////////////////////////////////////////////////////////////////////

struct MoveOnly {
  explicit MoveOnly(std::string data_) : data(data_) {
  }

  MoveOnly(const MoveOnly& that) = delete;
  MoveOnly& operator=(const MoveOnly& that) = delete;

  MoveOnly(MoveOnly&& that) = default;

  std::string data;
};

struct NonDefaultConstructable {
  explicit NonDefaultConstructable(int v) : value(v) {
  }

  int value{0};
};

//////////////////////////////////////////////////////////////////////

TEST_SUITE(FuturePar) {
  SIMPLE_TEST(All1) {
    executors::ManualExecutor manual;

    auto l1 = futures::Execute(manual, []() {
      return 3;
    });

    auto l2 = futures::Execute(manual, []() {
      return 2;
    });

    auto l3 = futures::Execute(manual, []() {
      return 1;
    });

    auto all = !futures::All(std::move(l1), std::move(l2), std::move(l3));

    ASSERT_FALSE(all.HasResult());

    manual.RunAtMost(2);

    // Still not completed
    ASSERT_FALSE(all.HasResult());

    manual.Drain();

    ASSERT_TRUE(all.HasResult());

    auto all_result = *std::move(all);
    ASSERT_TRUE(all_result.IsOk());

    ASSERT_EQ(*all_result, std::vector<int>({3, 2, 1}));
  }

  SIMPLE_TEST(All2) {
    auto [f1, p1] = futures::Contract<int>();
    auto [f2, p2] = futures::Contract<int>();

    auto all = !futures::All(std::move(f1), std::move(f2));

    ASSERT_FALSE(all.HasResult());

    // First error
    std::move(p2).SetError(TimeoutError());

    ASSERT_TRUE(all.HasResult());

    // Second error
    std::move(p1).SetError(TimeoutError());

    ASSERT_TRUE(all.HasResult());

    auto all_result = *std::move(all);
    ASSERT_TRUE(all_result.Failed());
  }

  SIMPLE_TEST(AllNonDefaultConstructable) {
    auto [f1, p1] = futures::Contract<NonDefaultConstructable>();
    auto [f2, p2] = futures::Contract<NonDefaultConstructable>();

    auto all = !futures::All(std::move(f1), std::move(f2));

    ASSERT_FALSE(all.HasResult());

    std::move(p1).SetValue(NonDefaultConstructable{1});

    ASSERT_FALSE(all.HasResult());

    std::move(p2).SetValue(NonDefaultConstructable{2});

    ASSERT_TRUE(all.HasResult());

    auto all_result = *std::move(all);
    ASSERT_TRUE(all_result.IsOk());

    auto all_values = std::move(*all_result);

    ASSERT_EQ(all_values[0].value, 1);
    ASSERT_EQ(all_values[1].value, 2);
  }

  SIMPLE_TEST(AllEmptyInput) {
    std::vector<futures::BoxedFuture<int>> inputs;
    auto all = !futures::All(std::move(inputs));

    ASSERT_TRUE(all.HasResult());
    ASSERT_TRUE((*std::move(all)).IsOk());
  }

  SIMPLE_TEST(FirstOf1) {
    auto [f1, p1] = futures::Contract<int>();
    auto [f2, p2] = futures::Contract<int>();

    auto first_of = !futures::FirstOf(std::move(f1), std::move(f2));

    ASSERT_FALSE(first_of.HasResult());

    std::move(p2).SetValue(13);

    ASSERT_TRUE(first_of.HasResult());

    std::move(p1).SetError(TimeoutError());

    auto first_result = *std::move(first_of);

    ASSERT_EQ(first_result.ExpectValue(), 13);
  }

  SIMPLE_TEST(FirstOf2) {
    auto [f1, p1] = futures::Contract<int>();
    auto [f2, p2] = futures::Contract<int>();
    auto [f3, p3] = futures::Contract<int>();

    auto first_of = !futures::FirstOf(std::move(f1), std::move(f2), std::move(f3));

    ASSERT_FALSE(first_of.HasResult());

    std::move(p1).SetError(TimeoutError());

    ASSERT_FALSE(first_of.HasResult());

    std::move(p3).SetError(TimeoutError());

    ASSERT_FALSE(first_of.HasResult());

    std::move(p2).SetValue(44);

    ASSERT_TRUE(first_of.HasResult());

    auto first_result = *std::move(first_of);

    ASSERT_EQ(first_result.ExpectValue(), 44);
  }

  SIMPLE_TEST(FirstOfNonDefaultConstructable) {
    auto [f1, p1] = futures::Contract<NonDefaultConstructable>();
    auto [f2, p2] = futures::Contract<NonDefaultConstructable>();

    auto first_of = !futures::FirstOf(std::move(f1), std::move(f2));

    ASSERT_FALSE(first_of.HasResult());

    std::move(p2).SetValue(NonDefaultConstructable{2});

    ASSERT_TRUE(first_of.HasResult());

    std::move(p1).SetValue(NonDefaultConstructable{1});

    ASSERT_TRUE(first_of.HasResult());

    auto first_result = *std::move(first_of);
    ASSERT_TRUE(first_result.IsOk());
    ASSERT_EQ(first_result->value, 2);
  }

  SIMPLE_TEST(Join) {
    auto [f1, p1] = futures::Contract<int>();
    auto [f2, p2] = futures::Contract<int>();

    auto join = !futures::Join(std::move(f1), std::move(f2));

    ASSERT_FALSE(join.HasResult());

    std::move(p1).SetValue(1);
    ASSERT_FALSE(join.HasResult());

    std::move(p2).SetValue(2);
    ASSERT_TRUE(join.HasResult());

    (*std::move(join)).ExpectValue();
  }

  SIMPLE_TEST(JoinError) {
    auto [f1, p1] = futures::Contract<int>();
    auto [f2, p2] = futures::Contract<int>();

    auto join = !futures::Join(std::move(f1), std::move(f2));

    ASSERT_FALSE(join.HasResult());

    std::move(p2).SetError(TimeoutError());
    ASSERT_TRUE(join.HasResult());

    auto result = *std::move(join);
    ASSERT_TRUE(result.Failed());
  }

  SIMPLE_TEST(JoinMixedInputs) {
    auto f1 = futures::Invoke([]() {
      return 7;
    });

    auto [f2, p2] = futures::Contract<std::string>();

    auto join = !futures::Join(std::move(f1), std::move(f2));

    ASSERT_FALSE(join.HasResult());

    std::move(p2).SetValue("Done");

    ASSERT_TRUE(join.HasResult());
  }

  SIMPLE_TEST(JoinEagerVector) {
    auto [f1, p1] = futures::Contract<int>();
    auto [f2, p2] = futures::Contract<int>();

    std::vector<futures::EagerFuture<int>> inputs;
    inputs.push_back(std::move(f1));
    inputs.push_back(std::move(f2));

    auto join = !futures::Join(std::move(inputs));

    std::move(p2).SetValue(2);

    ASSERT_FALSE(join.HasResult());

    std::move(p1).SetValue(1);

    ASSERT_TRUE(join.HasResult());
  }

  SIMPLE_TEST(WithInterrupt) {
    timers::Queue tk;

    auto timeout_error = fallible::Err(123).Domain("Test").Reason("Timed out").Done();

    {
      // Value
      auto timeout = futures::After(tk.Delay(2s));
      auto value = futures::After(tk.Delay(1s)) | futures::Map([]() {
        return 42;
      });

      auto with_timeout = futures::Interrupt(std::move(value), std::move(timeout), timeout_error);

      auto result = Await(std::move(with_timeout));

      ASSERT_TRUE(result.IsOk());
      ASSERT_EQ(result.ValueOrThrow(), 42);
    }

    {
      // Timeout
      auto timeout = futures::After(tk.Delay(1s));
      auto value = futures::After(tk.Delay(2s)) | futures::Map([]() {
        return 42;
      });

      auto with_timeout = futures::Interrupt(std::move(value), std::move(timeout), timeout_error);

      auto result = Await(std::move(with_timeout));

      ASSERT_FALSE(result.IsOk());

      auto error = result.Error();
      ASSERT_EQ(error.Code(), timeout_error.Code());
    }

    tk.Stop();
  }

  SIMPLE_TEST(Quorum) {
    std::vector<futures::Promise<int>> promises;
    std::vector<futures::EagerFuture<int>> inputs;

    for (size_t i = 0; i < 5; ++i) {
      auto [f, p] = futures::Contract<int>();
      promises.push_back(std::move(p));
      inputs.push_back(std::move(f));
    }

    auto quorum = !futures::Quorum(std::move(inputs), 3);

    // 1/3
    std::move(promises[2]).SetValue(17);
    // 2/3
    std::move(promises[4]).SetValue(42);

    ASSERT_FALSE(quorum.HasResult());

    // 3/3
    std::move(promises[0]).SetValue(256);

    ASSERT_TRUE(quorum.HasResult());

    // > threshold
    std::move(promises[1]).SetValue(13);

    auto q_result = *std::move(quorum);
    ASSERT_TRUE(q_result.IsOk());

    auto q_values = *q_result;

    ASSERT_EQ(q_values.size(), 3);
    ASSERT_EQ(q_values[0], 17);
    ASSERT_EQ(q_values[1], 42);
    ASSERT_EQ(q_values[2], 256);
  }

  SIMPLE_TEST(MixCombinators) {
    auto [f1, p1] = futures::Contract<int>();
    auto [f2, p2] = futures::Contract<int>();
    auto [f3, p3] = futures::Contract<int>();

    auto first_of_12 = futures::FirstOf(std::move(f1), std::move(f2));
    auto f = !futures::All(std::move(first_of_12), std::move(f3));

    std::move(p2).SetError(TimeoutError());
    std::move(p3).SetValue(3);

    ASSERT_FALSE(f.HasResult());

    std::move(p1).SetValue(1);

    ASSERT_TRUE(f.HasResult());
  }
}
