#include <wheels/test/framework.hpp>

#include <await/executors/impl/fibers/manual.hpp>

#include <await/await.hpp>

#include <await/futures/make/execute.hpp>
#include <await/futures/make/value.hpp>
#include <await/futures/syntax/go.hpp>
#include <await/futures/syntax/bang.hpp>

// Internals
#include <await/fibers/core/suspend_guard.hpp>

using namespace await;

TEST_SUITE(FutureAwait) {
  SIMPLE_TEST(EagerFastPath) {
    return;  // TODO: fix optimization

    executors::fibers::ManualExecutor manual;

    ~futures::Execute(manual, []() {
      auto f = !futures::Value(7);

      ASSERT_TRUE(f.HasResult());

      {
        fibers::SuspendGuard guard;

        int value = Await(std::move(f)).ExpectValue();
        ASSERT_EQ(value, 7);

        ASSERT_FALSE(guard.HasBeenSuspended());
      }
    });

    manual.Drain();
  }
}