#include <wheels/test/framework.hpp>

#include <await/executors/impl/thread_pool.hpp>

#include <await/fibers/boot/go.hpp>
#include <await/fibers/sync/event.hpp>

#include <atomic>

using namespace await;

TEST_SUITE(OneShotEvent) {
  SIMPLE_TEST(JustWorks) {
    executors::ThreadPool scheduler{4};

    bool ok = false;

    fibers::Go(scheduler, [&ok]() {
      fibers::OneShotEvent event;

      fibers::Go([&ok, &event]() {
        ok = true;
        event.Fire();
      });

      event.Wait();
      ASSERT_TRUE(ok);
    });

    scheduler.WaitIdle();

    ASSERT_TRUE(ok);

    scheduler.Stop();
  }

  SIMPLE_TEST(MultiWaiters) {
    executors::ThreadPool scheduler{4};

    bool ok = false;
    std::atomic<size_t> waiters{0};
    fibers::OneShotEvent event;

    static const size_t kWaiters = 7;

    for (size_t i = 0; i < kWaiters; ++i) {
      fibers::Go(scheduler, [&]() {
        event.Wait();
        ASSERT_TRUE(ok);
        ++waiters;
      });
    }

    fibers::Go(scheduler, [&]() {
      ok = true;
      event.Fire();
    });

    scheduler.WaitIdle();

    ASSERT_TRUE(ok);
    ASSERT_EQ(waiters.load(), kWaiters);

    scheduler.Stop();
  }
}
