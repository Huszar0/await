#include <wheels/test/framework.hpp>
#include <wheels/test/util/cpu_timer.hpp>

#include <await/executors/impl/thread_pool.hpp>

#include <await/fibers/boot/go.hpp>
#include <await/fibers/sync/mutex.hpp>

#include <atomic>
#include <chrono>
#include <thread>

using namespace await;
using namespace std::chrono_literals;

TEST_SUITE(Mutex) {
  SIMPLE_TEST(Counter) {
    executors::ThreadPool scheduler{4};

    fibers::Mutex mutex;
    size_t cs = 0;

    for (size_t i = 0; i < 10; ++i) {
      fibers::Go(scheduler, [&]() {
        for (size_t j = 0; j < 1024; ++j) {
          std::lock_guard guard(mutex);
          ++cs;
        }
      });
    }

    scheduler.WaitIdle();

    ASSERT_EQ(cs, 1024 * 10);

    scheduler.Stop();
  }

  SIMPLE_TEST(Blocking) {
    executors::ThreadPool scheduler{4};

    fibers::Mutex mutex;

    wheels::ProcessCPUTimer timer;

    fibers::Go(scheduler, [&]() {
      mutex.Lock();
      std::this_thread::sleep_for(1s);
      mutex.Unlock();
    });

    fibers::Go(scheduler, [&]() {
      mutex.Lock();
      mutex.Unlock();
    });

    scheduler.WaitIdle();

    ASSERT_TRUE(timer.Elapsed() < 100ms);

    scheduler.Stop();
  }

  TEST(UnlockFork, wheels::test::TestOptions().TimeLimit(5s)) {
    executors::ThreadPool scheduler{4};

    fibers::Mutex mutex;

    fibers::Go(scheduler, [&]() {
      mutex.Lock();
      std::this_thread::sleep_for(1s);
      mutex.Unlock();
    });

    std::this_thread::sleep_for(128ms);

    for (size_t i = 0; i < 3; ++i) {
      fibers::Go(scheduler, [&]() {
        mutex.Lock();
        mutex.Unlock();
        std::this_thread::sleep_for(2s);
      });
    }

    scheduler.WaitIdle();
    scheduler.Stop();
  }
}
