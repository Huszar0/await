#include <await/executors/impl/thread_pool.hpp>

#include <await/fibers/boot/go.hpp>
#include <await/fibers/sync/semaphore.hpp>

#include <wheels/test/framework.hpp>

#include <atomic>
#include <thread>

using namespace await;

TEST_SUITE(Semaphore) {
  SIMPLE_TEST(DoNotSuspend) {
    executors::ThreadPool scheduler{/*threads=*/4};

    fibers::Semaphore sem{1};

    fibers::Go(scheduler, [&]() {
      ASSERT_TRUE(sem.TryAcquire());
      ASSERT_FALSE(sem.TryAcquire());
      sem.Release();

      ASSERT_TRUE(sem.TryAcquire());
      sem.Release();

      sem.Acquire();
      sem.Release();
    });

    scheduler.WaitIdle();

    scheduler.Stop();
  }

  SIMPLE_TEST(Suspend) {
    executors::ThreadPool scheduler{/*threads=*/4};

    fibers::Semaphore sem{0};

    fibers::Go(scheduler, [&]() {
      std::this_thread::sleep_for(1s);
      sem.Release();
    });

    bool done = false;

    fibers::Go(scheduler, [&]() {
      sem.Acquire();
      done = true;
    });

    scheduler.WaitIdle();

    ASSERT_TRUE(done);

    scheduler.Stop();
  }
}
