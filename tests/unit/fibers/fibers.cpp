#include <await/fibers/boot/go.hpp>
#include <await/fibers/sched/yield.hpp>
#include <await/fibers/sched/teleport.hpp>

#include <await/fibers/sync/mutex.hpp>
#include <await/fibers/sync/condvar.hpp>

#include <await/executors/impl/thread_pool.hpp>
#include <await/executors/impl/strand.hpp>
#include <await/executors/impl/inline.hpp>
#include <await/executors/impl/manual.hpp>

#include <carry/new.hpp>

#include <wheels/test/framework.hpp>

#include <chrono>

using namespace await::executors;
using namespace await::fibers;
using namespace std::chrono_literals;
using wheels::test::TestOptions;

template <typename F>
void RunScheduler(size_t threads, F init) {
  ThreadPool scheduler{threads};

  Go(scheduler, std::move(init));

  scheduler.WaitIdle();
  scheduler.Stop();
}

TEST_SUITE(Fibers) {
  SIMPLE_TEST(HelloWorld) {
    ThreadPool tp{1};

    Go(tp, []() {
      std::cout << "Hello!" << std::endl;
    });

    tp.WaitIdle();
    tp.Stop();
  }

  SIMPLE_TEST(Spawns) {
    bool done{false};

    ThreadPool scheduler{2};

    Go(scheduler, [&]() {
      // Spawn in current executor
      Go([&]() {
        done = true;
      });
    });

    scheduler.WaitIdle();

    ASSERT_TRUE(done);

    scheduler.Stop();
  }

  TEST(Mutex, TestOptions().TimeLimit(30s)) {
    static const size_t kThreads = 4;
    static const size_t kFibers = 512;
    static const size_t kIncrements = 1024;

    ThreadPool thread_pool{kThreads};

    Mutex mutex;
    std::atomic<size_t> shared_counter = 0;

    for (size_t i = 0; i < kFibers; ++i) {
      Go(thread_pool, [&shared_counter, &mutex]() {
        for (size_t k = 0; k < kIncrements; ++k) {
          {
            auto guard = mutex.Guard();
            shared_counter.store(shared_counter.load() + 1);
          }
          if (k % 17 == 0) {
            Yield();
          }
        }
      });
    }

    thread_pool.WaitIdle();

    std::cout << "Shared counter value: " << shared_counter.load() << std::endl;
    std::cout << "Increments made: " << kFibers * kIncrements << std::endl;

    ASSERT_EQ(shared_counter.load(), kFibers * kIncrements);

    thread_pool.Stop();
  }

  SIMPLE_TEST(MutexTryLock) {
    RunScheduler(1, []() {
      Mutex mutex;

      ASSERT_TRUE(mutex.TryLock());
      mutex.Unlock();

      mutex.Lock();
      ASSERT_FALSE(mutex.TryLock());
      mutex.Unlock();
    });
  }

//  SIMPLE_TEST(FiberHandleInvalid) {
//    auto h = FiberHandle::Invalid();
//    ASSERT_TRUE(!h.IsValid());
//  }

  SIMPLE_TEST(Id) {
    RunScheduler(1, []() {
      FiberId id = self::GetId();
      ASSERT_EQ(self::GetId(), id);  // Still the same

      Go([parent_id = id]() {
        ASSERT_NE(self::GetId(), parent_id);  // Different ids
      });
    });
  }

  SIMPLE_TEST(Names) {
    RunScheduler(1, []() {
      ASSERT_FALSE(self::GetName().has_value());
      self::SetName("tester");
      ASSERT_EQ(*self::GetName(), "tester");
    });
  }

  SIMPLE_TEST(TeleportGuard) {
    ThreadPool tp1{3};
    ThreadPool tp2{3};

    Go(tp1, [&]() {
      {
        TeleportGuard t(tp2);
        std::cout << "Teleported to tp2" << std::endl;
      }
      std::cout << "Teleported back to tp1" << std::endl;
    });

    tp1.WaitIdle();
    tp2.WaitIdle();
    tp1.WaitIdle();

    tp1.Stop();
    tp2.Stop();
  }

  SIMPLE_TEST(Manual) {
    ManualExecutor manual;

    auto routine = []() {
      Yield();
      Yield();
      Yield();
    };

    Go(manual, routine);
    Go(manual, routine);

    size_t steps = manual.RunAtMost(100500);

    ASSERT_EQ(steps, 8);
  }
}

TEST_SUITE(CondVar) {
  SIMPLE_TEST(RoundRobin) {
    static const size_t kThreads = 3;

    ThreadPool thread_pool{kThreads};

    static const size_t kSteps = 1024;
    static const size_t kFibers = 5;

    Mutex mutex;
    CondVar cond;
    size_t turn = 0;
    size_t steps = 0;

    auto routine = [&](size_t i) {
      for (size_t k = 0; k < kSteps; ++k) {
        std::unique_lock lock(mutex);

        while (turn != i) {
          cond.Wait(lock);
        }

        ASSERT_TRUE(turn == i);
        ++steps;
        turn = (turn + 1) % kFibers;
        cond.NotifyAll();
      }
    };

    for (size_t i = 0; i < kFibers; ++i) {
      Go(thread_pool, [routine, i]() {
        routine(i);
      });
    }

    thread_pool.WaitIdle();

    ASSERT_EQ(steps, kSteps * kFibers);

    thread_pool.Stop();
  }
}
