#include <wheels/test/framework.hpp>

#include <await/futures/make/contract.hpp>
#include <await/futures/make/execute.hpp>
#include <await/futures/make/never.hpp>

#include <await/futures/combine/par/first_of.hpp>
#include <await/futures/combine/par/all.hpp>
#include <await/futures/combine/par/join.hpp>
#include <await/futures/combine/par/interrupt.hpp>

#include <await/futures/combine/seq/map.hpp>
#include <await/futures/combine/seq/start.hpp>
#include <await/futures/combine/seq/clone.hpp>
#include <await/futures/combine/seq/flatten.hpp>

#include <await/futures/run/unwrap.hpp>
#include <await/futures/run/go.hpp>

#include <await/futures/syntax/bang.hpp>
#include <await/futures/syntax/go.hpp>

#include <await/executors/impl/thread_pool.hpp>
#include <await/executors/impl/inline.hpp>
#include <await/executors/impl/manual.hpp>
#include <await/executors/impl/fibers/manual.hpp>

#include <await/executors/task/checkpoint.hpp>

#include <await/cancel/current.hpp>

#include <await/fibers/sched/yield.hpp>

#include <await/await.hpp>

#include <thread>

using namespace await;

//////////////////////////////////////////////////////////////////////

fallible::Error TestError() {
  return fallible::Err(1).Domain("Tests").Reason("Test").Done();
}

//////////////////////////////////////////////////////////////////////

TEST_SUITE(CancelFutures) {
  SIMPLE_TEST(PromiseCancel) {
    auto [f, p] = futures::Contract<int>();

    auto g = std::move(f) | futures::Map([](int value) {
      std::abort();
      return value + 1;
    }) | futures::Start();

    auto h = std::move(g) | futures::Map([](int value) {
      return value + 1;
    }) | futures::Start();

    std::move(p).Cancel();

    ASSERT_TRUE(h.IsCancelled());
  }

  SIMPLE_TEST(WaitResult) {
    auto [f, p] = futures::Contract<int>();

    std::thread producer([p = std::move(p)]() mutable {
      std::move(p).Cancel();
    });

    ASSERT_THROW(Await(std::move(f)).ValueOrThrow(), executors::task::CancelledException);

    producer.join();
  }

  SIMPLE_TEST(FutureRequestCancel) {
    auto [f, p] = futures::Contract<int>();

    auto g = std::move(f) | futures::Map([](int value) {
      std::abort();
      return value + 1;
    }) | futures::Map([](int value) {
      return value + 1;
    }) | futures::Start();

    std::move(g).RequestCancel();

    p.CancelSubscribe(executors::Inline(), []() {
      std::cout << "Cancel requested!" << std::endl;
    });

    ASSERT_TRUE(p.CancelRequested());

    std::move(p).Cancel();
  }

  struct Widget {
    ~Widget() {
      std::cout << "~Widget" << std::endl;
    }
  };

  SIMPLE_TEST(DropCancelHandlers) {
    executors::fibers::ManualExecutor manual;

    auto [f, p] = futures::Contract<int>();

    p.CancelSubscribe(manual, []() {
      std::abort();
    });

    std::move(p).SetValue(7);

    std::move(f).RequestCancel();

    manual.Drain();
  }

  SIMPLE_TEST(BeforeMap) {
    executors::ManualExecutor manual;

    auto f = futures::Execute(manual, []() {}) |
             futures::Map([]() {}) |
             futures::Map([]() { FAIL_TEST("Running"); }) |
             futures::Start();

    manual.RunAtMost(1);

    std::move(f).RequestCancel();

    manual.Drain();
  }

  SIMPLE_TEST(FirstOf) {
    auto [f1, p1] = futures::Contract<int>();
    auto [f2, p2] = futures::Contract<int>();

    auto first_of = !futures::FirstOf(std::move(f1), std::move(f2));

    std::move(p2).SetValue(17);

    ASSERT_TRUE(first_of.HasResult());
    ASSERT_TRUE(p1.CancelRequested());
  }

  SIMPLE_TEST(FirstOfMap) {
    executors::fibers::ManualExecutor manual;

    auto [f1, p1] = futures::Contract<int>();
    auto [f2, p2] = futures::Contract<int>();

    auto first_of = futures::FirstOf(std::move(f1), std::move(f2));

    auto f = std::move(first_of) | futures::Via(manual) | futures::Map([](int value) {
      executors::task::Checkpoint();
      std::cout << "Map!" << std::endl;
      return value;
    }) | futures::Start();

    std::move(p2).SetValue(17);

    manual.Drain();

    ASSERT_TRUE(f.HasResult());
    ASSERT_TRUE(p1.CancelRequested());
  }

  SIMPLE_TEST(All) {
    auto [f1, p1] = futures::Contract<int>();
    auto [f2, p2] = futures::Contract<int>();

    auto all = !futures::All(std::move(f1), std::move(f2));

    std::move(p2).SetError(TestError());

    ASSERT_TRUE(all.HasResult());
    ASSERT_TRUE(p1.CancelRequested());
  }

  SIMPLE_TEST(Join) {
    auto [f1, p1] = futures::Contract<int>();
    auto [f2, p2] = futures::Contract<int>();

    auto join = !futures::Join(std::move(f1), std::move(f2));

    std::move(p2).SetError(TestError());

    ASSERT_TRUE(join.HasResult());
    ASSERT_TRUE(p1.CancelRequested());
  }

  SIMPLE_TEST(Interrupt) {
    auto [f, p] = futures::Contract<int>();

    p.CancelSubscribe(executors::Inline(), []() {
      std::cout << "Interrupted!" << std::endl;
    });

    // Interrupt
    auto [fi, pi] = futures::Contract<wheels::Unit>();

    auto g = !futures::Interrupt(std::move(f), std::move(fi), TestError());

    std::move(pi).SetValue({});

    ASSERT_TRUE(p.CancelRequested());
  }

  SIMPLE_TEST(Await1) {
    executors::fibers::ManualExecutor manual;

    auto [f, p] = futures::Contract<int>();

    auto g = !futures::Execute(manual, [f = std::move(f)]() mutable {
      Await(std::move(f)).ExpectOk();
    });

    manual.Drain();

    ASSERT_TRUE(!g.HasResult() && !g.IsCancelled());

    std::move(p).Cancel();

    manual.Drain();

    ASSERT_TRUE(g.IsCancelled());
  }

  SIMPLE_TEST(Await2) {
    executors::fibers::ManualExecutor manual;

    futures::Execute(manual, [&]() {
      {
        std::cout << "Step 1" << std::endl;

        auto f = futures::Execute(manual, []() -> int {
          return 1;
        });
        std::cout << Await(std::move(f)).ExpectValue() << std::endl;
      }  // <-- state for `f` has been destroyed

      {
        std::cout << "Step 2" << std::endl;

        auto f = futures::Execute(manual, []() -> int {
          return 2;
        });
        std::cout << Await(std::move(f)).ExpectValue() << std::endl;
      }  // <-- state for `f` has been destroyed
    }) | futures::Detach();

    manual.Drain();
  }

  SIMPLE_TEST(ThreadAwait) {
    executors::ThreadPool pool{4};

    auto f = !futures::Execute(pool, []() {

      auto inner = futures::Spawn([]() {
        while (true) {
          executors::task::Checkpoint();
        }
      });

      Await(std::move(inner)).ExpectOk();
    });

    std::move(f).RequestCancel();

    pool.WaitIdle();
    pool.Stop();
  }

  SIMPLE_TEST(PromiseDtor) {
    executors::fibers::ManualExecutor manual;

    auto [f, p] = futures::Contract<int>();

    auto g = !futures::Execute(manual, [p = std::move(p)]() {
      while (true) {
        std::cout << "Hi!" << std::endl;
        fibers::Yield();
      }
    });

    manual.RunAtMost(7);

    std::move(g).RequestCancel();

    manual.Drain();
  }

  SIMPLE_TEST(Flatten) {
    executors::ManualExecutor manual;

    auto f = futures::Execute(manual, [&]() {
      return futures::Execute(manual, []() {
        while (true) {
          executors::task::Checkpoint();
        }
      });
    });

    auto g = std::move(f) | futures::Flatten() | futures::Start();

    std::move(g).RequestCancel();

    manual.Drain();
  }

//  SIMPLE_TEST(CancelOnMoveAssignment) {
//    executors::fibers::ManualExecutor manual;
//
//    auto f = !futures::Execute(manual, []() -> int {
//      while (true) {
//        fibers::Yield();
//      }
//    });
//
//    // Cancel first task
//    f = !futures::Execute(manual, []() {
//      return 7;
//    });
//
//    manual.Drain();
//  }

  SIMPLE_TEST(Fork) {
    auto [input, p] = futures::Contract<int>();

    auto [left, right] = std::move(input) | futures::Clone();

    std::move(left).RequestCancel();

    auto righte = std::move(right) | futures::Start();

    ASSERT_TRUE(p.CancelRequested());
    std::move(p).Cancel();

    {
      ASSERT_TRUE(righte.IsCancelled());
    }
  }

  SIMPLE_TEST(ExecuteFork) {
    executors::fibers::ManualExecutor manual;

    auto f = futures::Execute(manual, []() {
      while (true) {
        fibers::Yield();
      }
    }) | futures::Map([](){}) |
             futures::Start() |
             futures::Map([](){}) |
             futures::Start();

    auto [l, r] = std::move(f) | futures::Clone();

    std::move(r).RequestCancel();

    auto le = std::move(l) | futures::Start();

    manual.Drain();

    ASSERT_TRUE(le.IsCancelled());
  }

  SIMPLE_TEST(GoFork) {
    executors::fibers::ManualExecutor manual;

    auto f = futures::Execute(manual, []() {
      while (true) {
        fibers::Yield();
      }
    }) | futures::Map([](){}) |
             futures::Start() |
             futures::Map([](){}) |
             futures::Start();

    auto [l, r] = std::move(f) | futures::Clone();

    std::move(l) | futures::Map([](){}) | futures::Go();

    manual.RunAtMost(6);

    std::move(r).RequestCancel();

    manual.Drain();
  }

  SIMPLE_TEST(DropAsLazy) {
    executors::fibers::ManualExecutor manual;

    auto f = !futures::Execute(manual, []() {
      while (true) {
        fibers::Yield();
      }
    });

    f.IAmEager();

    {
      auto l = std::move(f) | futures::Map([](){});
      // f.IAmEager();
    }

    manual.Drain();
  }

  SIMPLE_TEST(DropBoxed) {
    executors::fibers::ManualExecutor manual;

    auto f = !futures::Execute(manual, []() {
      while (true) {
        fibers::Yield();
      }
    });

    f.IAmEager();

    {
      auto l = std::move(f) | futures::Box();
    }

    manual.Drain();
  }

  SIMPLE_TEST(AllReset) {
    executors::fibers::ManualExecutor manual;

    auto f = !futures::Execute(manual, []() {
      while (true) {
        fibers::Yield();
      }
    });

    auto g = futures::Execute(manual, []() {
      auto h = futures::Spawn([]() {
        return 42;
      });
      ASSERT_FALSE(cancel::Current().HasLinks());
      Await(std::move(h)).ExpectOk();
    });

    auto all = !futures::All(std::move(f), std::move(g));

    size_t steps = manual.RunAtMost(11);
    ASSERT_EQ(steps, 11);

    std::move(all).RequestCancel();

    manual.Drain();
  }

  SIMPLE_TEST(FirstOfReset) {
    executors::fibers::ManualExecutor manual;

    auto f = !futures::Execute(manual, []() {
      while (true) {
        fibers::Yield();
      }
    });

    auto g = futures::Execute(manual, []() {
      auto h = futures::Spawn([]() {
        throw std::runtime_error("Test");
      });
      Await(std::move(h)).ValueOrThrow();
    });

    auto first = !futures::FirstOf(std::move(f), std::move(g));

    ASSERT_EQ(manual.RunAtMost(11), 11);

    std::move(first).RequestCancel();

    manual.Drain();
  }

  SIMPLE_TEST(ResetCancelTokenAfterEagerFuture) {
    executors::fibers::ManualExecutor manual;

    auto f = !futures::Execute(manual, [](){});

    auto g = std::move(f) | futures::Map([]() {
      ASSERT_FALSE(cancel::Current().HasLinks());

      auto h = !futures::Spawn([](){});
      Await(std::move(h)).ExpectOk();
    }) | futures::Box();

    ~std::move(g);

    manual.Drain();
  }

  SIMPLE_TEST(Never) {
    executors::fibers::ManualExecutor manual;

    bool cancelled = false;

    auto f = futures::Execute(manual, [&]() {
      wheels::Defer defer([&] { cancelled = true; });

      auto never = futures::Never();
      Await(std::move(never)).ExpectOk();
    }) | futures::Start();

    manual.Drain();

    f.RequestCancel();

    manual.Drain();

    ASSERT_TRUE(cancelled);
  }
}
