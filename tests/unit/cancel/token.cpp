#include <wheels/test/framework.hpp>

#include <await/cancel/token.hpp>
#include <await/cancel/never.hpp>
#include <await/cancel/manual.hpp>

#include <await/executors/impl/inline.hpp>

#include <optional>

using namespace await;

TEST_SUITE(CancelToken) {
//  SIMPLE_TEST(Source) {
//    cancel::Source cancel_source;
//
//    auto t1 = cancel_source.MakeToken();
//    ASSERT_TRUE(t1.Cancellable());
//    ASSERT_FALSE(t1.CancelRequested());
//
//    cancel_source.RequestCancel();
//    ASSERT_TRUE(t1.CancelRequested());
//
//    // New token
//    auto t2 = cancel_source.MakeToken();
//    ASSERT_TRUE(t2.CancelRequested());
//  }
//
//  SIMPLE_TEST(Never) {
//    auto t1 = cancel::Never();
//    ASSERT_FALSE(t1.Cancellable());
//    ASSERT_FALSE(t1.CancelRequested());
//
//    auto t2 = cancel::Never();
//    ASSERT_FALSE(t2.CancelRequested());
//  }
//
//  SIMPLE_TEST(Link) {
//    cancel::Source s1;
//    cancel::Source s2;
//    cancel::Source s3;
//
//    auto t1 = s1.MakeToken();
//    auto t2 = s2.MakeToken();
//    auto t3 = s3.MakeToken();
//
//    // s1 -> t1
//    // s2 -> t2
//    // s3 -> t3
//
//    t1.Link(std::move(s2));
//    t2.Link(std::move(s3));
//
//    // s1 -> t1 -Link-> s2 -> t2 -Link-> s3 -> t3
//
//    ASSERT_FALSE(t3.CancelRequested());
//
//    s1.RequestCancel();
//
//    ASSERT_TRUE(t3.CancelRequested());
//  }
//
//  SIMPLE_TEST(ManualLifetime) {
//    cancel::ManualLifetimeState<> manual_state;
//
//    manual_state.Allocate();
//
//    auto source = manual_state.AsSource();
//    auto token = manual_state.MakeToken();
//
//    ASSERT_FALSE(token.CancelRequested());
//
//    source.RequestCancel();
//
//    ASSERT_TRUE(token.CancelRequested());
//
//  }
//
//  SIMPLE_TEST(Subscribe) {
//    cancel::Source s;
//
//    auto t = s.MakeToken();
//
//    bool handler_called = false;
//
//    t.Subscribe(executors::Inline(), [&handler_called]() {
//      std::cout << "Cancel requested!" << std::endl;
//      handler_called = true;
//    });
//
//    s.RequestCancel();
//
//    ASSERT_TRUE(handler_called);
//  }
}
