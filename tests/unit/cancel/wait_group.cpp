#include <wheels/test/framework.hpp>

#include <await/fibers/sched/yield.hpp>
#include <await/fibers/sync/wait_group.hpp>

#include <await/executors/impl/fibers/pool.hpp>

#include <await/futures/make/execute.hpp>
#include <await/futures/syntax/bang.hpp>

#include <await/await.hpp>

#include <exception>

using namespace await;

TEST_SUITE(CancelWaitGroup) {
  SIMPLE_TEST(JustWait) {
    executors::fibers::Pool pool{4};

    auto join = futures::Execute(pool, []() {
      fibers::WaitGroup wg;

      for (size_t i = 0; i < 4; ++i) {
        wg.Add(futures::Spawn([]() {
          for (size_t j = 0; j < 3; ++j) {
            fibers::Yield();
          }
          return 42;
        }));
      }

      wg.Wait();
    });

    Await(std::move(join)).ExpectOk();

    pool.Stop();
  }

  SIMPLE_TEST(RequestCancel) {
    executors::fibers::Pool pool{4};

    auto join = futures::Execute(pool, []() {
      fibers::WaitGroup wg;

      wg.Add(futures::Spawn([]() {
        while (true) {
          fibers::Yield();
        }
      }));

      wg.RequestCancel();
      wg.Wait();
    });

    Await(std::move(join)).ExpectOk();

    pool.Stop();
  }

  SIMPLE_TEST(SiblingError) {
    executors::fibers::Pool pool{4};

    auto join = futures::Execute(pool, []() {
      fibers::WaitGroup wg;

      wg.Add(futures::Spawn([]() {
        while (true) {
          fibers::Yield();
        }
      }));

      wg.Add(futures::Spawn([]() {
        throw std::runtime_error("Test");
      }));

      wg.Wait();
    });

    Await(std::move(join)).ExpectOk();

    pool.Stop();
  }

  SIMPLE_TEST(PropagateCancel) {
    executors::fibers::Pool pool{4};

    auto f = !futures::Execute(pool, []() {
      fibers::WaitGroup wg;

      wg.Add(futures::Spawn([]() {
        while (true) {
          fibers::Yield();
        }
      }));

      wg.Wait();

      FAIL_TEST("Not cancelled");
    });

    std::move(f).RequestCancel();

    pool.WaitIdle();
    pool.Stop();
  }
}
