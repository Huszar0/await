ProjectLog("Unit tests")

# List sources

file(GLOB_RECURSE TEST_SOURCES "./*.cpp")

# All tests target

add_executable(await_unit_tests ${TEST_SOURCES})
target_link_libraries(await_unit_tests await)
