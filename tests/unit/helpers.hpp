#pragma once

#include <wheels/test/framework.hpp>

#include <await/executors/impl/thread_pool.hpp>

#include <chrono>
#include <ctime>
#include <atomic>
#include <thread>

namespace test_helpers {

////////////////////////////////////////////////////////////////////////////////

inline void ExpectPool(await::executors::ThreadPool& pool) {
  WHEELS_VERIFY(await::executors::ThreadPool::Current() == &pool,
                "Unexpected execution context");
}

////////////////////////////////////////////////////////////////////////////////

class StopWatch {
  using Clock = std::chrono::steady_clock;
  using TimePoint = std::chrono::time_point<Clock>;
  using Duration = std::chrono::nanoseconds;

 public:
  StopWatch() : start_time_(Now()) {
  }

  Duration Elapsed() const {
    return Now() - start_time_;
  }

  Duration Restart() {
    auto elapsed = Elapsed();
    start_time_ = Now();
    return elapsed;
  }

 private:
  static TimePoint Now() {
    return Clock::now();
  }

 private:
  TimePoint start_time_;
};

////////////////////////////////////////////////////////////////////////////////

class CPUTimeMeter {
 public:
  CPUTimeMeter() {
    start_clocks_ = std::clock();
  }

  double UsageSeconds() const {
    return ClocksToSeconds(std::clock() - start_clocks_);
  }

 private:
  static double ClocksToSeconds(size_t clocks) {
    return 1.0 * clocks / CLOCKS_PER_SEC;
  }

 private:
  std::clock_t start_clocks_;
};

#if __has_feature(thread_sanitizer) || __has_feature(address_sanitizer)

class CPUTimeBudgetGuard {
 public:
  CPUTimeBudgetGuard(double /*limit*/) {
  }
};

#else

class CPUTimeBudgetGuard {
 public:
  CPUTimeBudgetGuard(double limit) : limit_(limit) {
  }

  ~CPUTimeBudgetGuard() {
    auto usage = meter_.UsageSeconds();
    std::cout << "CPU usage: " << usage << " seconds" << std::endl;
    ASSERT_TRUE(usage <= limit_);
  }

 private:
  CPUTimeMeter meter_;
  double limit_;
};

#endif

////////////////////////////////////////////////////////////////////////////////

class OnePassBarrier {
 public:
  OnePassBarrier(size_t threads) : total_(threads) {
  }

  void Pass() {
    arrived_.fetch_add(1);
    while (arrived_.load() < total_) {
      std::this_thread::yield();
    }
  }

 private:
  size_t total_{0};
  std::atomic<size_t> arrived_{0};
};

}  // namespace test_helpers