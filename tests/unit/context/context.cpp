#include <wheels/test/framework.hpp>

#include <carry/empty.hpp>
#include <carry/wrap.hpp>
#include <carry/new.hpp>
#include <carry/mutable.hpp>
#include <carry/current.hpp>
#include <carry/scope.hpp>

#include <await/executors/impl/thread_pool.hpp>
#include <await/executors/impl/manual.hpp>

#include <await/futures/make/execute.hpp>
#include <await/futures/make/just.hpp>
#include <await/futures/make/value.hpp>

#include <await/futures/combine/seq/map.hpp>
#include <await/futures/combine/seq/start.hpp>
#include <await/futures/combine/seq/flatten.hpp>

#include <await/futures/run/go.hpp>

#include <await/futures/syntax/unwrap.hpp>

#include <await/await.hpp>

#include <optional>

using namespace await;

TEST_SUITE(Context) {
  SIMPLE_TEST(Entries) {
    auto ctx = carry::New()
                   .SetInt64("test", 123)
                   .SetString("id", "12345")
                   .Done();

    ASSERT_EQ(ctx.Get<int64_t>("test"), 123);
    ASSERT_EQ(ctx.Get<std::string>("id"), "12345");

    ASSERT_FALSE(ctx.TryGet<int64_t>("missing"));

    ASSERT_EQ(ctx.GetOr<std::string>("missing", "or"), "or");
  }

  SIMPLE_TEST(DefaultCtor) {
    carry::Context ctx;
    ASSERT_FALSE(ctx.TryGet<std::string>("key"));
  }

  SIMPLE_TEST(Copy) {
    auto ctx1 = carry::New().SetInt64("test", 123).Done();

    auto ctx2 = ctx1;
    ASSERT_EQ(ctx2.Get<int64_t>("test"), 123);
  }

  SIMPLE_TEST(Wrap) {
    auto ctx1 = carry::New()
                    .SetInt64("test", 123)
                    .Done();

    auto ctx2 = carry::Wrap(ctx1)
                    .SetInt64("test", 456)
                    .SetInt64("y", 7)
                    .Done();

    ASSERT_EQ(ctx1.Get<int64_t>("test"), 123);
    ASSERT_EQ(ctx2.Get<int64_t>("test"), 456);
    ASSERT_EQ(ctx2.Get<int64_t>("y"), 7);
    ASSERT_FALSE(ctx1.TryGet<int64_t>("y"));
  }

  SIMPLE_TEST(WrapImplicitDone) {
    auto ctx1 = carry::Empty();

    carry::Context ctx2 = carry::Wrap(ctx1).SetString("key", "test");

    ASSERT_EQ(ctx2.GetString("key"), "test");
  }

  SIMPLE_TEST(Wrapper) {
    auto ctx1 = carry::New()
                    .SetInt64("test", 123)
                    .Done();

    carry::Wrapper wrapper(ctx1);
    wrapper.SetInt64("test", 456);
    wrapper.SetInt64("port", 80).SetString("name", "test");

    auto ctx2 = wrapper.Done();

    ASSERT_EQ(ctx2.GetInt64("test"), 456);
    ASSERT_EQ(ctx2.GetInt64("port"), 80);
    ASSERT_EQ(ctx2.GetString("name"), "test")
  }

  SIMPLE_TEST(Builder) {
    carry::Builder builder;

    builder.SetString("key", "value");
    builder.SetInt64("port", 80).SetBool("flag", true);

    auto ctx = builder.Done();

    ASSERT_EQ(ctx.GetString("key"), "value");
    ASSERT_EQ(ctx.GetInt64("port"), 80);
    ASSERT_EQ(ctx.GetBool("flag"), true);
  }

  SIMPLE_TEST(GetPanics) {
    auto ctx = carry::New().SetInt64("key", 123).Done();
    // ctx.GetUInt64("key");  // Panics
  }

  SIMPLE_TEST(JustViaWithMap) {
    executors::ManualExecutor manual;

    auto ctx = carry::New().SetInt64("key", 123).Done();

    auto l = futures::Just() |
             futures::Via(manual) |
             futures::With(ctx) |
             futures::Map([]() {
      auto ctx = carry::Current();

      ASSERT_EQ(ctx.GetInt64("key"), 123);
    });

    auto l_ctx = l.UserContext();

    ASSERT_EQ(l_ctx.GetInt64("key"), 123);

    manual.Drain();
  }

  SIMPLE_TEST(WithUpdate) {
    auto ctx = carry::New().SetInt64("key", 123).Done();

    auto l1 = futures::Just() |
              futures::With(ctx) |
              futures::Map([]() {
      auto ctx = carry::Current();

      ASSERT_EQ(ctx.GetInt64("key"), 123);
    });

    auto l2 = std::move(l1) | futures::With<int64_t>("key", 456) | futures::Map([]() {
      auto ctx = carry::Current();

      ASSERT_EQ(ctx.GetInt64("key"), 456);
    });

    (*std::move(l2)).ExpectOk();
  }

  SIMPLE_TEST(Scope) {
    executors::ThreadPool pool{4};

    auto ctx1 = carry::New().SetString("key", "v1").Done();

    futures::Just() | futures::Via(pool) | futures::With(ctx1) | futures::Map([&]() {
      {
        auto curr = carry::Current();
        ASSERT_EQ(curr.Get<std::string>("key"), "v1");
      }

      {
        auto ctx2 = carry::Wrap(ctx1).SetString("key", "v2").Done();

        carry::Scope scope1(ctx2);

        {
          auto curr = carry::Current();
          ASSERT_EQ(curr.Get<std::string>("key"), "v2");
        }

        {
          carry::WrapScope<int64_t> scope2("test", 7);

          auto curr = carry::Current();
          ASSERT_EQ(curr.Get<int64_t>("test"), 7);

          std::cout << curr.Get<int64_t>("test") << std::endl;
        }
      }

      {
        auto curr = carry::Current();
        ASSERT_EQ(curr.Get<std::string>("key"), "v1");
      }
    }) | futures::Go();

    pool.WaitIdle();
    pool.Stop();
  }

  SIMPLE_TEST(Execute) {
    executors::ManualExecutor manual;

    auto ctx = carry::New().SetInt64("key", 123).Done();

    bool done = false;

    futures::Just() | futures::Via(manual) | futures::With(ctx) | futures::Map([&]() {
      futures::Spawn([&]() {
        // Inherit context from parent task
        auto ctx = carry::Current();

        std::cout << "key -> " << ctx.GetInt64("key") << std::endl;
        ASSERT_EQ(ctx.GetInt64("key"), 123);

        done = true;
      }) | futures::Go();
    }) | futures::Go();

    manual.Drain();

    ASSERT_TRUE(done);
  }

  SIMPLE_TEST(PropagateWithFutures) {
    auto ctx = carry::New().SetInt64("key", 123).Done();

    auto just = futures::Value(7) | futures::With(ctx);

    auto f = std::move(just) |
                 futures::Map([](int value) {
                   return value + 1;
                 }) |
                 futures::Map([](int value) {
                   return value + 1;
                 }) |
                 futures::Start();

    ASSERT_TRUE(f.IsValid());

    auto f_ctx = f.UserContext();

    std::cout << f_ctx.Get<int64_t>("key") << std::endl;

    ASSERT_EQ(f_ctx.Get<int64_t>("key"), 123);
  }

  SIMPLE_TEST(CollectKeys) {
    auto ctx1 = carry::New()
                    .SetString("header.timeout", "100500")
                    .SetString("header.trace_id", "abc-123")
                    .SetString("key1", "value1")
                    .Done();

    auto ctx2 = carry::Wrap(ctx1)
                    .SetString("header.timeout", "1000")
                    .SetString("key2", "value2")
                    .Done();

    auto headers = ctx2.CollectKeys("header.");

    ASSERT_EQ(headers.size(), 2);

    std::set<std::string> expected_headers;
    expected_headers.insert("header.timeout");
    expected_headers.insert("header.trace_id");

    ASSERT_EQ(headers, expected_headers);
  }

  SIMPLE_TEST(Mutable) {
    carry::MutableContext mut_ctx;
    auto ctx = mut_ctx.View();

    mut_ctx.Set<std::string>("key1", "value1");
    mut_ctx.Set<int64_t>("key2", 100500);

    {
      ASSERT_EQ(ctx.Get<std::string>("key1"), "value1");
      ASSERT_EQ(ctx.Get<int64_t>("key2"), 100500);

      ASSERT_FALSE(ctx.TryGet<bool>("key3"));
    }

    mut_ctx.Set<bool>("key3", true);

    ASSERT_EQ(ctx.GetBool("key3"), true);
  }

  SIMPLE_TEST(FlatMap) {
    auto f = futures::Invoke([]() {
      return futures::Just() | futures::With<int>("key", 7);
    }) | futures::Flatten();

    auto ctx = f.UserContext();

    (*std::move(f)).ExpectOk();

    ASSERT_TRUE(ctx.TryGet<int>("key"));
  }
}
