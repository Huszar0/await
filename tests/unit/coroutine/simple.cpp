#include "simple.hpp"

#include <twist/ed/local/ptr.hpp>

#include <wheels/core/assert.hpp>
#include <wheels/core/defer.hpp>

namespace await {

// static thread_local SimpleCoroutine* current = nullptr;
static twist::ed::ThreadLocalPtr<SimpleCoroutine> current;

SimpleCoroutine::SimpleCoroutine(std::function<void()> fun)
    : fun_(fun),
      stack_(AllocateStack()),
      impl_(stack_.MutView(), this, nullptr) {
}

void SimpleCoroutine::Resume() {
  SimpleCoroutine* prev = current;
  current = this;

  wheels::Defer rollback([prev]() {
    current = prev;
  });

  impl_.Resume();
}

void SimpleCoroutine::Suspend() {
  WHEELS_VERIFY(current, "Not in coroutine");
  current->impl_.Suspend();
}

// Intentionally naive and inefficient
sure::Stack SimpleCoroutine::AllocateStack() {
  return sure::Stack::AllocateBytes(/*at_least=*/64 * 1024);
}

}  // namespace await
