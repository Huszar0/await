#include <await/fibers/core/coroutine.hpp>

#include <sure/stack.hpp>

#include <functional>

namespace await {

// Standalone coroutine, for tests

class SimpleCoroutine : private fibers::IRunnable {
 public:
  explicit SimpleCoroutine(std::function<void()> fun);

  void Resume();

  // Suspend current coroutine
  static void Suspend();

  bool IsCompleted() {
    return impl_.IsCompleted();
  }

 private:
  // IRunnable
  void RunCoro(void*) noexcept override {
    fun_();
  }

 private:
  static sure::Stack AllocateStack();

 private:
  std::function<void()> fun_;
  sure::Stack stack_;
  fibers::Coroutine impl_;
};

}  // namespace await
