#include <await/executors/impl/thread_pool.hpp>
#include <await/executors/impl/strand.hpp>
#include <await/executors/impl/manual.hpp>
#include <await/executors/boot/submit.hpp>

#include <wheels/test/framework.hpp>
#include <wheels/core/stop_watch.hpp>

#include <thread>
#include <deque>
#include <atomic>

using namespace await::executors;
using namespace std::chrono_literals;

void ExpectThreadPool(ThreadPool& pool) {
  ASSERT_EQ(ThreadPool::Current(), &pool);
}

TEST_SUITE(Strand) {
  SIMPLE_TEST(JustWorks) {
    ThreadPool pool{4};
    Strand strand{pool};

    bool done = false;

    Submit(strand, [&]() {
      done = true;
    });

    pool.WaitIdle();

    ASSERT_TRUE(done);

    pool.Stop();
  }

  SIMPLE_TEST(Decorator) {
    ThreadPool pool{4};
    Strand strand(pool);

    bool done{false};

    for (size_t i = 0; i < 128; ++i) {
      Submit(strand, [&]() {
        ExpectThreadPool(pool);
        done = true;
      });
    }

    pool.WaitIdle();

    ASSERT_TRUE(done);

    pool.Stop();
  }

  SIMPLE_TEST(Counter) {
    ThreadPool pool{13};

    size_t counter = 0;

    Strand strand(pool);

    static const size_t kIncrements = 1234;

    for (size_t i = 0; i < kIncrements; ++i) {
      Submit(strand, [&]() {
        ExpectThreadPool(pool);
        ++counter;
      });
    };

    pool.WaitIdle();

    ASSERT_EQ(counter, kIncrements);

    pool.Stop();
  }

  SIMPLE_TEST(Fifo) {
    ThreadPool pool{13};
    Strand strand(pool);

    size_t next_index = 0;

    static const size_t kTasks = 12345;

    for (size_t i = 0; i < kTasks; ++i) {
      Submit(strand, [&, i]() {
        ExpectThreadPool(pool);
        ASSERT_EQ(next_index, i);
        ++next_index;
      });
    };

    pool.WaitIdle();

    ASSERT_EQ(next_index, kTasks);

    pool.Stop();
  }

  class Robot {
   public:
    explicit Robot(IExecutor& executor) : strand_(executor) {
    }

    void Kick() {
      Submit(strand_, [this]() {
        ++steps_;
      });
    }

    size_t Steps() const {
      return steps_;
    }

   private:
    Strand strand_;
    size_t steps_{0};
  };

  SIMPLE_TEST(ConcurrentStrands) {
    ThreadPool pool{16};

    static const size_t kStrands = 50;

    std::deque<Robot> robots;
    for (size_t i = 0; i < kStrands; ++i) {
      robots.emplace_back(pool);
    }

    static const size_t kKicks = 25;
    static const size_t kIterations = 25;

    for (size_t i = 0; i < kIterations; ++i) {
      for (size_t j = 0; j < kStrands; ++j) {
        for (size_t k = 0; k < kKicks; ++k) {
          robots[j].Kick();
        }
      }
    }

    pool.WaitIdle();

    for (size_t i = 0; i < kStrands; ++i) {
      ASSERT_EQ(robots[i].Steps(), kKicks * kIterations);
    }

    pool.Stop();
  }

  SIMPLE_TEST(ConcurrentExecutes) {
    ThreadPool workers{2};
    Strand strand{workers};

    ThreadPool clients{4};

    static const size_t kTasks = 1024;

    size_t task_count{0};

    for (size_t i = 0; i < kTasks; ++i) {
      Submit(clients, [&]() {
        Submit(strand, [&]() {
          ASSERT_EQ(ThreadPool::Current(), &workers);
          ++task_count;
        });
      });
    }

    clients.WaitIdle();
    workers.WaitIdle();

    ASSERT_EQ(task_count, kTasks);

    workers.Stop();
    clients.Stop();
  }

  SIMPLE_TEST(StrandOverManual) {
    ManualExecutor manual;
    Strand strand{manual};

    static const size_t kTasks = 117;

    size_t tasks = 0;

    for (size_t i = 0; i < kTasks; ++i) {
      Submit(strand, [&]() {
        ++tasks;
      });
    }

    manual.Drain();

    ASSERT_EQ(tasks, kTasks);
  }

  SIMPLE_TEST(Batching) {
    ManualExecutor manual;
    Strand strand{manual};

    static const size_t kTasks = 100;

    size_t completed = 0;
    for (size_t i = 0; i < kTasks; ++i) {
      Submit(strand, [&completed]() {
        ++completed;
      });
    };

    size_t tasks = manual.Drain();
    ASSERT_LT(tasks, 5);
  }

  SIMPLE_TEST(StrandOverStrand) {
    ThreadPool pool{4};

    Strand strand_1(pool);
    Strand strand_2((IExecutor&)strand_1);

    static const size_t kTasks = 17;

    size_t tasks = 0;

    for (size_t i = 0; i < kTasks; ++i) {
      Submit(strand_2, [&tasks]() {
        ++tasks;
      });
    }

    pool.WaitIdle();

    ASSERT_EQ(tasks, kTasks);

    pool.Stop();
  }

  SIMPLE_TEST(DoNotOccupyThread) {
    pools::compute::ThreadPool pool{1};
    Strand strand{pool};

    Submit(pool, []() {
      std::this_thread::sleep_for(500ms);
    });

    std::atomic<bool> stop_requested{false};

    auto snooze = []() {
      std::this_thread::sleep_for(100ms);
    };

    for (size_t i = 0; i < 5; ++i) {
      Submit(strand, snooze);
    }

    Submit(pool, [&stop_requested]() {
      stop_requested.store(true);
    });

    while (!stop_requested.load()) {
      Submit(strand, snooze);
      std::this_thread::sleep_for(100ms);
    }

    pool.WaitIdle();
    pool.Stop();
  }

  SIMPLE_TEST(NonBlockingExecute) {
    ThreadPool pool{1};
    Strand strand{pool};

    Submit(strand, [&]() {
      // Bubble
      std::this_thread::sleep_for(1s);
    });

    std::this_thread::sleep_for(256ms);

    {
      wheels::StopWatch stop_watch;

      Submit(strand, [&]() {
        // Do nothing
      });

      ASSERT_LE(stop_watch.Elapsed(), 100ms);
    }

    pool.WaitIdle();
    pool.Stop();
  }
}
