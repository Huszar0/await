#include <await/executors/impl/pools/compute/thread_pool.hpp>
#include <await/executors/boot/submit.hpp>

#include <wheels/test/framework.hpp>

#include "../../helpers.hpp"

#include <thread>
#include <chrono>

using await::executors::pools::compute::ThreadPool;
using namespace std::chrono_literals;

TEST_SUITE(ComputeThreadPool) {
  SIMPLE_TEST(JustWorks) {
    ThreadPool pool{4};

    Submit(pool, []() {
      std::cout << "Hello from pool" << std::endl;
    });

    pool.WaitIdle();
    pool.Stop();
  }

  SIMPLE_TEST(ThreadCount) {
    {
      ThreadPool pool{7};
      ASSERT_EQ(pool.ThreadCount(), 7);
      pool.Stop();
    }

    {
      ThreadPool pool{3};
      ASSERT_EQ(pool.ThreadCount(), 3);
      pool.Stop();
    }
  }

  SIMPLE_TEST(Wait) {
    ThreadPool pool{4};

    bool done = false;

    Submit(pool, [&]() {
      std::this_thread::sleep_for(256ms);
      done = true;
    });

    pool.WaitIdle();

    ASSERT_TRUE(done);

    pool.Stop();
  }

  SIMPLE_TEST(MultiWait) {
    ThreadPool pool{1};

    for (size_t i = 0; i < 3; ++i) {
      bool done = false;

      Submit(pool, [&]() {
        std::this_thread::sleep_for(128ms);
        done = true;
      });

      pool.WaitIdle();

      ASSERT_TRUE(done);
    }

    pool.Stop();
  }

  SIMPLE_TEST(ManyTasks) {
    static const size_t kTasks = 1024;

    ThreadPool pool{3};

    std::atomic<size_t> completed{0};
    for (size_t i = 0; i < kTasks; ++i) {
      Submit(pool, [&]() {
        ASSERT_EQ(ThreadPool::Current(), &pool);
        completed.fetch_add(1);
      });
    }

    pool.WaitIdle();

    ASSERT_EQ(completed, kTasks);

    pool.Stop();
  }

//  SIMPLE_TEST(AfterStop) {
//    ThreadPool pool{1};
//    pool.Stop();
//
//    Execute(pool, []() {
//      FAIL_TEST("Executed after Stop");
//    });
//
//    std::this_thread::sleep_for(500ms);
//  }

//  SIMPLE_TEST(Stop) {
//    ThreadPool pool{1};
//
//    Execute(pool, []() {
//      // bubble
//      std::this_thread::sleep_for(1s);
//    });
//
//    std::this_thread::sleep_for(100ms);
//
//    bool done = false;
//    Execute(pool, [&done]() {
//      done = true;
//    });
//    pool.Stop();
//
//    ASSERT_FALSE(done);
//
//    Execute(pool, [&done]() {
//      done = true;
//    });
//
//    std::this_thread::sleep_for(1s);
//
//    ASSERT_FALSE(done);
//  }

  SIMPLE_TEST(JoinThreads1) {
    // Use `Shutdown`
    ThreadPool pool{4};
  }

  SIMPLE_TEST(BlockingQueue) {
    ThreadPool pool{3};

    // Warmup
    Submit(pool, []() {});

    {
      test_helpers::CPUTimeBudgetGuard budget(0.1);

      std::this_thread::sleep_for(1s);

      pool.WaitIdle();
      pool.Stop();
    }
  }

  SIMPLE_TEST(BlockingJoin) {
    ThreadPool pool{2};

    Submit(pool, []() {
      std::this_thread::sleep_for(1s);
    });

    {
      test_helpers::CPUTimeBudgetGuard budget(0.1);

      pool.WaitIdle();
      pool.Stop();
    }
  }

  SIMPLE_TEST(Fifo) {
    ThreadPool pool{1};

    size_t next_task = 0;

    static const size_t kTasks = 256;
    for (size_t t = 0; t < kTasks; ++t) {
      Submit(pool, [t, &next_task]() {
        ASSERT_EQ(next_task, t);
        ++next_task;
      });
    }

    pool.WaitIdle();
    pool.Stop();
  }

#if !defined(TWIST_FAULTY)

  SIMPLE_TEST(RacyCounter) {
    ThreadPool pool{13};

    std::atomic<size_t> counter = 0;

    static const size_t kIncrements = 123456;
    for (size_t i = 0; i < kIncrements; ++i) {
      Submit(pool, [&counter]() {
        auto old = counter.load();
        std::this_thread::yield();
        counter.store(old + 1);
      });
    };

    pool.WaitIdle();

    std::cout << "Racy counter value after " << kIncrements
              << " increments: " << counter.load() << std::endl;

    ASSERT_NE(counter.load(), kIncrements);

    pool.Stop();
  }

#endif

  SIMPLE_TEST(TwoThreadPools) {
    ThreadPool pool1{3};
    ThreadPool pool2{3};

    Submit(pool1, [&]() {
      ASSERT_EQ(ThreadPool::Current(), &pool1);
    });

    Submit(pool2, [&]() {
      ASSERT_EQ(ThreadPool::Current(), &pool2);
    });

    pool1.WaitIdle();
    pool2.WaitIdle();

    pool1.Stop();
    pool2.Stop();
  }

  SIMPLE_TEST(UseAllThreads) {
    ThreadPool pool{3};

    std::atomic<size_t> done = 0;

    auto sleeper = [&]() {
      std::this_thread::sleep_for(1s);
      ASSERT_EQ(ThreadPool::Current(), &pool);
      done.fetch_add(1);
    };

    test_helpers::StopWatch stop_watch;

    Submit(pool, sleeper);
    Submit(pool, sleeper);
    Submit(pool, sleeper);

    pool.WaitIdle();

    auto elapsed = stop_watch.Elapsed();

    ASSERT_EQ(done, 3);
    ASSERT_GE(elapsed, 1s);
    ASSERT_LT(elapsed, 1500ms);

    pool.Stop();
  }

  void CountDown(size_t & count, await::executors::IExecutor* e) {
    Submit(*e, [&count, e]() mutable {
      std::this_thread::sleep_for(100ms);
      count--;
      if (count > 0) {
        CountDown(count, e);
      }
    });
  }

  SIMPLE_TEST(CountDown) {
    ThreadPool pool{3};

    size_t count = 10;
    CountDown(count, &pool);

    pool.WaitIdle();

    ASSERT_EQ(count, 0);

    pool.Stop();
  }

  SIMPLE_TEST(ConcurrentExecutes) {
    ThreadPool pool{2};

    static const size_t kProducers = 5;
    static const size_t kTasks = 1024;

    test_helpers::OnePassBarrier barrier{kProducers};
    std::atomic<int> done{0};

    auto task = [&done]() {
      done.fetch_add(1);
    };

    std::vector<std::thread> producers;

    for (size_t i = 0; i < kProducers; ++i) {
      producers.emplace_back([&pool, &task, &barrier]() {
        barrier.Pass();
        for (size_t j = 0; j < kTasks; ++j) {
          Submit(pool, task);
        }
      });
    }

    for (auto& t : producers) {
      t.join();
    }

    pool.WaitIdle();

    ASSERT_EQ(done.load(), kProducers * kTasks);

    pool.Stop();
  }
}
