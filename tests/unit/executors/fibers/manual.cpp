#include <await/executors/impl/fibers/manual.hpp>

#include <await/fibers/sched/yield.hpp>

#include <await/futures/make/execute.hpp>
#include <await/futures/run/sink.hpp>

#include <wheels/test/framework.hpp>

using namespace await;

TEST_SUITE(FibersManualExecutor) {
  SIMPLE_TEST(Yield) {
    executors::fibers::ManualExecutor manual;

    static const size_t kYields = 7;

    auto fiber = futures::Execute(manual, []() {
      for (size_t i = 0; i < kYields; ++i) {
        fibers::Yield();
      }
    }) | futures::Sink();

    // Submit task to manual executor / launch fiber
    fiber.Start();

    size_t steps = manual.Drain();

    ASSERT_EQ(steps, kYields + 1);
  }

  SIMPLE_TEST(RunAtMost) {
    executors::fibers::ManualExecutor manual;

    static const size_t kYields = 3;

    auto fiber = futures::Execute(manual, []() {
                   for (size_t i = 0; i < kYields; ++i) {
                     fibers::Yield();
                   }
                 }) | futures::Sink();

    // Submit task to manual executor / launch fiber
    fiber.Start();

    size_t steps = 0;

    while (manual.HasTasks()) {
      steps += manual.RunAtMost(1);
    }

    ASSERT_EQ(steps, kYields + 1);
  }
}
