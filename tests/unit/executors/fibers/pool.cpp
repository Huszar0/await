#include <await/executors/impl/fibers/pool.hpp>

#include <await/fibers/sched/yield.hpp>

#include <await/futures/make/execute.hpp>

#include <await/await.hpp>

#include <wheels/test/framework.hpp>

using namespace await;

TEST_SUITE(FiberPool) {
  SIMPLE_TEST(Yield) {
    executors::fibers::Pool pool{4};

    static const size_t kYields = 7;

    size_t yields = 0;

    auto f = futures::Execute(pool, [&yields]() {
      for (size_t i = 0; i < kYields; ++i) {
        fibers::Yield();
        ++yields;
      }
    });

    Await((std::move(f))).ExpectOk();

    ASSERT_EQ(yields, kYields);

    pool.Stop();
  }
}
