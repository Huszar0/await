#include <wheels/test/framework.hpp>

#include <twist/test/with/wheels/stress.hpp>

#include <twist/test/budget.hpp>

#include <await/executors/impl/thread_pool.hpp>
#include <await/executors/impl/strand.hpp>

#include <await/await.hpp>

#include <await/futures/make/contract.hpp>
#include <await/futures/make/execute.hpp>

#include <await/futures/combine/seq/start.hpp>
#include <await/futures/combine/seq/box.hpp>

#include <await/futures/combine/par/all.hpp>
#include <await/futures/combine/par/first_of.hpp>

#include <await/futures/run/go.hpp>

#include <wheels/core/unit.hpp>

#include <atomic>
#include <chrono>

using namespace await;
using namespace std::chrono_literals;

using wheels::Unit;

//////////////////////////////////////////////////////////////////////

void StressTestPipeline() {
  executors::ThreadPool pool{4};
  executors::Strand strand{pool};

  size_t iter = 0;

  while (twist::test::KeepRunning()) {
    ++iter;

    size_t pipelines = 1 + iter % 3;

    std::atomic<size_t> counter1{0};
    size_t counter2 = 0;
    std::atomic<size_t> counter3{0};

    for (size_t j = 0; j < pipelines; ++j) {
      futures::Execute(pool,
                       [&]() {
                         ++counter1;
                       })
          | futures::Via(strand)
          | futures::Map([&](Unit) -> Unit {
            ++counter2;
            return {};
          })
          | futures::Via(pool)
          | futures::Start()
          | futures::Map([&](fallible::Result<Unit>) {
            ++counter3;
          }) | futures::Go();
    }

    pool.WaitIdle();

    ASSERT_EQ(counter1.load(), pipelines);
    ASSERT_EQ(counter2, pipelines);
    ASSERT_EQ(counter3.load(), pipelines);
  }

  pool.Stop();
}

//////////////////////////////////////////////////////////////////////

void StressTestAll() {
  executors::ThreadPool pool{4};

  size_t iter = 0;

  while (twist::test::KeepRunning()) {
    ++iter;

    size_t inputs = 1 + iter % 4;

    std::vector<futures::BoxedFuture<int>> futs;

    for (int j = 0; j < (int)inputs; ++j) {
      futs.push_back(futures::Execute(pool, [j]() -> int {
        return j;
      }) | futures::Box());
    }

    auto all = futures::All(std::move(futs));

    auto ints = Await(std::move(all)).ExpectValue();

    ASSERT_EQ(ints.size(), inputs);

    std::sort(ints.begin(), ints.end());

    for (int j = 0; j < (int)inputs; ++j) {
      ASSERT_EQ(ints[j], j);
    }
  }

  pool.Stop();
}

//////////////////////////////////////////////////////////////////////

void StressTestFirstOf() {
  executors::ThreadPool pool{4};

  size_t iter = 0;

  while (twist::test::KeepRunning()) {
    ++iter;

    size_t inputs = 1 + iter % 4;

    std::vector<futures::BoxedFuture<int>> futs;

    for (int j = 0; j < (int)inputs; ++j) {
      futs.push_back(futures::Execute(pool, [j]() -> int {
        return j;
      }) | futures::Box());
    }

    auto first_of = futures::FirstOf(std::move(futs));

    auto first_value = Await(std::move(first_of)).ExpectValue();

    ASSERT_TRUE((first_value >= 0) && (first_value <= 4));

    pool.WaitIdle();
  }

  pool.Stop();
}

//////////////////////////////////////////////////////////////////////

TEST_SUITE(Futures) {
  TWIST_TEST(StressPipeline, 5s) {
    StressTestPipeline();
  }

  TWIST_TEST(StressAll, 5s) {
    StressTestAll();
  }

  TWIST_TEST(StressFirstOf, 5s) {
    StressTestFirstOf();
  }
}

RUN_ALL_TESTS()
