#include <await/executors/impl/thread_pool.hpp>
#include <await/executors/impl/strand.hpp>
#include <await/executors/boot/submit.hpp>

#include <twist/test/with/wheels/stress.hpp>

#include <twist/test/budget.hpp>

#include <wheels/test/framework.hpp>

#include <deque>

using namespace await::executors;
using namespace std::chrono_literals;

/////////////////////////////////////////////////////////////////////

class Robot {
 public:
  explicit Robot(IExecutor& e)
      : strand_(e) {
  }

  void Cmd() {
    Submit(strand_, [this]() {
      Step();
    });
  }

  size_t Steps() const {
    return steps_;
  }

 private:
  void Step() {
    ++steps_;
  }

 private:
  Strand strand_;
  size_t steps_{0};
};

void Robots(size_t strands, size_t load) {
  ThreadPool pool{4};

  std::deque<Robot> robots;
  for (size_t i = 0; i < strands; ++i) {
    robots.emplace_back(pool);
  }

  ThreadPool clients{strands};

  size_t iters = 0;

  while (twist::test::KeepRunning()) {
    ++iters;

    for (auto& robot : robots) {
      Submit(clients, [&robot, load]() {
        for (size_t j = 0; j < load; ++j) {
          robot.Cmd();
        }
      });
    }

    clients.WaitIdle();
    pool.WaitIdle();
  }

  for (auto& robot : robots) {
    ASSERT_EQ(robot.Steps(), iters * load);
  }

  pool.Stop();
  clients.Stop();
}

//////////////////////////////////////////////////////////////////////

void Fifo() {
  ThreadPool pool{4};
  Strand strand(pool);

  while (twist::test::KeepRunning()) {
    size_t next_index = 0;

    static const size_t kTasks = 128;

    for (size_t i = 0; i < kTasks; ++i) {
      Submit(strand, [&, i]() {
        ASSERT_EQ(next_index, i);
        ++next_index;
      });
    };

    pool.WaitIdle();

    ASSERT_EQ(next_index, kTasks);
  }

  pool.Stop();
}

//////////////////////////////////////////////////////////////////////

void MissingTasks() {
  ThreadPool pool{4};

  size_t iter = 0;

  while (twist::test::KeepRunning()) {
    Strand strand(pool);

    size_t todo = 2 + (iter++) % 5;

    size_t done = 0;

    for (size_t i = 0; i < todo; ++i) {
      Submit(strand, [&done]() {
        ++done;
      });
    }

    pool.WaitIdle();

    ASSERT_EQ(done, todo);
  }

  pool.Stop();
}

//////////////////////////////////////////////////////////////////////

TEST_SUITE(Strand) {
  TWIST_TEST(Robots_1, 5s) {
    Robots(30, 30);
  }

  TWIST_TEST(Robots_2, 5s) {
    Robots(50, 20);
  }

  TWIST_TEST(Fifo, 5s) {
    MissingTasks();
  }

  TWIST_TEST(MissingTasks, 5s) {
    MissingTasks();
  }
}

RUN_ALL_TESTS()
