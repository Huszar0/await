#include <await/executors/impl/thread_pool.hpp>
#include <await/executors/impl/strand.hpp>
#include <await/executors/boot/submit.hpp>

#include <twist/test/with/wheels/stress.hpp>

#include <twist/test/budget.hpp>

#include <wheels/test/framework.hpp>

#include <memory>

using namespace await::executors;
using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

class Widget : public std::enable_shared_from_this<Widget> {
 public:
  Widget(IExecutor& e, size_t& kicks)
      : kicks_(kicks), strand_(e) {
  }

  void Kick() {
    await::executors::Submit(strand_, [this, self = shared_from_this()]() {
      DoKick();
    });
  }

 private:
  // Serialized via strand
  void DoKick() {
    ++kicks_;
  }

 private:
  size_t& kicks_;
  await::executors::Strand strand_;
};

void LifetimeTest() {
  ThreadPool pool{4};

  size_t iter = 0;

  while (twist::test::KeepRunning()) {
    size_t kicks = 0;

    std::shared_ptr<Widget> widget = std::make_shared<Widget>(pool, kicks);

    size_t todo = 1 + (iter++) % 5;

    for (size_t i = 0; i < todo; ++i) {
      Submit(pool, [widget]() {
        widget->Kick();
      });
    }

    widget.reset();

    pool.WaitIdle();

    ASSERT_EQ(kicks, todo);
  }

  pool.Stop();
}

//////////////////////////////////////////////////////////////////////

TEST_SUITE(Strand) {
  TWIST_TEST(Lifetime, 5s) {
    LifetimeTest();
  }
}

RUN_ALL_TESTS()
