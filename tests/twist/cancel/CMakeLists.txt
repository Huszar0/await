ProjectLog("Stress tests for cancellation")

add_executable(await_stress_tests_cancel main.cpp)
target_link_libraries(await_stress_tests_cancel await)
