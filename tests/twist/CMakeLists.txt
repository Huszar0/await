ProjectLog("Stress tests")

# Executors
add_subdirectory(executors/pools/compute)
add_subdirectory(executors/queues/work_stealing_queue)
add_subdirectory(executors/pools/fast)
add_subdirectory(executors/strand)

# Fibers
add_subdirectory(fibers/mutex)
add_subdirectory(fibers/semaphore)
add_subdirectory(fibers/condvar)
add_subdirectory(fibers/wait_group)
add_subdirectory(fibers/channel)
add_subdirectory(fibers/select)

# Futures
add_subdirectory(futures)

# Cancellation
add_subdirectory(cancel)

add_custom_target(await_stress_tests ALL
        DEPENDS
        await_stress_tests_tp
        await_stress_tests_wsqueue
        await_stress_tests_ftp
        await_stress_tests_strand
        await_stress_tests_strand_lifetime
        await_stress_tests_strand_mo
        await_stress_tests_mutex
        await_stress_tests_semaphore
        await_stress_tests_condvar
        await_stress_tests_wg
        await_stress_tests_channel
        await_stress_tests_select
        await_stress_tests_futures
        await_stress_tests_cancel)
