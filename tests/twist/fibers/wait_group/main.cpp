#include <wheels/test/framework.hpp>

#include <twist/test/with/wheels/stress.hpp>

#include <twist/test/budget.hpp>

#include <await/executors/impl/thread_pool.hpp>
#include <await/fibers/boot/go.hpp>
#include <await/fibers/sync/wait_group.hpp>

#include <atomic>
#include <chrono>

using namespace await;
using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

void StressTest1(size_t workers, size_t waiters) {
  executors::ThreadPool scheduler{/*threads=*/4};

  while (twist::test::KeepRunning()) {
    fibers::WaitGroup wg;

    // Number of completed waiters
    std::atomic<size_t> waiters_done{0};
    std::atomic<size_t> workers_done{0};

    wg.Add(workers);

    // Waiters

    for (size_t i = 0; i < waiters; ++i) {
      fibers::Go(scheduler, [&]() {
        wg.Wait();
        waiters_done.fetch_add(1);
      });
    }

    // Workers

    for (size_t j = 0; j < workers; ++j) {
      fibers::Go(scheduler, [&]() {
        workers_done.fetch_add(1);
        wg.Done();
      });
    }

    scheduler.WaitIdle();

    ASSERT_EQ(waiters_done.load(), waiters);
    ASSERT_EQ(workers_done.load(), workers);
  }

  scheduler.Stop();
}

//////////////////////////////////////////////////////////////////////

void StressTest2() {
  executors::ThreadPool scheduler{/*threads=*/4};

  size_t iter = 0;

  while (twist::test::KeepRunning()) {
    ++iter;

    bool done = false;

    fibers::Go(scheduler, [&done, iter]() {
      const size_t steps = 1 + iter % 3;

      // Размещаем wg на куче, но только для того, чтобы
      // AddressSanitizer мог обнаружить ошибку
      // Можно считать, что wg находится на стеке
      auto wg = std::make_unique<fibers::WaitGroup>();
      //fibers::WaitGroup wg;

      wg->Add(1);
      fibers::Go([&wg]() {
        wg->Done();
      });

      wg->Wait();

      wg.reset();

      ASSERT_TRUE(steps > 0);

      done = true;
    });

    scheduler.WaitIdle();

    ASSERT_TRUE(done);
  }

  scheduler.Stop();
}

//////////////////////////////////////////////////////////////////////

TEST_SUITE(WaitGroup) {
  TWIST_TEST(Stress_1_1, 5s) {
    StressTest1(/*workers=*/1, /*waiters=*/1);
  }

  TWIST_TEST(Stress_1_2, 5s) {
    StressTest1(/*workers=*/2, /*waiters=*/2);
  }

  TWIST_TEST(Stress_1_3, 5s) {
    StressTest1(/*workers=*/3, /*waiters=*/1);
  }

  TWIST_TEST(Stress_2, 5s) {
    StressTest2();
  }
}

RUN_ALL_TESTS()
