#include <wheels/test/framework.hpp>

#include <twist/test/with/wheels/stress.hpp>

#include <twist/test/budget.hpp>

#include <await/executors/impl/thread_pool.hpp>
#include <await/fibers/boot/go.hpp>
#include <await/fibers/sync/semaphore.hpp>

#include <atomic>
#include <chrono>

using namespace await;

using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

void StressTest1(size_t fibers, size_t permits) {
  executors::ThreadPool scheduler{4};

  fibers::Semaphore sema{permits};
  std::atomic<size_t> load{0};

  std::atomic<size_t> counter{0};

  for (size_t i = 0; i < fibers; ++i) {
    fibers::Go(scheduler, [&]() {
      while (twist::test::KeepRunning()) {
        sema.Acquire();

        {
          counter.fetch_add(1);

          ASSERT_TRUE(load.fetch_add(1) < permits);
          load.fetch_sub(1);
        }

        sema.Release();
      }
    });
  }

  scheduler.WaitIdle();

  std::cout << "Acquire count: " << counter.load() << std::endl;

  scheduler.Stop();
}

//////////////////////////////////////////////////////////////////////

void StressTest2() {
  executors::ThreadPool scheduler{4};

  while (twist::test::KeepRunning()) {
    fibers::Semaphore sema{0};

    bool done = false;

    fibers::Go(scheduler, [&]() {
      sema.Acquire();
      done = true;
    });

    fibers::Go(scheduler, [&]() {
      sema.Release();
    });

    scheduler.WaitIdle();

    ASSERT_TRUE(done);
  }

  scheduler.Stop();
}

//////////////////////////////////////////////////////////////////////

TEST_SUITE(Semaphore) {
  TWIST_TEST(Stress_1_1, 5s) {
    StressTest1(/*fibers=*/4, /*permits=*/1);
  }

  TWIST_TEST(Stress_1_2, 5s) {
    StressTest1(/*fibers=*/10, /*permits=*/3);
  }

  TWIST_TEST(Stress_1_3, 5s) {
    StressTest1(/*fibers=*/10, /*permits=*/7);
  }

  TWIST_TEST(Stress_2, 10s) {
    StressTest2();
  }
}

RUN_ALL_TESTS()
