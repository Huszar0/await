#include <await/executors/impl/pools/compute/thread_pool.hpp>
#include <await/executors/impl/fibers/pool.hpp>
#include <await/timers/impl/queue.hpp>

#include <await/fibers/sched/sleep_for.hpp>
#include <await/fibers/sched/self.hpp>

#include <await/futures/make/execute.hpp>
#include <await/futures/make/after.hpp>

#include <await/futures/combine/seq/map.hpp>

#include <await/await.hpp>

#include <iostream>

using namespace await;

using namespace std::chrono_literals;

void SleepForExample() {
  std::cout << "SleepFor Example" << std::endl;

  executors::fibers::Pool pool{4};
  timers::Queue timers{pool};

  {
    auto join = futures::Execute(pool, [&]() {
      assert(fibers::AmIFiber());

      for (int i = 5; i > 0; --i) {
        std::cout << i << std::endl;

        // Suspend current fiber for 1s
        fibers::SleepFor(timers.Delay(1s));
      }

      std::cout << "Done!" << std::endl;
    });

    Await(std::move(join)).ExpectOk();
  }

  std::cout << std::endl;

  timers.Stop();
  pool.Stop();
}

void AfterExample() {
  std::cout << "After Example" << std::endl;

  executors::pools::compute::ThreadPool pool{4};
  timers::Queue timers{pool};

  // `after` will be fulfilled with Unit value 3s later
  auto after = futures::After(timers.Delay(3s));

  auto print_later = std::move(after) | futures::Map([]() {
    std::cout << "3 seconds later..." << std::endl;
  });

  // Block current thread for 3s
  Await(std::move(print_later)).ExpectOk();

  timers.Stop();
  pool.Stop();
}

int main() {
  SleepForExample();
  AfterExample();
  return 0;
}
