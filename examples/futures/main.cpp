#include <await/executors/impl/fibers/pool.hpp>
#include <await/executors/impl/fibers/manual.hpp>
#include <await/executors/impl/thread_pool.hpp>
#include <await/timers/impl/queue.hpp>

#include <await/futures/types/future.hpp>

#include <await/futures/make/execute.hpp>
#include <await/futures/make/after.hpp>
#include <await/futures/make/value.hpp>
#include <await/futures/make/fail.hpp>
#include <await/futures/make/just.hpp>
#include <await/futures/make/contract.hpp>

#include <await/futures/combine/seq/map_more.hpp>
#include <await/futures/combine/seq/flatten.hpp>
#include <await/futures/combine/seq/anyway.hpp>
#include <await/futures/combine/seq/via.hpp>
#include <await/futures/combine/seq/with.hpp>
#include <await/futures/combine/seq/yield.hpp>
#include <await/futures/combine/seq/timeout.hpp>
#include <await/futures/combine/seq/clone.hpp>

#include <await/futures/combine/par/first_of.hpp>
#include <await/futures/combine/par/all.hpp>
#include <await/futures/combine/par/interrupt.hpp>

#include <await/futures/run/go.hpp>
#include <await/futures/run/sink.hpp>

#include <await/futures/syntax/bang.hpp>
#include <await/futures/syntax/join.hpp>
#include <await/futures/syntax/or.hpp>
#include <await/futures/syntax/unwrap.hpp>
#include <await/futures/syntax/sequence.hpp>

#include <await/fibers/sched/yield.hpp>

#include <await/await.hpp>

#include <carry/new.hpp>

#include <iostream>
#include <atomic>

using namespace await;

using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

void ExecuteExample() {
  std::cout << "Execute Example" << std::endl;

  executors::ThreadPool pool{/*threads=*/4};

  {
    // There are no dynamic memory allocations in this scope

    auto task = futures::Execute(pool, []() {
      std::cout << "Hello, World!" << std::endl;
    });

    // Submit task to pool and wait for result
    Await(std::move(task)).ExpectOk();
  }

  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void ConceptExample() {
  std::cout << "Concept Example" << std::endl;

  executors::ThreadPool pool{/*threads=*/4};

  {
    // Annotate `task` type with Future<T> concept
    // https://en.cppreference.com/w/cpp/language/constraints

    futures::Future<int> auto task = futures::Execute(pool, []() {
      return 42;
    });

    // Submit task to pool and wait for result
    Await(std::move(task)).ExpectOk();
  }

  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void PipelineExample() {
  std::cout << "Pipeline Example" << std::endl;

  executors::ThreadPool pool{/*threads=*/4};

  {
    auto pipeline = futures::Value(1) |
                    futures::Via(pool) |
                    futures::Map([](int value) {
                      return value + 1;
                    }) |
                    futures::Map([](int value) {
                      return value * 2;
                    });

    // Start pipeline and wait for result
    auto value = Await(std::move(pipeline)).ExpectValue();

    std::cout << "Pipeline -> " << value << std::endl;
  }

  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void JustViaMapExample() {
  std::cout << "Just.Via.Map Example" << std::endl;

  executors::ThreadPool pool{4};

  // Make new context
  auto ctx = carry::New()
                 .SetString("Example", "Just.Via.Map")
                 .Done();

  // Set executor and context
  auto task = futures::Just() |
              futures::Via(pool) |
              futures::With(ctx) |
              futures::Map([]() {
                // Access context of the current task
                auto ctx = executors::task::Context();

                std::cout << "Example -> " << ctx.GetString("Example") << std::endl;
              });

  Await(std::move(task)).ExpectOk();

  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void RecoverExample() {
  std::cout << "Recover Example" << std::endl;

  auto f = futures::Invoke([]() -> int {
             throw std::runtime_error("Fail");
           }) |
           futures::AndThen([](int value) {
             // Skipped
             return value + 1;
           }) |
           futures::OrElse([](fallible::Error /*ignored*/) {
             // Fallback
             return fallible::Ok(42);
           });

  auto result = std::move(f) | futures::Unwrap();

  std::cout << "Exception.Recover -> "
            << *result << std::endl;

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void ContractExample() {
  std::cout << "Contract Example" << std::endl;

  // Contract = (Future, Promise)
  auto [f, p] = futures::Contract<int>();

  {
    // Consumer

    f.IAmEager();

    assert(!f.HasResult());
  }

  {
    // Producer
    std::move(p).SetValue(42);
  }

  {
    // Consumer

    assert(f.HasResult());

    // Unwrap ready result
    // Precondition: f.HasResult() == true
    auto result = std::move(f) | futures::Unwrap();

    std::cout << "Contract -> " << result.ExpectValue() << std::endl;
  }

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void StartExample() {
  std::cout << "Start Example" << std::endl;

  executors::ThreadPool pool{/*threads=*/4};

  {
    // Create lazy computation
    futures::Future<int> auto l = futures::Execute(pool, []() {
      return 42;
    });

    // Convert lazy Future to eager = start computation
    futures::EagerFuture<int> f = std::move(l) | futures::Start();

    // Consume future asynchronously
    std::move(f) |
        futures::Map([](fallible::Result<int> result) {
          std::cout << "Execute -> " << *result << std::endl;
        }) |
        futures::Go();
  }

  pool.WaitIdle();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void BoxExample() {
  std::cout << "Box Example" << std::endl;

  auto l = futures::Value(7) | futures::Map([](int value) {
             return value + 35;
           });

  // Erase thunk type
  futures::BoxedFuture<int> box = std::move(l) | futures::Box();

  int value = (*std::move(box)).ExpectValue();
  std::cout << "Value -> " << value << std::endl;

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void AutoBoxingExample() {
  {
    futures::BoxedFuture<int> f = futures::Value(7);
  }

  {
    futures::BoxedFuture<int> f = futures::Fail(fallible::errors::TimedOut());
  }
}

//////////////////////////////////////////////////////////////////////

void BangOperatorExample() {
  std::cout << "Bang Example" << std::endl;

  executors::ThreadPool pool{/*threads=*/4};

  {
    // Create lazy computation
    auto lazy = futures::Execute(pool, []() {
                  return 42;
                }) |
                futures::Map([](int value) {
                  return value + 1;
                }) |
                futures::Map([](int value) {
                  std::cout << "Value -> " << value << std::endl;
                });

    // Bang for strictness!
    // https://ghc.gitlab.haskell.org/ghc/doc/users_guide/exts/strict.html

    futures::EagerUnitFuture eager = !std::move(lazy);

    Await(std::move(eager)).ExpectOk();
  }

  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void FirstOfExample() {
  std::cout << "FirstOf Example" << std::endl;

  executors::fibers::Pool pool{4};

  {
    auto f = futures::Execute(pool, []() -> int {
               wheels::Defer note([]() {
                 std::cout << "Cancelled" << std::endl;
               });

               while (true) {
                 std::cout << "Running!" << std::endl;
                 fibers::Yield();
               }
             }) | futures::Anyway([]() {
               std::cout << "Anyway" << std::endl;
             }) | futures::Start();

    f.IAmEager();

    // Lazy future
    auto l = futures::Execute(pool, []() {
      return 42;
    });

    auto first = futures::FirstOf(std::move(f), std::move(l));

    int value = Await(std::move(first)).ExpectValue();

    std::cout << "Value -> " << value << std::endl;
  }

  pool.WaitIdle();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void FlattenExample() {
  std::cout << "Flatten Example" << std::endl;

  executors::fibers::Pool pool{4};

  {
    futures::Future<int> auto f = futures::Execute(pool, []() {
                                           // Nested eager asynchronous computation
                                           return !futures::Spawn([] {
                                             for (size_t i = 0; i < 7; ++i) {
                                               fibers::Yield();
                                             }
                                             return 42;
                                           });
                                         }) |
                                         futures::Flatten();

    int value = Await(std::move(f)).ExpectValue();

    std::cout << "Value -> " << value << std::endl;
  }

  pool.WaitIdle();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void YieldExample() {
  std::cout << "Yield Example" << std::endl;

  executors::ThreadPool pool{1};

  {
    futures::Execute(pool, []() {
      std::cout << "1" << std::endl;
    }) |
        futures::Map([](){
            // Submit "interrupt" task
            futures::Spawn([]() {
              std::cout << "Interrupt" << std::endl;
            }) | futures::Go();
            std::cout << "2" << std::endl;

          }) |
        futures::Map([]() {
            std::cout << "3" << std::endl;
          }) |
        futures::Map([&]() {
            std::cout << "4" << std::endl;
          }) |
        futures::Yield() | // <-- Yield to "interrupt" task
        futures::Map([]() {
            std::cout << "5" << std::endl;
          }) |
        futures::Go();
  }

  pool.WaitIdle();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void CloneExample() {
  std::cout << "Clone Example" << std::endl;

  executors::ThreadPool pool{4};

  {
    auto exe = futures::Execute(pool, []() {
      return "Hi";
    });

    // Only eager futures can be forked
    auto [l, r] = std::move(exe) | futures::Clone();

    std::cout << "Right -> " << Await(std::move(r)).ExpectValue() << std::endl;

    {
      auto result = *std::move(l);
      std::cout << "Left -> " << *result << std::endl;
    }
  }

  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void AfterExample() {
  std::cout << "After Example" << std::endl;

  timers::Queue timers;

  // Future that will be fulfilled with Unit value after 1s
  auto after1s = futures::After(timers.Delay(1s));

  auto print_later = std::move(after1s) | futures::Map([]() {
                       std::cout << "1 second later..." << std::endl;
                     });

  Await(std::move(print_later)).ExpectOk();

  timers.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void WithTimeoutExample() {
  std::cout << "WithTimeout Example" << std::endl;

  executors::fibers::Pool pool{4};
  timers::Queue timers;

  std::atomic<int> iters = 0;

  // Infinite loop
  auto loop = futures::Execute(pool, [&iters]() {
    while (true) {
      ++iters;
      fibers::Yield();
    }
  });

  auto with_timeout = std::move(loop) | futures::WithTimeout(timers.Delay(1s));

  Await(std::move(with_timeout)).Ignore("Timeout");

  std::cout << "Iterations made: " << iters.load() << std::endl;

  timers.Stop();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void SinkExample() {
  std::cout << "Sink Example" << std::endl;

  executors::fibers::ManualExecutor manual;

  auto fiber = futures::Execute(manual, []() {
    for (size_t i = 0; i < 7; ++i) {
      fibers::Yield();
    }
  }) | futures::Sink();

  // Submit task to manual executor
  fiber.Start();

  size_t steps = manual.Drain();

  std::cout << "Steps = " << steps << std::endl;
  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void SyntaxExample() {
  std::cout << "Syntax (Operators) Example" << std::endl;

  executors::fibers::Pool pool{4};
  timers::Queue timers;

  {
    // Pipe operator: |
    auto f = futures::After(timers.Delay(2s)) |
             futures::Map([] {
               return 7;
             });

    auto g = futures::After(timers.Delay(1s)) |
             futures::Map([] {
               return 11;
             });

    // Or operator: or (||)
    auto first = std::move(f) or std::move(g);

    auto result = Await(std::move(first));

    std::cout << result.ExpectValue() << std::endl;
  }

  {
    auto exe = futures::Execute(pool, []() {
      std::cout << "Compute" << std::endl;
      return 17;
    });

    auto after = futures::After(timers.Delay(2s));

    // Join operator: and (&&)
    auto join = std::move(exe) and std::move(after);

    Await(std::move(join)).ExpectOk();
  }

  {
    // Sequence operator: >>
    auto f = futures::After(timers.Delay(1s)) >>
      futures::After(timers.Delay(1s)) >>
      futures::Invoke([] {
        std::cout << "2s later" << std::endl;
      });

    Await(std::move(f)).ExpectOk();
  }

  timers.Stop();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

int main() {
  ExecuteExample();
  ConceptExample();
  PipelineExample();
  JustViaMapExample();
  RecoverExample();
  ContractExample();
  StartExample();
  BoxExample();
  AutoBoxingExample();
  BangOperatorExample();
  FirstOfExample();
  FlattenExample();
  YieldExample();
  CloneExample();
  AfterExample();
  WithTimeoutExample();
  SinkExample();
  SyntaxExample();

  return 0;
}
