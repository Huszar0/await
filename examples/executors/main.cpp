#include <await/executors/impl/pools/compute/thread_pool.hpp>
#include <await/executors/impl/fibers/pool.hpp>
#include <await/executors/impl/strand.hpp>
#include <await/executors/impl/manual.hpp>

#include <await/executors/boot/submit.hpp>

#include <await/futures/make/execute.hpp>

#include <await/await.hpp>

// FiberPool Example
#include <await/fibers/sched/yield.hpp>
#include <await/fibers/sched/self.hpp>

#include <iostream>
#include <thread>
#include <vector>

using namespace await;

//////////////////////////////////////////////////////////////////////

// Executors are to function execution as allocators are to memory allocation

// Экзекуторы исполняют задачи – пользовательские лямбды

// Запланировать задачу (лямбду) в экзекутор можно с помощью функции futures::Execute
// Когда и в каком потоке задача будет исполнена – зависит от конкретного экзекутора.

//////////////////////////////////////////////////////////////////////

void ThreadPoolExample() {
  std::cout << "ThreadPool Example" << std::endl;

  // ThreadPool (пул потоков) – фиксированный набор потоков (их называют workers),
  // разбирающих общую неограниченную очередь задач.

  executors::pools::compute::ThreadPool pool{/*threads=*/4};

  std::atomic<size_t> counter{0};

  static const size_t kIncrements = 100500;

  // Отправляем в пул маленькие задачи, которые будут
  // инкрементировать счетчик `counter`
  // Эти задачи будут исполняться параллельно четырьмя потоками пула
  for (size_t i = 0; i < kIncrements; ++i) {
    executors::Submit(pool, [&counter]() {
      // Неатомарный инкремент!
      // Атомик здесь для того, чтобы избежать data race / UB
      counter.store(counter.load() + 1);
    });
  }

  // Блокируем текущий поток до завершения всех запланированных в пул задач
  pool.WaitIdle();

  // Некоторые инкременты будут выполняться параллельно,
  // так что можно ожидать, что некоторые из них не будут учтены
  // в итоговом значении счетчика:

  // T1: r1 = counter.load()
  // T2: r2 = counter.load()
  // <-- В r1 и r2 одинаковые значения
  // T1: counter.store(r1 + 1)
  // T2: counter.store(r2 + 1)
  // <-- Потеряли один из инкрементов

  std::cout << "Counter value = " << counter << ", expected " << kIncrements
            << std::endl;

  std::cout << std::endl;

  // Останавливаем потоки пула
  pool.Stop();
}

//////////////////////////////////////////////////////////////////////

void ExecuteExample() {
  std::cout << "Execute Example" << std::endl;

  executors::pools::compute::ThreadPool pool{/*threads=*/4};

  // Задачи планируются в экзекуторы с помощью API futures

  // Вызов futures::Execute строит фьючу, которая представляет
  // запуск лямбды в указанном экзекуторе

  auto future = futures::Execute(pool, []() -> int {
    std::cout << "Running..." << std::endl;
    return 42;
  });

  // <-- Фьючи – ленивые: задача к этому моменту еще
  // не была отправлена в пул потоков

  // Запускаем фьючу (отправляем задачу в пул потоков) и
  // блокируем текущий поток до ее завершения.
  // Получаем результат в виде fallible::Result<int>
  auto result = Await(std::move(future));

  // <-- К этому моменту задача уже завершена

  // Проверяем, что результат содержит значение
  assert(result.IsOk());
  // Обращаемся к значению
  std::cout << "Value = " << *result << std::endl;

  pool.Stop();
}

//////////////////////////////////////////////////////////////////////

// Этот пример намеренно сломан!

void LifetimeExample() {
  std::cout << "Lifetime Example" << std::endl;

  executors::pools::compute::ThreadPool pool{/*threads=*/4};

  {
    // Если лямбда задачи захватывает ссылки на данные,
    // то пользователь должен гарантировать, что время жизни этих данных
    // покрывает время исполнения задачи

    std::string data = "Hi!";

    executors::Submit(pool, [&data]() {
      // Нагружаем планировщик
      for (size_t i = 0; i < 100; ++i) {
        fibers::Yield();
      }
      // Строка `data` уже может быть разрушена, use-after-free
      std::cout << "Data = " << data << std::endl;
    });

  }  // <-- Строка `data` разрушается

  pool.WaitIdle();
  pool.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void StrandExample() {
  std::cout << "Strand Example" << std::endl;

  executors::pools::compute::ThreadPool pool{4};

  // Strand гарантирует, что запланированные в него задачи будут исполнены
  // строго последовательно, с учетом причинности (happens-before) планирования.

  // Strand не имеет собственных потоков, он декорирует другой экзекутор,
  // который и использует для запуска запланированных задач. Как правило,
  // это пул потоков.

  // Strand – это асинхронная альтернатива мьютексу для использования в пуле потоков:
  // Strand не использует взаимное исключение на уровне потоков.

  // В данном примере декорируемый экзекутор – пул потоков `pool`,
  // но сам Strand знать и учитывать этого не может. Strand полагается
  // лишь на одну гарантию: любая запланированная в декорируемый экзекутор
  // задача будет когда-нибудь исполнена в каком-то потоке.

  executors::Strand strand(pool);

  // Неатомарный счетчик
  size_t counter = 0;

  static const size_t kClients = 5;
  static const size_t kIncrementsPerClient = 12345;

  std::vector<std::thread> clients;

  // Запускаем потоки, которые будут планировать задачи в Strand
  for (size_t i = 0; i < kClients; ++i) {
    clients.emplace_back([&]() {
      // Каждый из этих потоков планирует в Strand
      // kIncrementsPerClient инкрементов счетчика `counter`
      for (size_t j = 0; j < kIncrementsPerClient; ++j) {
        executors::Submit(strand, [&]() {
          // Сами по себе инкременты неатомарны,
          // но они будут "сериализованы" через Strand
          ++counter;
        });
      }
    });
  }

  // Дожидаемся завершения потоков, планирующих задачи в Strand
  for (auto& t : clients) {
    t.join();
  }

  // Дожидаемся завершения задач-инкрементов
  pool.WaitIdle();

  // Все инкременты будут "сериализованы" Strand-ом, так что
  // итоговое значение счетчика будет совпадать с общим числом инкрементов
  std::cout << "Counter value = " << counter
            << ", expected " << kClients * kIncrementsPerClient
            << std::endl;

  std::cout << std::endl;

  pool.Stop();
}

//////////////////////////////////////////////////////////////////////

// Идиоматическое применение Strand:
// последовательный автомат с асинхронным API

class AsyncObject : public std::enable_shared_from_this<AsyncObject> {
 public:
  AsyncObject(executors::IExecutor& e)
      : strand_(e) {
  }

  void AsyncMutate() {
    // Задачи, запланированные в Strand, захватывают сильную ссылку на объект,
    // продляя время его жизни и (транзитивно) время жизни самого Strand-а
    executors::Submit(strand_, [self = shared_from_this()]() {
      self->Mutate();
    });
  }

 private:
  // Все мутации сериализованы через Strand
  void Mutate() {
    std::cout << "Mutation #" << ++count_ << std::endl;
  }

 private:
  executors::Strand strand_;
  // Состояние, "защищенное" Strand-ом
  int count_ = 0;
};

void AsyncObjectExample() {
  std::cout << "AsyncObject Example" << std::endl;

  executors::pools::compute::ThreadPool pool{4};

  // В одном пуле могут работать неограниченное число
  // асинхронных автоматов
  auto obj = std::make_shared<AsyncObject>(pool);

  // Обращаемся к объекту из разных потоков
  for (size_t i = 0; i < 17; ++i) {
    executors::Submit(pool, [obj]() {
      obj->AsyncMutate();
    });
  }

  obj.reset();

  pool.WaitIdle();

  std::cout << std::endl;

  pool.Stop();
}

//////////////////////////////////////////////////////////////////////

void ManualExample() {
  std::cout << "ManualExecutor Example" << std::endl;

  // ManualExecutor – однопоточная очередь задач

  // Все запланированные в ManualExecutor задачи попадают в эту очередь
  // и запускаются с помощью вызовов методов RunNext / RunAtMost / Drain
  executors::ManualExecutor manual;

  // Запланируем задачу на исполнение в экзекуторе `manual`
  executors::Submit(manual, []() {
    std::cout << "Task 1" << std::endl;
  });

  // И еще одну
  executors::Submit(manual, []() {
    std::cout << "Task 2" << std::endl;
  });

  // Теперь обе задачи находятся в очереди ManualExecutor-а,
  // но ни одна из них пока не была запущена.
  std::cout << "<-- No tasks" << std::endl;

  // Запустим первую в очереди задачу:
  manual.RunNext();
  // <- К этому моменту на консоль напечаталось "Step 1"

  std::cout << "<-- 1 task" << std::endl;

  // Теперь запланируем третью задачу
  executors::Submit(manual, []() {
    std::cout << "Task 3" << std::endl;
  });

  // Выполним две оставшиеся в очереди задачи
  manual.Drain();

  std::cout << "<-- 3 tasks" << std::endl;

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void FiberPoolExample() {
  std::cout << "FiberPool Example" << std::endl;

  // fibers::Pool – многопоточный пул файберов или, иначе, пул потоков,
  // который автомагически (sic) запускает все запланированные в него задачи
  // в служебных файберах.
  // См. examples/fibers

  // Если задача не перепланирует / не останавливает исполняющий ее файбер, то
  // после ее завершения этот служебный файбер сразу же запустит следующую в очереди
  // планировщика задачу, без дополнительных накладных расходов (на переключение контекста).

  // Если задача остановила файбер, в котором была запущена (и тем самым “приклеилась”
  // к нему), то после ее завершения служебный файбер вернется в пул и по необходимости
  // будет использован повторно.

  executors::fibers::Pool fiber_pool{/*threads=*/4};

  // Библиотека Await предполагает, что конечный пользователь _всегда_
  // использует пул файберов в качестве базового экзекутора

  executors::Submit(fiber_pool, []() {
    // Убедимся, что исполняемся в файбере
    assert(fibers::AmIFiber());

    std::cout << "Fiber!" << std::endl;

    // Файберам доступны функции для планирования (fibers/sched) и
    // примитивы синхронизации (fibers/sync)

    for (size_t i = 0; i < 17; ++i) {
      fibers::Yield();  // Перепланируем файбер
    }
  });

  fiber_pool.WaitIdle();
  fiber_pool.Stop();
}

//////////////////////////////////////////////////////////////////////

int main() {
  ThreadPoolExample();

  ExecuteExample();
  // LifetimeExample();

  // Strand
  StrandExample();
  AsyncObjectExample();

  ManualExample();

  FiberPoolExample();

  return 0;
}
