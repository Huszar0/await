#include <await/executors/impl/fibers/pool.hpp>
#include <await/executors/impl/manual.hpp>

#include <await/fibers/boot/go.hpp>

#include <await/fibers/sched/self.hpp>
#include <await/fibers/sched/yield.hpp>
#include <await/fibers/sched/teleport.hpp>

#include <await/fibers/sync/wait_group.hpp>
#include <await/fibers/sync/mutex.hpp>
#include <await/fibers/sync/semaphore.hpp>
#include <await/fibers/sync/condvar.hpp>
#include <await/fibers/sync/channel.hpp>
#include <await/fibers/sync/select.hpp>

#include <await/futures/make/execute.hpp>
#include <await/futures/make/after.hpp>

#include <await/futures/run/go.hpp>

#include <await/await.hpp>

#include <await/timers/impl/queue.hpp>

#include <wheels/core/defer.hpp>

#include <iostream>
#include <cassert>
#include <chrono>

using namespace std::chrono_literals;

using namespace await;

//////////////////////////////////////////////////////////////////////

// Файберы (stackful fibers) – кооперативные потоки, реализованные в
// пространстве пользователя.
//
// В отличие от потоков операционной системы, которые разумно заводить
// по числу ядер в процессоре, число запущенных файберов в программе
// может быть сколь угодно большим.

// Если вы знакомы с Golang, то можно (грубо) считать, что файбер ~ горутина.

// Файберы – stackful, у каждого файбера есть собственный call stack,
// который выделяется ему на старте. Благодаря этому пользователю не нужно
// явно маркировать в коде вызовы, останавливающие файбер, специальными
// ключевыми словами await / co_await / т.п.

// Файберы предназначены не для параллельных вычислений, а для параллельного
// _ожидания_ (например, ответа на RPC или завершения IO).

// Останавливаясь, файберы не блокируют потоки.

// Файберы планируются в пространстве пользователя, и потому им необходимы собственные
// примитивы синхронизации: мьютексы, кондвары, каналы и т.п.
// Использовать блокирующие _поток_ примитивы синхронизации из std (например,
// std::mutex или std::condition_variable) в файберах нельзя!

// Файберы – кооперативны: вытеснения нет, запущенный файбер не может быть вытеснен
// в пользу другого файбера, он может остановиться только сам,
// * вызвав fibers::Yield()
// * встав на ожидание фьючи – Await(std::move(future))
// * вызвав mutex.Lock() на мьютексе, который захвачен другим файбером
// и т.п.

// Средой исполнения для файберов служат экзекуторы.
// С точки зрения экзекуторов файбер – это задача.
// В частности, можно запускать файберы на ManualExecutor-е, тогда они будут
// исполняться в одном потоке и детерминированно, что очень удобно для тестирования.

// Пользователь Await не должен запускать файберы непосредственно.
// Вместо этого он должен использовать специальный экзекутор – пул файберов –
// executors::fibers::Pool, который запускает все запланированные в него
// лямбды в служебных файберах.

//////////////////////////////////////////////////////////////////////

// `threads` – число потоков пула-планировщика,
// `init` – код первого запущенного файбера
template <typename F>
void RunScheduler(size_t threads, F init) {
  // Пул потоков, в котором будут исполняться файберы
  executors::fibers::Pool scheduler{threads};

  auto done = futures::Execute(scheduler, std::move(init));
  Await(std::move(done)).ExpectOk();

  scheduler.WaitIdle();
  scheduler.Stop();
}

//////////////////////////////////////////////////////////////////////

void YieldExample() {
  std::cout << "Yield Example" << std::endl;

  // Запускаем файберы на одном потоке
  RunScheduler(/*threads=*/1, []() {
    // Мы - файбер (а не поток)!
    assert(fibers::AmIFiber());

    // Запускаем еще один файбер
    futures::Spawn([]() {
      for (size_t j = 1; j <= 17; ++j) {
        std::cout << "Fiber 2, step " << j << std::endl;

        // Файберы - кооперативные: они не вытесняются,
        // они отдают управление планировщику только явно и добровольно.

        // Вызов Yield() останавливает текущий файбер
        // и перепланирует его в текущий экзекутор.
        // В данном случае экзекутор – это пул потоков, так что текущий файбер
        // перемещается в хвост очереди пула потоков,
        // уступая право исполняться следующему в очереди файберу.

        // Эквивалентно
        // * runtime.Gosched() в Golang
        // * std::this_thread::yield() в С++

        // Попробуйте закомментировать следующий Yield и посмотреть,
        // к чему это приведет:
        fibers::Yield();
      }
    }) | futures::Go();

    for (size_t i = 1; i <= 17; ++i) {
      std::cout << "Fiber 1, step " << i << std::endl;
      fibers::Yield();  // Перепланируемся, уступаем возможность исполняться второму
                        // файберу
    }
  });

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void WaitGroupExample() {
  std::cout << "WaitGroup Example" << std::endl;

  RunScheduler(/*threads=*/4, []() {
    // ~ sync.WaitGroup
    // https://gobyexample.com/waitgroups

    fibers::WaitGroup wg;

    std::atomic<size_t> counter;

    static const size_t kTasks = 1024;

    for (size_t i = 0; i < kTasks; ++i) {
      wg.Add(futures::Spawn([&counter]() {
        // Нагружаем планировщик
        for (size_t j = 0; j < 7; ++j) {
          counter.fetch_add(1);
          fibers::Yield();
        }
      }));
    }

    // Дожидаемся завершения всех запущенных задач
    wg.Wait();

    std::cout << "Counter = " << counter.load() << std::endl;
    assert(counter.load() == kTasks * 7);
  });

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void MutexExample() {
  std::cout << "Mutex Example" << std::endl;

  RunScheduler(/*threads=*/4, []() {
    // Неатомарный счетчик
    size_t counter = 0;

    // Но он защищен _файберным_ мьютексом
    // std::mutex в файберах использовать нельзя: вместе с файбером он заблокирует
    // и поток пула, а их всего 4
    fibers::Mutex mutex;

    {
      // WaitGroup для координации подзадач
      fibers::WaitGroup wg;

      for (size_t i = 0; i < 12345; ++i) {
        wg.Add(futures::Spawn([&]() {
          // Захватываем мьютекс
          // Если мьютекс в данный момент захвачен другим файбером, то текущий
          // файбер останавливается, но поток пула при этом не блокируется!
          {
            // В конструкторе вызывается mutex.Lock(), в деструкторе – mutex.Unlock()
            std::lock_guard guard(mutex);

            // <-- Мы в критической секции
            ++counter;
          }  //  <-- guard разрушается и освобождает мьютекс
        }));
      }

      // Дожидаемся завершения всех запущенных в скоупе файберов
      wg.Wait();
    }

    std::cout << "Counter value = " << counter << std::endl;
  });

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void SemaphoreExample() {
  std::cout << "Semaphore Example" << std::endl;

  RunScheduler(/*threads=*/4, []() {
    std::atomic<size_t> load{0};

    std::atomic<size_t> acquired{0};

    static const size_t kConcurrencyLimit = 3;

    // Семафор ограничивает конкурентный доступ к разделяемому ресурсу
    fibers::Semaphore semaphore{/*permits=*/ kConcurrencyLimit};

    {
      fibers::WaitGroup wg;

      for (size_t i = 0; i < 128; ++i) {
        wg.Add(futures::Spawn([&]() {
          for (size_t j = 0; j < 1024; ++j) {
            semaphore.Acquire();

            acquired.fetch_add(1);

            {
              // <- Не более kConcurrencyLimit файберов

              [[maybe_unused]] size_t curr_load = load.fetch_add(1) + 1;
              assert(curr_load <= kConcurrencyLimit);
              load.fetch_sub(1);
            }

            semaphore.Release();
          }
        }));
      }

      wg.Wait();
    }

    std::cout << "Acquired: " << acquired.load() << std::endl;
  });

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void ChannelExample() {
  std::cout << "Channel Example" << std::endl;

  // В этом примере два файбера "шагают" по очереди, синхронизируясь через
  // каналы

  RunScheduler(/*threads=*/4, []() {
    fibers::Channel<int> first;
    fibers::Channel<int> second;

    static const size_t kIterations = 42;

    // Обратите внимание: каналы захватываются в лямбды по значению
    // Producer-ы и consumer-ы разделяют владение каналом

    futures::Spawn([first, second]() mutable {
      for (size_t j = 0; j < kIterations; ++j) {
        int value = second.Receive();
        std::cout << "Fiber #2: " << value << std::endl;
        first.Send(value + 1);
      }
    }) | futures::Go();


    second.Send(1);

    // Пинг-понг
    for (size_t i = 0; i + 1 < kIterations; ++i) {
      int value = first.Receive();
      std::cout << "Fiber #1: " << value << std::endl;
      second.Send(value + 1);
    }

    first.Receive();
  });

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void SelectExample() {
  std::cout << "Select Example" << std::endl;

  RunScheduler(/*threads=*/4, []() {
    fibers::Channel<int> xs;
    fibers::Channel<int> ys;

    // Producer #1, отправляет "сообщения" (целые числа) в канал `xs`
    futures::Spawn([xs]() mutable {
      for (int i = 0; i < 32; ++i) {
        xs.Send(i);
      }
    }) | futures::Go();

    // Producer #2, отправляет сообщения в канал `ys`
    futures::Spawn([ys]() mutable {
      for (int j = 0; j < 32; ++j) {
        ys.Send(j);
      }
    }) | futures::Go();

    // Consumer получает сообщения из обоих каналов по мере их появления
    for (size_t k = 0; k < 64; ++k) {

      // selected - std::variant
      auto selected = Select(xs, ys);

      switch (selected.index()) {
        case 0:
          // Достали сообщение из канала `xs`
          std::cout << "Received X = " << std::get<0>(selected) << std::endl;
          break;
        case 1:
          // Достали сообщение из канала `ys`
          std::cout << "Received Y = " << std::get<1>(selected) << std::endl;
          break;
      }
    }
  });

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

void AwaitExample() {
  std::cout << "Await Example" << std::endl;

  // В этом примере мы используем алгоритм `Await` для синхронного ожидания фьючи

  // Алгоритм Await может дожидаться не только фьючу, он расширяется на
  // любой тип Awaitable, для которого реализована функция await::GetAwaiter,
  // возвращающая Awaiter, который умеет “дожидаться” события, связанного с Awaitable.

  // Дизайн заимствован из сопрограмм С++20:
  // https://lewissbaker.github.io/2017/11/17/understanding-operator-co-await

  RunScheduler(/*threads=*/4, []() {
    bool done = false;

    auto child = futures::Spawn([&done]() {
      for (size_t i = 0; i < 100'500; ++i) {
        fibers::Yield();  // Нагружаем планировщик
      }
      done = true;
    });

    // Синхронно дожидаемся дочернюю задачу с помощью алгоритма Await
    Await(std::move(child)).ExpectOk();

    assert(done);
  });

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

// Internals

// Экзекуторы executors::fibers::Pool и pools::fast::ThreadPool поддерживают
// подсказки, в частности, Hint::Next, который означает, что планируемую задачу
// следует поместить в специальный LIFO-слот и запустить следующей, в обход
// локальной очереди задач текущего воркера.

// Этими подсказками пользуются каналы, что позволяет достичь большей локальности
// по данным.

// С другой стороны, fibers::Yield отправляет текущий файбер в конец глобальной
// очереди задач шардированного планировщика.

// Перечисленные обстоятельства в примере ниже могут привести к патологическому
// сценарию планирования: два файбера запускают друг друга через LIFO-слот, не
// позволяя тем самым запуститься третьему файберу, который должен их остановить.

// Чтобы этого не произошло, планировщик иногда должен опрашивать глобальную
// очередь _до_ обращения к LIFO-слоту.

void FairnessExample() {
  std::cout << "Fairness Example" << std::endl;

  executors::fibers::Pool scheduler{1};

  auto join = futures::Execute(scheduler, []() {
    fibers::Channel<int> xs{1};
    fibers::Channel<int> ys{1};

    fibers::WaitGroup wg;

    std::atomic<bool> stop_requested{false};

    wg.Add(futures::Spawn([xs, ys, &stop_requested]() mutable {
      while (!stop_requested.load()) {
        ys.Receive();
        std::cout << "Ping" << std::endl;
        xs.Send(1);
      }
    }));

    wg.Add(futures::Spawn([xs, ys, &stop_requested]() mutable {
      ys.Send(1);
      while (!stop_requested.load()) {
        xs.Receive();
        std::cout << "Pong" << std::endl;
        ys.Send(1);
      }
    }));

    wg.Add(futures::Spawn([&]() {
      for (size_t i = 0; i < 10; ++i) {
        fibers::Yield();
      }
      stop_requested.store(true);
    }));

    wg.Wait();
  });

  Await(std::move(join)).ExpectOk();

  scheduler.WaitIdle();
  scheduler.Stop();

  std::cout << std::endl;
}

//////////////////////////////////////////////////////////////////////

// Файберы могут исполняться на произвольном экзекуторе. Если в качестве
// экзекутора выбрать ManualExecutor, то получим _детерминированное_
// исполнение, что очень удобно для тестирования.

void ManualExample() {
  std::cout << "Manual Example" << std::endl;

  // Manual executor позволяет детерминированно исполнять
  // запущенные файберы
  executors::ManualExecutor manual;

  // fibers::Go – низкоуровневая функция, запускающая отдельный файбер
  // для исполнения лямбды, без возможности дождаться завершения или отменить исполнение.
  // Функция необходима для бутстраппинга самой библиотеки, для тестирования.
  // Конечный пользователь должен использовать экзекутор executors::fibers::Pool
  // и запускать задачи _только_ через API futures!

  fibers::Go(manual, []() {
    // Первый файбер

    std::cout << "Step 1" << std::endl;
    // Файбер перепланирует себя в конец очереди экзекутора `manual`,
    // уступает возможность исполняться второму файберу.
    fibers::Yield();
    std::cout << "Step 3" << std::endl;
  });

  fibers::Go(manual, []() {
    // Второй файбер

    std::cout << "Step 2" << std::endl;
    fibers::Yield();  // Уступаем первому
    std::cout << "Step 4" << std::endl;
  });

  // К этому моменту мы стартовали два файбера, но ни один
  // из них пока не начал исполняться (см. ManualExample из examples/executors)

  // "Шагаем" файберами до тех пор, пока они не завершат свои лямбды

  manual.RunAtMost(1);
  std::cout << "After Step 1" << std::endl;

  manual.RunAtMost(1);
  std::cout << "After Step 2" << std::endl;

  manual.Drain();

  /* В силу детерминизма и однопоточности _гарантируется_ следующий вывод:
   * Step 1
   * Step 2
   * Step 3
   * Step 4
   */
}

//////////////////////////////////////////////////////////////////////

int main() {
  YieldExample();

  WaitGroupExample();

  MutexExample();
  SemaphoreExample();

  ChannelExample();
  SelectExample();
  FairnessExample();

  AwaitExample();

  ManualExample();

  return 0;
}
