#include <await/executors/impl/fibers/pool.hpp>
#include <await/executors/impl/strand.hpp>

#include <await/fibers/sched/yield.hpp>

#include <await/futures/make/execute.hpp>
#include <await/futures/combine/seq/map_more.hpp>
#include <await/futures/run/go.hpp>

#include <twist/rt/run.hpp>

#include <fmt/core.h>

using namespace await;

void TwistedExample() {
  executors::fibers::Pool pool{4};
  executors::Strand strand{pool};

  size_t cs = 0;

  for (int i = 0; i < 7; ++i) {
    futures::Execute(pool, [] {
      for (size_t i = 0; i < 3; ++i) {
        fibers::Yield();
      }
    })
        | futures::Via(strand)
        | futures::AndThen([&cs](wheels::Unit) {
          ++cs;
        })
        | futures::Go();
  }

  pool.WaitIdle();
  pool.Stop();

  fmt::println("cs = {}", cs);
}

int main() {
  twist::rt::Run([] {
    TwistedExample();
  });

  return 0;
}
