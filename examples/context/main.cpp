#include <await/executors/impl/thread_pool.hpp>

#include <carry/empty.hpp>
#include <carry/new.hpp>
#include <carry/wrap.hpp>
#include <carry/mutable.hpp>

#include <await/executors/task/context.hpp>

#include <await/futures/make/just.hpp>

#include <await/futures/combine/seq/with.hpp>
#include <await/futures/combine/seq/via.hpp>
#include <await/futures/combine/seq/map.hpp>

#include <await/await.hpp>

#include <iostream>

using namespace await;

///////////////////////////////////////////////////////////////////////

// Контекст – иммутабельный словарь из строковых ключей в гетерогенные значения

// Типичное применение контекста - трассировка RPC

///////////////////////////////////////////////////////////////////////

void ConstructorsExample() {
  std::cout << "Basics Example" << std::endl;

  {
    auto empty = carry::Empty();

    [[maybe_unused]] auto maybe = empty.TryGet<int64_t>("key");
    assert(!maybe.has_value());
  }

  auto ctx1 = carry::New()
                  .SetString("key1", "value1")
                  .SetInt64("key2", 123)
                  .Done();

  auto ctx2 = carry::Wrap(ctx1)
                  .SetString("key1", "value2")  // Перезаписываем ключ
                  .SetString("key3", "value3")  // Устанавливаем новый ключ
                  .Done();

  std::cout << "ctx1: "
            << "key1 -> " << ctx1.Get<std::string>("key1") << ", "
            << "key2 -> " << ctx1.Get<int64_t>("key2") << std::endl;

  std::cout << "ctx2: "
            << "key1 -> " << ctx2.Get<std::string>("key1") << ", "
            << "key2 -> " << ctx2.Get<int64_t>("key2") << ", "
            << "key3 -> " << ctx2.Get<std::string>("key3") << std::endl;

  std::cout << std::endl;
}

///////////////////////////////////////////////////////////////////////

void CurrentExample() {
  std::cout << "Current Example" << std::endl;

  executors::ThreadPool pool{4};

  // Конструируем новый контекст
  auto ctx = carry::New()
                 .SetString("request_id", "XYZ-123")
                 .Done();

  // Планируем задачу в пул потоков, задаем ей контекст
  auto f = futures::Just() |
           futures::Via(pool) |
           futures::With(ctx) |
           futures::Map([]() {
             // Обращаемся к текущему контексту
             auto ctx = executors::task::Context();

             std::cout << "request_id = "
                       << ctx.Get<std::string>("request_id")
                       << std::endl;
           });

  // Запускаем задачу и блокируем поток до ее завершения
  Await(std::move(f)).ExpectOk();

  pool.Stop();

  std::cout << std::endl;
}

///////////////////////////////////////////////////////////////////////

void PropagationExample() {
  std::cout << "Propagation Example" << std::endl;

  executors::ThreadPool pool{4};

  auto ctx = carry::New()
                 .SetString("request_id", "XYZ-123")
                 .Done();

  // Начинаем цепочку задач
  auto f = futures::Just() |
           futures::Via(pool) |
           futures::With(ctx) |
           futures::Map([]() {
             auto ctx = executors::task::Context();

             std::cout << "f / request_id = "
                       << ctx.Get<std::string>("request_id")
                       << std::endl;
           });

  // Продолжаем цепочку задач
  // Каждая последующая задача автоматически наследует контекст предшествующей
  auto g = std::move(f) | futures::Map([]() {
    auto ctx = executors::task::Context();

    std::cout << "g / request_id = "
              << ctx.Get<std::string>("request_id")
              << std::endl;
  });

  // Блокируем поток до завершения цепочки задач
  Await(std::move(g)).ExpectOk();

  pool.Stop();

  std::cout << std::endl;
}

///////////////////////////////////////////////////////////////////////

void MutatorExample() {
  std::cout << "Mutator Example" << std::endl;

  executors::ThreadPool pool{4};

  auto ctx = carry::New()
                 .SetString("key", "init")
                 .Done();

  auto init = futures::Just() |
              futures::Via(pool) |
              futures::With(ctx) |
              futures::Map([]() { return; });

  // Create mutable wrapper for context associated with future `init`
  auto mutator = carry::MutableWrap(init.UserContext());

  // Capture mutator to continuation
  auto then = std::move(init) | futures::Map([mutator]() mutable {
    // Mutate "output" context
    mutator.SetString("key", "new");

    auto ctx = executors::task::Context();

    std::cout << "then / key -> "
              << ctx.GetString("key")
              << std::endl;
  }) | futures::With(mutator.View());  // <- Set output context

  auto last = std::move(then) | futures::Map([]() {
    auto ctx = executors::task::Context();

    std::cout << "last / key -> " << ctx.GetString("key") << std::endl;
  });

  Await(std::move(last)).ExpectOk();

  pool.Stop();
}

///////////////////////////////////////////////////////////////////////

int main() {
  ConstructorsExample();
  CurrentExample();
  PropagationExample();
  MutatorExample();
  return 0;
}
