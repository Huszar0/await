# Await

Concurrency for C++ 🚀 with functional flavour λ

[Demo](demo/main.cpp)

## Prologue

_You don't have to be an engineer to be a racing driver, but you do have to have Mechanical Sympathy_ – [Jackie Stewart](https://en.wikipedia.org/wiki/Jackie_Stewart), 🏎️ racing driver

[Mechanical Sympathy](https://wa.aws.amazon.com/wellarchitected/2020-07-02T19-33-23/wat.concept.mechanical-sympathy.en.html)

## Features

- Orthogonal basis for concurrency: _executors_ × _fibers_ × _futures_ × _cancellation_
- Scalable work-stealing task scheduler (executor) – `executors::pools::fast::ThreadPool`
- Transparent stackful fibers via `executors::fibers::Pool` (backed by `pools::fast::ThreadPool`) with automatic pooling
- Functional futures with extensive combinator language and fluent API
- Futures are lazy ⟹ no dynamic memory allocations and synchronization required
- Transparent cooperative cancellation (interruption), wait-free for sequential composition / lock-free for parallel composition
- Structured concurrency (short-circuiting) via parallel futures combinators (`All`, `FirstOf`, etc) and `fibers::WaitGroup` (nursery)
- Support for task contexts, transparent context propagation
- Deterministic stress-tests with fault-injection via 🧵 [`Twist`](https://gitlab.com/Lipovsky/twist) framework

## Contents

- [Executors](await/executors/impl)
  - Thread Pools (`pools`)
    - `pools::compute::ThreadPool` with shared blocking queue for independent CPU-bound tasks
    - Scalable work-stealing `pools::fast::ThreadPool` for fibers / stackless coroutines (IO-bound tasks)
  - `Strand` (asynchronous mutex)
  - `ManualExecutor` for deterministic testing
  - Transparent fibers (`fibers`)
    - `fibers::Pool`
    - `fibers::ManualExecutor`
- [Futures](await/futures)
  - Types (`types`)
    - concepts `SomeFuture`, `Future<T>`, `UnitFuture`
    - `BoxedFuture<T>`
    - `EagerFuture<T>`
  - Constructors (`make`)
    - `Execute` / `Spawn` / `Invoke`
    - `Contract` + `Promise<T>` (eager)
    - `After`
    - `Value(T)` / `Fail(Error)` / `Ready(Result)`
    - `Just`
    - `Never`
  - Combinators (`combine`)
    - Sequential composition (`seq`)
      - `Map` – applies user λ to async result / value / error
      - `AndThen` / `OrElse` – separate happy / failure path for `Map`
      - `Flatten` – flattens nested futures (for asynchronous mappers)
      - `FlatMap` = `Map` + `Flatten`
      - `Via` / `Inline` – executors for mappers
      - `Yield` / `Hint` – hints for scheduler
      - `With` – attaches user-defined context
      - `Box` – erases concrete `Future` (`Thunk`) type
      - `Start` – converts to `EagerFuture`, starts operation
      - `Clone` – splits future value into two copies
      - `ToUnit` – erases value type to `Unit`
      - `OnComplete` / `OnCancel` / `Anyway` – asynchronous alternative to `defer` / RAII
      - `WithTimeout` – attaches timeout
      - `Sequence` – runs two asynchronous operations in sequence
    - Parallel composition (`par`)
      - `All`
      - `FirstOf`
      - `Quorum`
      - `Join`
      - `Interrupt`
  - Terminators / runners (`run`)
    - `Await` – synchronously unwraps `Result` from future
    - `Unwrap` – optimistically unwraps ready `Result` without waiting, ~ `operator *` for `std::optional`
    - `Go` – starts and detaches future
    - `Sink` – converts `Future` into `Chain` that can be started on demand
  - Operators (`syntax`)
    - _pipe_: `Just() | Via(pool) | Map([]{})`
    - _sequence_: `f >> g` ~ `Sequence(f, g)` ~ `Await(f) ; g`
    - _bang_: `!f` ~ `f | Start()`
    - _unwrap_: `*f` ~ `f | Unwrap()`
    - _go_: `~f` ~ `f | Go()`
    - _or_: `f or g` ~ `FirstOf(f, g)`
    - _join_: `f and g` ~ `Join(f, g)`
    - _sum_: `f + g` ~ `All(f, g)`
- [Stackful Fibers](await/fibers)
  - Scheduling (`sched`)
    - `Yield`
    - `TeleportTo` + `TeleportGuard`
    - `SleepFor`
  - Synchronization (`sync`)
    - `Mutex` (lock-free)
    - `OneShotEvent` (lock-free)
    - `WaitGroup` (lock-free)
    - `Semaphore`
    - `CondVar`
    - `Channel<T>` + `Select`
- [Timers](await/timers)
- Generic [`Await` algorithm](await/await/algorithm.hpp) (~ `co_await`)
  - `Await(Future)`

## Docs

- [User Guide](https://docs.google.com/document/d/1-5QVP1DAfGfLoialJsLHlX5vb30ehHA3cAfFafBGiGQ/edit?usp=sharing)

## Examples

- [Futures](examples/futures/main.cpp)
- [Executors](examples/executors/main.cpp)
- [Fibers](examples/fibers/main.cpp)
- [Cancellation](examples/cancel/main.cpp)
- [Timers](examples/timers/main.cpp)
- [Context](examples/context/main.cpp)

## Tutorial

- [Implementing your own Future](tutorial/future/event/event.hpp) for `AsyncEvent`
  - [Usage](tutorial/future/example/main.cpp)
  - [Tests](tutorial/future/tests)

## Inspiration 

- [Your Server as a Function](https://monkey.org/~marius/funsrv.pdf), [Futures: Twitter vs Scala](https://www.youtube.com/watch?v=jiYe-LdPrS0), [Twitter Futures](https://twitter.github.io/finagle/guide/developers/Futures.html#)
- [Project Loom Proposal](http://cr.openjdk.java.net/~rpressler/loom/Loom-Proposal.html), State of Loom: [Part 1](https://cr.openjdk.java.net/~rpressler/loom/loom/sol1_part1.html), [Part 2](https://cr.openjdk.java.net/~rpressler/loom/loom/sol1_part2.html)
- [Notes on structured concurrency, or: Go statement considered harmful](https://vorpus.org/blog/notes-on-structured-concurrency-or-go-statement-considered-harmful/)
- [Timeouts and cancellation for humans](https://vorpus.org/blog/timeouts-and-cancellation-for-humans/)
- [Unified Executors](https://github.com/facebookexperimental/libunifex)
- [Asymmetric Transfer](https://lewissbaker.github.io/)
- Folly: [Futures](https://github.com/facebook/folly/tree/master/folly/futures), [Executors](https://github.com/facebook/folly/tree/master/folly/executors), [Fibers](https://github.com/facebook/folly/tree/master/folly/fibers)
- [Go scheduler: Implementing language with lightweight concurrency](https://www.youtube.com/watch?v=-K11rY57K7k)
- [1024cores](https://www.1024cores.net/home)
- [Асинхронность в программировании](https://habr.com/ru/company/jugru/blog/446562/)

## Requirements

|                  | _Supported_       |
|------------------|-------------------|
| Architecture     | x86-64, ARMv8-A-64 |
| Operating System | Linux,  MacOS     |
| Compiler         | Clang++ (≥ 13)    |

## Dependencies

- [Wheels](https://gitlab.com/Lipovsky/wheels) – core components, test framework
- [Sure](https://gitlab.com/Lipovsky/sure) – context switching for stackful fibers / coroutines
- [Twist](https://gitlab.com/Lipovsky/twist) – fault injection framework for concurrency
- [Fallible](https://gitlab.com/Lipovsky/fallible) – error handling
- [Carry](https://gitlab.com/Lipovsky/carry) – user context

### Third-party

- [Mimalloc](https://github.com/microsoft/mimalloc) – memory allocation

## Build

```shell
# Clone repo
git clone https://gitlab.com/Lipovsky/await.git 
cd await
mkdir build && cd build
# Generate build files
cmake -DAWAIT_TESTS=ON -DAWAIT_EXAMPLES=ON -DCMAKE_CXX_COMPILER=/usr/bin/clang++-13 -DCMAKE_C_COMPILER=/usr/bin/clang-13 ..
# Build example
make await_example_futures
# Run example
./examples/futures/await_example_futures
```

### CMake options

#### Flags

- `AWAIT_TESTS` – Enable tests
- `AWAIT_EXAMPLES` – Enable examples
- `AWAIT_DEVELOPER` – Developer mode: tests + examples + `-Werror`
- `AWAIT_MIMALLOC` – Use `mimalloc` memory allocator
