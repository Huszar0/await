#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

#include <await/futures/impl/lazy/terminators/unwrap.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace fluent {

struct [[nodiscard]] Unwrap {
  template <SomeFuture Future>
  auto Pipe(Future input) {
    lazy::terminators::UnwrapConsumer<ValueOf<Future>> consumer;
    input.Start(&consumer);
    return consumer.ExpectResult();
  }
};

}  // namespace fluent

//////////////////////////////////////////////////////////////////////

inline auto Unwrap() {
  return fluent::Unwrap();
}

}  // namespace await::futures
