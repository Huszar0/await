#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/impl/lazy/terminators/sink.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

template <SomeFuture Future>
using Chain = lazy::terminators::Sink<Future>;

//////////////////////////////////////////////////////////////////////

namespace fluent {

struct [[nodiscard]] Sink {
  template <SomeFuture Future>
  Chain<Future> Pipe(Future input) {
    return lazy::terminators::Sink(std::move(input));
  }
};

}  // namespace fluent

//////////////////////////////////////////////////////////////////////

inline auto Sink() {
  return fluent::Sink();
}

}  // namespace await::futures
