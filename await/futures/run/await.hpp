#pragma once

#include <await/await/algorithm.hpp>
#include <await/await/types/future.hpp>

#include <await/futures/types/traits/value_of.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace fluent {

struct Await {
  template <SomeFuture Future>
  fallible::Result<ValueOf<Future>> Pipe(Future input) {
    return await::Await(std::move(input));
  }
};

}  // namespace fluent

//////////////////////////////////////////////////////////////////////

inline auto Await() {
  return fluent::Await();
}

}  // namespace await::futures
