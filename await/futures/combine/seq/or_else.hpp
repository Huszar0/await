#pragma once

#include <await/futures/combine/seq/recover.hpp>

namespace await::futures {

template <typename Handler>
auto OrElse(Handler handler) {
  return Recover(std::move(handler));
}

}  // namespace await::futures
