#pragma once

#include <await/futures/impl/lazy/thunks/seq/on_cancel.hpp>
#include <await/futures/impl/lazy/thunks/seq/map.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/syntax/pipe.hpp>

#include <fallible/result/mappers.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace fluent {

template <typename Handler>
struct [[nodiscard]] OnComplete {
  Handler handler;

  OnComplete(Handler h)
      : handler(std::move(h)) {
  }

  template <SomeFuture Future>
  auto Pipe(Future input) {
    using ValueType = ValueOf<Future>;

    static_assert(fallible::ConstResultHandler<Handler, ValueType>);

    auto mapper =
        [handler = std::move(handler)](fallible::Result<ValueType> input) mutable {
          handler(input);
          return input;
        };

    return lazy::thunks::Map(std::move(input), std::move(mapper));
  }
};

template <typename Handler>
struct [[nodiscard]] OnCancel {
  Handler handler;

  OnCancel(Handler h)
      : handler(std::move(h)) {
    static_assert(fallible::EventHandler<Handler>);
  }

  template <SomeFuture Future>
  auto Pipe(Future input) {
    return lazy::thunks::OnCancel(std::move(input), std::move(handler));
  }
};

}  // namespace fluent

//////////////////////////////////////////////////////////////////////

template <typename Handler>
auto OnComplete(Handler handler) {
  return fluent::OnComplete(std::move(handler));
}

template <typename Handler>
auto OnCancel(Handler handler) {
  return fluent::OnCancel(std::move(handler));
}

}  // namespace await::futures
