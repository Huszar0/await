#pragma once

#include <await/futures/impl/lazy/thunks/seq/eager.hpp>

#include <await/futures/types/eager.hpp>

#include <await/futures/types/traits/value_of.hpp>
#include <await/futures/types/traits/is_lazy.hpp>

#include <await/futures/impl/eager/states/evaluate.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace fluent {

struct [[nodiscard]] Start {
  template <SomeFuture Future>
  auto Pipe(Future input) {
    static_assert(IsLazy<Future>);

    using Evaluator = eager::states::Evaluate<Future>;

    auto evaluator = refer::New<Evaluator>(std::move(input));
    evaluator->StartEvaluation();
    return lazy::thunks::Eager<ValueOf<Future>>(std::move(evaluator));
  }
};

}  // namespace fluent

//////////////////////////////////////////////////////////////////////

inline auto Start() {
  return fluent::Start();
}

}  // namespace await::futures
