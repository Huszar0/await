#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/impl/lazy/thunks/seq/sequence.hpp>

namespace await::futures {

// Sequence two asynchronous operations

template <SomeFuture FirstFuture, SomeFuture ThenFuture>
Future<ValueOf<ThenFuture>> auto Sequence(FirstFuture first, ThenFuture then) {
  return lazy::thunks::Sequence(std::move(first), std::move(then));
}

}  // namespace await::futures
