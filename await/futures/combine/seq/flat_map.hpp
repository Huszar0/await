#pragma once

#include <await/futures/combine/seq/map.hpp>
#include <await/futures/combine/seq/flatten.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace fluent {

template <typename Mapper>
struct FlatMap {
  Mapper mapper;

  FlatMap(Mapper m)
      : mapper(std::move(m)) {
  }

  template <SomeFuture Future>
  auto Pipe(Future input) {
    return std::move(input) |
           futures::Map(std::move(mapper)) |
           futures::Flatten();
  }
};

}  // namespace fluent

//////////////////////////////////////////////////////////////////////

// Shortcut for: f | Map(mapper) | Flatten()

template <typename Mapper>
auto FlatMap(Mapper mapper) {
  return fluent::FlatMap{std::move(mapper)};
}

}  // namespace await::futures
