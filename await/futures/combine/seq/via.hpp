#pragma once

#include <await/futures/impl/lazy/thunks/seq/via.hpp>

#include <await/futures/syntax/pipe.hpp>

#include <await/executors/impl/inline.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace fluent {

struct [[nodiscard]] Via {
  executors::IExecutor& executor;

  template <SomeFuture Future>
  auto Pipe(Future input) {
    return lazy::thunks::Via{std::move(input), executor};
  }
};

}  // namespace fluent

//////////////////////////////////////////////////////////////////////

inline auto Via(executors::IExecutor& executor) {
  return fluent::Via{executor};
}

inline auto Inline() {
  return fluent::Via{executors::Inline()};
}

}  // namespace await::futures
