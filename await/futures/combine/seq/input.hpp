#pragma once

#include <await/futures/impl/lazy/thunks/seq/input.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace fluent {

struct [[nodiscard]] ToInput {
  template <SomeFuture Future>
  auto Pipe(Future input) {
    return lazy::thunks::Input{std::move(input)};
  }
};

}  // namespace fluent

//////////////////////////////////////////////////////////////////////

inline auto ToInput() {
  return fluent::ToInput{};
}

}  // namespace await::futures
