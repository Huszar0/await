#pragma once

#include <await/futures/make/after.hpp>
#include <await/futures/combine/par/interrupt.hpp>
#include <await/futures/syntax/pipe.hpp>

#include <fallible/error/make.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace fluent {

struct [[nodiscard]] WithTimeout {
  timers::Delay delay;

  template <SomeFuture Future>
  auto Pipe(Future input) {
    return Interrupt(
        std::move(input),
        After(delay),
        fallible::errors::TimedOut().Done());
  }
};

}  // namespace fluent

//////////////////////////////////////////////////////////////////////

inline auto WithTimeout(timers::Delay delay) {
  return fluent::WithTimeout{delay};
}

}  // namespace await::futures
