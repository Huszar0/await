#pragma once

// Rule them all
#include <await/futures/combine/seq/anyway.hpp>
#include <await/futures/combine/seq/box.hpp>
#include <await/futures/combine/seq/clone.hpp>
#include <await/futures/combine/seq/flatten.hpp>
#include <await/futures/combine/seq/handle.hpp>
#include <await/futures/combine/seq/map_more.hpp>
#include <await/futures/combine/seq/start.hpp>
#include <await/futures/combine/seq/timeout.hpp>
#include <await/futures/combine/seq/unit.hpp>
#include <await/futures/combine/seq/via.hpp>
#include <await/futures/combine/seq/with.hpp>
#include <await/futures/combine/seq/yield.hpp>
