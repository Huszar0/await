#pragma once

#include <await/futures/impl/lazy/thunks/seq/with.hpp>

#include <await/futures/syntax/pipe.hpp>

#include <carry/wrap.hpp>

namespace await::futures {

namespace fluent {

//////////////////////////////////////////////////////////////////////

struct [[nodiscard]] With {
  carry::Context context;

  template <SomeFuture Future>
  auto Pipe(Future input) {
    return lazy::thunks::With{std::move(input), std::move(context)};
  }
};

template <typename V>
struct [[nodiscard]] WrapKey {
  carry::Key key;
  V value;

  template <SomeFuture Future>
  auto Pipe(Future input) {
    auto ctx = carry::Wrap(input.UserContext()).Set(key, value).Done();
    return lazy::thunks::With(std::move(input), std::move(ctx));
  }
};

}  // namespace fluent

//////////////////////////////////////////////////////////////////////

inline auto With(carry::Context context) {
  return fluent::With{std::move(context)};
}

template <typename V>
auto With(carry::Key key, V value) {
  return fluent::WrapKey<V>{key, std::move(value)};
}

}  // namespace await::futures
