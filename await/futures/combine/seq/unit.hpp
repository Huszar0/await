#pragma once

#include <await/futures/impl/lazy/thunks/seq/unit.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

template <SomeFuture Future>
SomeFuture auto ToUnit(Future future) {
  return lazy::thunks::ToUnit{std::move(future)};
}

//////////////////////////////////////////////////////////////////////

namespace fluent {

struct [[nodiscard]] ToUnit {
  template <SomeFuture Future>
  auto Pipe(Future input) {
    return futures::ToUnit(std::move(input));
  }
};

}  // namespace fluent

//////////////////////////////////////////////////////////////////////

inline auto ToUnit() {
  return fluent::ToUnit();
}

}  // namespace await::futures
