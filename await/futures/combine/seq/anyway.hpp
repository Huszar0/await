#pragma once

#include <await/futures/combine/seq/handle.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace fluent {

template <fallible::EventHandler Handler>
struct [[nodiscard]] Anyway {
  Handler handler;

  Anyway(Handler h)
      : handler(std::move(h)) {
  }

  template <SomeFuture Future>
  auto Pipe(Future input) {
    using ValueType = ValueOf<Future>;

    auto result_handler = [handler = handler](const fallible::Result<ValueType>&) mutable {
      handler();
    };

    return std::move(input) |
           futures::OnComplete(std::move(result_handler)) |
           futures::OnCancel(handler);
  }
};

}  // namespace fluent

//////////////////////////////////////////////////////////////////////

template <fallible::EventHandler Handler>
auto Anyway(Handler handler) {
  return fluent::Anyway{std::move(handler)};
}

}  // namespace await::futures
