#pragma once

#include <await/futures/impl/lazy/thunks/seq/map.hpp>

#include <await/futures/syntax/pipe.hpp>

#include <fallible/result/mappers.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace fluent {

template <typename Handler>
struct [[nodiscard]] Recover {
  Handler handler;

  Recover(Handler h)
      : handler(std::move(h)) {
  }

  template <SomeFuture Future>
  static void ApplicableTo() {
    static_assert(fallible::ErrorHandler<Handler, typename Future::ValueType>);
  }

  template <SomeFuture Future>
  auto Pipe(Future input) {
    ApplicableTo<Future>();
    return lazy::thunks::Map{std::move(input), std::move(handler)};
  }
};

}  // namespace fluent

//////////////////////////////////////////////////////////////////////

template <typename Handler>
auto Recover(Handler handler) {
  return fluent::Recover{std::move(handler)};
}

}  // namespace await::futures
