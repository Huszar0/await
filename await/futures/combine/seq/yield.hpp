#pragma once

#include <await/futures/impl/lazy/thunks/seq/hint.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace fluent {

struct [[nodiscard]] Hint {
  executors::SchedulerHint hint;

  template <SomeFuture Future>
  auto Pipe(Future input) {
    return lazy::thunks::Hint{std::move(input), hint};
  }
};

}  // namespace fluent

//////////////////////////////////////////////////////////////////////

inline auto Hint(executors::SchedulerHint hint) {
  return fluent::Hint{hint};
}

inline auto Yield() {
  return Hint(executors::SchedulerHint::Yield);
}

}  // namespace await::futures
