#pragma once

#include <await/futures/impl/lazy/thunks/seq/map.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/syntax/pipe.hpp>

#include <fallible/result/mappers.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace fluent {

template <typename Mapper>
struct [[nodiscard]] AndThen {
  Mapper mapper;

  AndThen(Mapper m)
      : mapper(std::move(m)) {
  }

  template <SomeFuture Future>
  static void ApplicableTo() {
    using T = ValueOf<Future>;
    static_assert(fallible::ValueHandler<Mapper, T>);
  }

  template <SomeFuture Future>
  auto Pipe(Future input) {
    ApplicableTo<Future>();
    return lazy::thunks::Map{std::move(input), std::move(mapper)};
  }
};

}  // namespace fluent

//////////////////////////////////////////////////////////////////////

template <typename Mapper>
auto AndThen(Mapper mapper) {
  return fluent::AndThen{std::move(mapper)};
}

}  // namespace await::futures
