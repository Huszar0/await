#pragma once

#include <await/futures/impl/lazy/thunks/seq/box.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace fluent {

struct [[nodiscard]] Box {
  template <SomeFuture Future>
  auto Pipe(Future input) {
    return lazy::thunks::Box{
        lazy::thunks::Wrap(std::move(input))};
  }
};

}  // namespace fluent

//////////////////////////////////////////////////////////////////////

inline auto Box() {
  return fluent::Box{};
}

//////////////////////////////////////////////////////////////////////

// Alias

template <typename V>
using BoxedFuture = lazy::thunks::Box<V>;

}  // namespace await::futures
