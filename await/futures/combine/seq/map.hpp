#pragma once

#include <await/futures/impl/lazy/thunks/seq/map.hpp>

#include <await/futures/syntax/pipe.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace fluent {

template <typename Mapper>
struct [[nodiscard]] Map {
  Mapper mapper;

  Map(Mapper m)
      : mapper(std::move(m)) {
  }

  template <SomeFuture Future>
  auto Pipe(Future input) {
    return lazy::thunks::Map{std::move(input), std::move(mapper)};
  }
};

}  // namespace fluent

//////////////////////////////////////////////////////////////////////

template <typename Mapper>
inline auto Map(Mapper mapper) {
  return fluent::Map{std::move(mapper)};
}

}  // namespace await::futures
