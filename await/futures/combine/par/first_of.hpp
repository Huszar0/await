#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/impl/par/first_of.hpp>

#include <await/futures/impl/lazy/thunks/seq/box.hpp>
#include <await/futures/impl/lazy/thunks/par/join.hpp>

#include <await/futures/impl/lazy/par/inputs/tuple.hpp>
#include <await/futures/impl/lazy/par/inputs/vector.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace detail {

template <class Inputs>
SomeFuture auto FirstOf(Inputs inputs) {
  using Combinator = lazy::thunks::Join<Inputs, combinators::FirstOfCombinator>;
  using ValueType = ValueOf<Combinator>;

  auto combinator = refer::New<Combinator>(std::move(inputs));
  return lazy::thunks::Box<ValueType>(std::move(combinator));
}

}  // namespace detail

//////////////////////////////////////////////////////////////////////

template <SomeFuture ... Fs>
SomeFuture auto FirstOf(Fs&& ... futures) {
  auto inputs = lazy::detail::ToTupleInputs(std::forward<Fs>(futures)...);
  return detail::FirstOf(std::move(inputs));
}

template <SomeFuture InputFuture>
SomeFuture auto FirstOf(std::vector<InputFuture> futures) {
  assert(!futures.empty());

  return detail::FirstOf(lazy::detail::ToVectorInputs(std::move(futures)));
}

}  // namespace await::futures
