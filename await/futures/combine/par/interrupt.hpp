#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/impl/lazy/thunks/par/interrupt.hpp>

#include <await/futures/combine/seq/box.hpp>
#include <await/futures/combine/seq/input.hpp>

#include <wheels/core/unit.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace detail {

template <SomeFuture MainFuture, SomeFuture InterruptFuture>
SomeFuture auto Interrupt(MainFuture main,
               InterruptFuture interrupt,
               fallible::Error error) {
  using T = ValueOf<MainFuture>;

  auto thunk = refer::New<lazy::thunks::WithInterrupt<MainFuture, InterruptFuture>>(
      std::move(main), std::move(interrupt), error);

  return lazy::thunks::Box<T>(std::move(thunk));
}

}  // namespace detail

//////////////////////////////////////////////////////////////////////

/*
 * Interrupt combinator
 *
 * Adds interruption condition for the given future
 *
 * Example:
 *
 * auto call = MakeRPC()
 *
 * auto call_with_timeout = Interrupt(
 *    std::move(call),
 *    futures::After(timeout),
 *    timeout_error);
 *
 */

template <SomeFuture MainFuture, SomeFuture InterruptFuture>
SomeFuture auto Interrupt(MainFuture future, InterruptFuture interrupt, fallible::Error error) {
  static_assert(
      std::same_as<typename InterruptFuture::ValueType, wheels::Unit>);

  return detail::Interrupt(
      std::move(future) | ToInput(),
      std::move(interrupt) | ToInput(),
      error);
}

}  // namespace await::futures
