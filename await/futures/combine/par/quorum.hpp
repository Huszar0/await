#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/impl/par/quorum.hpp>

#include <await/futures/impl/lazy/thunks/par/join.hpp>
#include <await/futures/impl/lazy/thunks/seq/box.hpp>

#include <await/futures/impl/lazy/par/inputs/tuple.hpp>
#include <await/futures/impl/lazy/par/inputs/vector.hpp>

#include <await/futures/make/just.hpp>
#include <await/futures/combine/seq/box.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace detail {

template <class Inputs>
SomeFuture auto Quorum(Inputs inputs, size_t threshold) {
  using Combinator = lazy::thunks::Join<Inputs, combinators::QuorumCombinator>;
  using ValueType = ValueOf<Combinator>;

  // Corner cases
  if (threshold == 0) {
    return futures::Value<ValueType>({}) | futures::Box();
  }
  if (inputs.Count() < threshold) {
    WHEELS_PANIC(
        "Number of inputs < required threshold, output future never completes");
  }

  auto combinator = refer::New<Combinator>(std::move(inputs), threshold);
  return lazy::thunks::Box<ValueType>(std::move(combinator));
}

}  // namespace detail

//////////////////////////////////////////////////////////////////////

// Quorum combinator

//////////////////////////////////////////////////////////////////////

template <SomeFuture InputFuture>
SomeFuture auto Quorum(std::vector<InputFuture> futures, size_t threshold) {
  return detail::Quorum(lazy::detail::ToVectorInputs(std::move(futures)), threshold);
}

}  // namespace await::futures

