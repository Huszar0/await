#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/impl/par/join.hpp>
#include <await/futures/impl/lazy/thunks/seq/input.hpp>
#include <await/futures/impl/lazy/thunks/par/join.hpp>

#include <await/futures/impl/lazy/par/inputs/tuple.hpp>
#include <await/futures/impl/lazy/par/inputs/vector.hpp>

#include <await/futures/make/just.hpp>

#include <await/futures/combine/seq/box.hpp>
#include <await/futures/combine/seq/unit.hpp>

#include <fallible/result/make.hpp>

#include <vector>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace detail {

template <class Inputs>
SomeFuture auto Join(Inputs inputs) {
  using Combinator = lazy::thunks::Join<Inputs, combinators::JoinCombinator>;
  using ValueType = ValueOf<Combinator>;

  auto combinator = refer::New<Combinator>(std::move(inputs));
  return lazy::thunks::Box<ValueType>(std::move(combinator));
}

}  // namespace detail

//////////////////////////////////////////////////////////////////////

// Join combinator

//////////////////////////////////////////////////////////////////////

template <SomeFuture ... Fs>
UnitFuture auto Join(Fs&& ... futures) {
  auto inputs = lazy::detail::ToTupleInputs(ToUnit(std::forward<Fs>(futures))...);
  return detail::Join(std::move(inputs));
}

// Vector of type-erased inputs

template <SomeFuture InputFuture>
UnitFuture auto Join(std::vector<InputFuture> futures) {
  if (futures.empty()) {
    return futures::Just() | futures::Box();
  }

  return detail::Join(
      lazy::detail::ToVectorInputs(std::move(futures)));
}

}  // namespace await::futures
