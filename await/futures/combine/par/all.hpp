#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/impl/par/all.hpp>

#include <await/futures/impl/lazy/thunks/par/join.hpp>

#include <await/futures/impl/lazy/par/inputs/tuple.hpp>
#include <await/futures/impl/lazy/par/inputs/vector.hpp>

#include <await/futures/types/traits/value_of.hpp>

#include <await/futures/make/value.hpp>
#include <await/futures/combine/seq/box.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

namespace detail {

template <class Inputs>
SomeFuture auto All(Inputs inputs) {
  using Combinator = lazy::thunks::Join<Inputs, combinators::AllCombinator>;
  using ValueType = ValueOf<Combinator>;

  auto combinator = refer::New<Combinator>(std::move(inputs));
  return lazy::thunks::Box<ValueType>(std::move(combinator));
}

}  // namespace detail

//////////////////////////////////////////////////////////////////////

template <SomeFuture ... Fs>
SomeFuture auto All(Fs&& ... futures) {
  auto inputs = lazy::detail::ToTupleInputs(std::forward<Fs>(futures)...);
  return detail::All(std::move(inputs));
}

template <SomeFuture InputFuture>
SomeFuture auto All(std::vector<InputFuture> futures) {
  using T = ValueOf<InputFuture>;

  if (futures.empty()) {
    return futures::Value(std::vector<T>{}) | futures::Box();
  }

  return detail::All(lazy::detail::ToVectorInputs(std::move(futures)));
}

}  // namespace await::futures

