#pragma once

#include <await/futures/impl/consume/consumers/linked.hpp>

#include <await/futures/impl/lazy/thunk.hpp>
#include <await/futures/impl/eager/state.hpp>

#include <await/futures/impl/race/rendezvous.hpp>

#include <await/cancel/states/strand.hpp>

#include <refer/ref_counted.hpp>

namespace await::futures::eager {

namespace states {

// [Await.FutureState]
template <lazy::Thunk Thunk>
class Evaluate final : public IState<typename Thunk::ValueType>,
                       public IConsumer<typename Thunk::ValueType>,
                       public cancel::detail::StrandStateBase,
                       public refer::RefCounted<Evaluate<Thunk>> {
  using ValueType = typename Thunk::ValueType;

 public:
  Evaluate(Thunk&& thunk)
      : thunk_(std::move(thunk)),
        executor_(&thunk_.Executor()),
        execution_hint_(thunk_.SchedulerHint()),
        context_(thunk_.UserContext()) {
  }

  // One-shot
  void StartEvaluation() {
    thunk_.Start(AsConsumer());
  }

  // IFutureState

  // Consume

  void SetConsumer(IConsumer<ValueType>* consumer) override {
    rendezvous_.SetConsumer(consumers::Link(consumer, CancelSource()));
  }

  void Drop() override {
    RequestCancel();
  }

  // Polling

  race::Ready GetReadyState() const override {
    return rendezvous_.GetState();
  }

  // Execution context

  executors::IExecutor& Executor() const override {
    return *executor_;
  }

  executors::SchedulerHint SchedulerHint() const override {
    return execution_hint_;
  }

  carry::Context UserContext() const override {
    return context_;
  }

 private:
  cancel::Source CancelSource() {
    return cancel::Source::FromState(this);
  }

  // IConsumer<ValueType>

  cancel::Token CancelToken() override {
    return cancel::Token::FromState(this);
  }

  void Consume(fallible::Result<ValueType> result) noexcept override {
    rendezvous_.SetResult(std::move(result));
    CompleteConsume();
  }

  void Cancel() noexcept override {
    rendezvous_.SetCancel();
    CompleteConsume();
  }

  // Memory management

  IConsumer<ValueType>* AsConsumer() {
    this->AddRef();
    return this;
  }

  void CompleteConsume() {
    this->ReleaseRef();  // Memory management
  }

 private:
  Thunk thunk_;
  race::Rendezvous<ValueType> rendezvous_;

  // Task context
  executors::IExecutor* executor_;
  executors::SchedulerHint execution_hint_;
  carry::Context context_;
};

}  // namespace states

}  // namespace await::futures::eager
