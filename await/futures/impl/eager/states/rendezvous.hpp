#pragma once

#include <await/futures/impl/eager/state.hpp>

#include <await/futures/impl/race/rendezvous.hpp>

#include <carry/empty.hpp>
#include <await/executors/impl/inline.hpp>

#include <optional>

namespace await::futures::eager {

namespace states {

//////////////////////////////////////////////////////////////////////

// [Await.FutureState]
template <typename T, class CancelStateBase>
class RendezvousStateBase : public IState<T>,
                            public CancelStateBase {
 protected:
  using CancelBase = CancelStateBase;

 public:
  RendezvousStateBase()
      : executor_(&executors::Inline()),
        context_(carry::Empty()) {
  }

  // Subscription

  void SetConsumer(IConsumer<T>* consumer) override {
    rendezvous_.SetConsumer(consumers::Link(consumer, CancelSource()));
  }

  void Drop() override {
    CancelStateBase::RequestCancel();
  }

  // Polling

  race::Ready GetReadyState() const override {
    return rendezvous_.GetState();
  }

  // Task context

  executors::IExecutor& Executor() const override {
    return *executor_;
  }

  carry::Context UserContext() const override {
    return context_;
  }

  executors::SchedulerHint SchedulerHint() const override {
    return executors::SchedulerHint::UpToYou;  // TODO
  }

  // Cancellation

  cancel::Source CancelSource() {
    return cancel::Source::FromState(this);
  }

  cancel::Token CancelToken() {
    return cancel::Token::FromState(this);
  }

  // Workarounds

  void SetExecutor(executors::IExecutor& executor) {
    executor_ = &executor;
  }

  void SetUserContext(carry::Context context) {
    context_ = std::move(context);
  }

  // For Promise

  void SetResult(fallible::Result<T> result) {
    rendezvous_.SetResult(std::move(result));
  }

  void SetError(fallible::Error error) {
    rendezvous_.SetError(std::move(error));
  }

  void SetCancel() {
    rendezvous_.SetCancel();
  }

 private:
  race::Rendezvous<T> rendezvous_;

  executors::IExecutor* executor_;
  carry::Context context_;
};

}  // namespace states

}  // namespace await::futures::eager
