#pragma once

#include <await/futures/impl/eager/states/rendezvous.hpp>

#include <await/cancel/states/fork.hpp>

namespace await::futures::eager {

namespace states {

template <typename T>
class Contract final
    : public RendezvousStateBase<T, cancel::detail::ForkStateBase>,
      public refer::RefCounted<Contract<T>> {
  using Self = Contract<T>;

 public:
  void SetResult(fallible::Result<T> result) {
    ReleaseCancelHandlers();
    RendezvousStateBase<T, cancel::detail::ForkStateBase>::SetResult(
        std::move(result));
  }

 private:
  void ReleaseCancelHandlers() {
    Self::ReleaseHandlers();
  }
};

}  // namespace states

}  // namespace await::futures::eager
