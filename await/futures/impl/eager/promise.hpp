#pragma once

#include <await/futures/impl/eager/states/contract.hpp>

#include <await/cancel/detail/handler.hpp>

namespace await::futures::eager {

//////////////////////////////////////////////////////////////////////

template <typename T>
class Promise {
  using State = states::Contract<T>;
  using StateRef = refer::Ref<State>;

 public:
  // Non-copyable
  Promise(const Promise<T>&) = delete;
  Promise& operator=(const Promise<T>&) = delete;

  // Movable
  Promise(Promise<T>&&) = default;
  Promise& operator=(Promise<T>&&) = default;

  bool IsValid() const {
    return state_.IsValid();
  }

  void SetValue(T value) && {
    ReleaseState()->SetResult(fallible::Ok(std::move(value)));
  }

  void SetError(fallible::Error error) && {
    ReleaseState()->SetResult(fallible::Fail(std::move(error)));
  }

  void Set(fallible::Result<T> result) && {
    ReleaseState()->SetResult(std::move(result));
  }

  // Cancel signal

  bool CancelRequested() const {
    return state_->CancelRequested();
  }

  cancel::Token CancelToken() {
    return cancel::Token::FromState(state_);
  }

  template <typename F>
  void CancelSubscribe(executors::IExecutor& executor, F handler) {
    auto handler_state = refer::New<cancel::detail::Handler<F>>(
        std::move(handler), executor, state_->UserContext());

    state_->AddHandler(handler_state.Release());
  }

  void Detach() && {
    state_.Reset();
  }

  // Cancellation

  void Cancel() && {
    ReleaseState()->SetCancel();
  }

  ~Promise() {
    if (IsValid()) {
      if (CancelRequested()) {
        state_->SetCancel();
      } else {
        // Or panics?
        state_.Reset();
      }
    }
  }

  // Private!
  explicit Promise(StateRef state)
      : state_(std::move(state)) {
  }

 private:
  StateRef ReleaseState() {
    WHEELS_ASSERT(state_.IsValid(), "Promise is invalid");
    return std::move(state_);
  }

 private:
  StateRef state_;
};

}  // namespace await::futures::eager
