#pragma once

#include <await/futures/impl/consume/consumer.hpp>

namespace await::futures::consumers {

template <typename T, typename Tag>
struct ITaggedConsumer : IConsumer<T> {
  // IConsumer

  void Consume(fallible::Result<T> result) noexcept override {
    ConsumeTagged(std::move(result), Tag{});
  }

  void Cancel() noexcept override {
    CancelTagged(Tag{});
  }

  virtual void ConsumeTagged(fallible::Result<T> result, Tag) noexcept = 0;
  virtual void CancelTagged(Tag) noexcept = 0;
};

}  // namespace await::futures::consumers
