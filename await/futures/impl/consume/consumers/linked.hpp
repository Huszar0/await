#pragma once

#include <await/cancel/token.hpp>
#include <await/futures/impl/consume/consumer.hpp>

namespace await::futures::consumers {

//////////////////////////////////////////////////////////////////////

template <typename T>
class LinkedConsumer {
 public:
  LinkedConsumer() = default;

  LinkedConsumer(IConsumer<T>* consumer, cancel::Source&& source);

  // Non-copyable
  LinkedConsumer(const LinkedConsumer&) = delete;
  LinkedConsumer& operator=(const LinkedConsumer&) = delete;

  // Movable
  LinkedConsumer(LinkedConsumer&&);
  LinkedConsumer& operator=(LinkedConsumer&&);

  ~LinkedConsumer() {
    assert(!IsValid());
  }

  IConsumer<T>* operator->() {
    return ReleaseConsumer();
  }

 private:
  IConsumer<T>* ReleaseConsumer();

  bool IsValid() const {
    return consumer_ != nullptr;
  }

 private:
  IConsumer<T>* consumer_ = nullptr;
  cancel::Link link_;
};

//////////////////////////////////////////////////////////////////////

template <typename T>
LinkedConsumer<T> Link(IConsumer<T>* consumer, cancel::Source&& source) {
  return {consumer, std::move(source)};
}

}  // namespace await::futures::consumers

#include <await/futures/impl/consume/consumers/linked-inl.hpp>
