#pragma once

#include <await/cancel/token.hpp>

#include <fallible/result/result.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

// Represents blocked thread, suspended fiber
// or asynchronous Future mapper

template <typename T>
struct IConsumer {
  virtual ~IConsumer() = default;

  // Producer -outcome-> Consumer

  // [Dev] Consume/Cancel are release-reference operation!
  virtual void Consume(fallible::Result<T>) noexcept = 0;
  virtual void Cancel() noexcept = 0;

  // Consumer -cancellation request-> Producer

  virtual cancel::Token CancelToken() = 0;
};

}  // namespace await::futures
