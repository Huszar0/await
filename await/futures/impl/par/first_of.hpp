#pragma once

#include <await/futures/impl/par/detail/done_or_count.hpp>

#include <fallible/result/make.hpp>

#include <vector>
#include <optional>

namespace await::futures {

namespace combinators {

//////////////////////////////////////////////////////////////////////

template <typename InputType>
class FirstOfCombinator {
 public:
  using OutputType = InputType;

  using MaybeResult = std::optional<fallible::Result<OutputType>>;

 public:
  FirstOfCombinator(size_t inputs)
      : inputs_(inputs) {
  }

  MaybeResult CombineValue(InputType input) {
    if (bool first = state_.Done()) {
      return fallible::Ok(std::move(input));
    } else {
      return std::nullopt;  // Already completed
    }
  }

  MaybeResult CombineError(fallible::Error error) {
    if (state_.IncrementAndFetch() == inputs_) {
      // Last error
      return fallible::Fail(std::move(error));
    } else {
      return std::nullopt;
    }
  }

  bool CombineCancel() {
    return state_.Done();
  }

  bool IsCompleted() const {
    return true;  // TODO
  }

 private:
  const size_t inputs_;

  detail::DoneOrCountStateMachine state_;
};

}  // namespace combinators

}  // namespace await::futures
