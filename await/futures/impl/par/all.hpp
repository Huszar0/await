#pragma once

#include <fallible/result/result.hpp>
#include <fallible/result/make.hpp>

#include <twist/ed/spin/lock.hpp>

#include <cassert>
#include <optional>
#include <vector>
#include <utility>

namespace await::futures {

namespace combinators {

template <typename InputType>
class AllCombinator {
 public:
  using OutputType = std::vector<InputType>;

  using MaybeResult = std::optional<fallible::Result<OutputType>>;

 public:
  AllCombinator(size_t inputs)
      : inputs_(inputs) {
  }

  MaybeResult CombineValue(InputType input) {
    std::unique_lock locker(mutex_);

    values_.push_back(std::move(input));

    if (values_.size() == inputs_) {
      completed_ = true;
      return fallible::Ok(std::move(values_));
    } else {
      return std::nullopt;
    }
  }

  MaybeResult CombineError(fallible::Error error) {
    std::unique_lock locker(mutex_);

    if (!std::exchange(completed_, true)) {
      return fallible::Fail(error);
    } else {
      return std::nullopt;
    }
  }

  bool CombineCancel() {
    std::unique_lock locker(mutex_);

    return !std::exchange(completed_, true);
  }

  bool IsCompleted() const {
    return completed_;
  }

 private:
  const size_t inputs_;

  mutable twist::ed::SpinLock mutex_;
  // All values
  std::vector<InputType> values_;
  bool completed_{false};
};

}  // namespace combinators

}  // namespace await::futures
