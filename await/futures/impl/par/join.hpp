#pragma once

#include <await/futures/impl/par/detail/done_or_count.hpp>

#include <fallible/result/make.hpp>

#include <cassert>
#include <optional>

namespace await::futures {

namespace combinators {

//////////////////////////////////////////////////////////////////////

template <typename InputType>
class JoinCombinator {
 public:
  using OutputType = wheels::Unit;

  using MaybeResult = std::optional<fallible::Status>;

 public:
  JoinCombinator(size_t inputs)
      : inputs_(inputs) {
  }

  MaybeResult CombineValue(InputType) {
    if (state_.IncrementAndFetch() == inputs_) {
      return fallible::Ok();
    } else {
      return std::nullopt;
    }
  }

  MaybeResult CombineError(fallible::Error error) {
    if (state_.Done()) {
      // First error
      return fallible::Fail(std::move(error));
    } else {
      return std::nullopt;
    }
  }

  bool CombineCancel() {
    return state_.Done();
  }

  bool IsCompleted() const {
    return true;  // TODO
  }

 private:
  const size_t inputs_;

  detail::DoneOrCountStateMachine state_;
};

}  // namespace combinators

}  // namespace await::futures
