#pragma once

#include <fallible/result/result.hpp>
#include <fallible/result/make.hpp>

#include <twist/ed/spin/lock.hpp>

#include <cassert>
#include <optional>
#include <vector>
#include <utility>

namespace await::futures {

namespace combinators {

template <typename InputType>
class QuorumCombinator {
 public:
  using OutputType = std::vector<InputType>;

  using MaybeResult = std::optional<fallible::Result<OutputType>>;

 public:
  QuorumCombinator(size_t inputs, size_t threshold)
      : inputs_(inputs),
        threshold_(threshold) {
  }

  MaybeResult CombineValue(InputType input) {
    std::lock_guard locker(mutex_);

    if (completed_) {
      return std::nullopt;  // Already
    }

    values_.push_back(std::move(input));

    if (values_.size() == threshold_) {
      // Quorum reached
      completed_ = true;
      return fallible::Ok(std::move(values_));
    }

    return std::nullopt;
  }

  // Returns true on failure
  MaybeResult CombineError(fallible::Error error) {
    std::lock_guard locker(mutex_);

    if (completed_) {
      return std::nullopt;  // Already
    }

    ++errors_;
    if (ImpossibleToReachQuorum()) {
      completed_ = true;
      return fallible::Fail(std::move(error));
    }

    return std::nullopt;
  }

  bool CombineCancel() {
    std::lock_guard locker(mutex_);

    return !std::exchange(completed_, true);
  }

  bool IsCompleted() const {
    std::lock_guard locker(mutex_);

    return completed_;
  }

 private:
  bool ImpossibleToReachQuorum() const {
    return errors_ + threshold_ > inputs_;
  }

 private:
  const size_t inputs_;
  const size_t threshold_;

  mutable twist::ed::SpinLock mutex_;
  std::vector<InputType> values_;
  size_t errors_{0};

  bool completed_{false};
};

}  // namespace combinators

}  // namespace await::futures
