#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

#include <await/cancel/never.hpp>

namespace await::futures::lazy {

namespace terminators {

template <Thunk Thunk>
class Sink : private IConsumer<typename Thunk::ValueType> {
  using ValueType = typename Thunk::ValueType;

 public:
  Sink(Thunk&& thunk)
      : thunk_(std::move(thunk)) {
  }

  // Non-copyable
  Sink(const Sink&) = delete;

  // Movable
  Sink(Sink&& that) = default;

  void Start() {
    thunk_.Start(/*consumer=*/this);
  }

 private:
  // IConsumer<ValueType>

  cancel::Token CancelToken() override {
    return cancel::Never();
  }

  void Consume(fallible::Result<ValueType>) noexcept override {
    // Ignore
  }

  void Cancel() noexcept override {
    // Ignore
  }

 private:
  Thunk thunk_;
};

}  // namespace terminators

}  // namespace await::futures::lazy
