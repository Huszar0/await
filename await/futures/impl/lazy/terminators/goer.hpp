#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

#include <await/cancel/never.hpp>

namespace await::futures::lazy {

namespace terminators {

template <Thunk Detached>
class Goer : private IConsumer<typename Detached::ValueType> {
  using ValueType = typename Detached::ValueType;

 public:
  Goer(Detached&& thunk)
      : thunk_(std::move(thunk)) {
  }

  void Start() {
    thunk_.Start(/*consumer=*/this);
  }

 private:
  // IConsumer<ValueType>

  cancel::Token CancelToken() override {
    return cancel::Never();
  }

  void Consume(fallible::Result<ValueType>) noexcept override {
    Done();
  }

  void Cancel() noexcept override {
    Done();
  }

  void Done() {
    delete this;
  }

 private:
  Detached thunk_;
};

}  // namespace terminators

}  // namespace await::futures::lazy
