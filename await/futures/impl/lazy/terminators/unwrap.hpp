#pragma once

#include <await/futures/impl/consume/consumer.hpp>

#include <await/cancel/never.hpp>

#include <wheels/core/assert.hpp>

#include <optional>

namespace await::futures::lazy {

namespace terminators {

template <typename T>
class UnwrapConsumer : public IConsumer<T> {
 public:
  void Consume(fallible::Result<T> result) noexcept override {
    result_ = std::move(result);
  }

  void Cancel() noexcept override {
    WHEELS_PANIC("Failed to consume result: Cancelled");
  }

  fallible::Result<T> ExpectResult() {
    WHEELS_VERIFY(result_.has_value(), "Failed to consume result: Not ready");
    return std::move(*result_);
  }

  cancel::Token CancelToken() override {
    return cancel::Never();
  }

 private:
  std::optional<fallible::Result<T>> result_;
};

}  // namespace terminators

}  // namespace await::futures::lazy
