#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/impl/lazy/thunks/seq/input.hpp>

// Containers
#include <tuple>

namespace await::futures::lazy {

namespace detail {

//////////////////////////////////////////////////////////////////////

template <typename ... Fs>
class TupleInputs {
  using Tuple = std::tuple<Fs...>;
  using First = std::tuple_element_t<0, Tuple>;

 public:
  using ValueType = typename First::ValueType;

 public:
  TupleInputs(Fs&& ... fs)
      : futures_(std::forward<Fs>(fs)...) {
  }

  static size_t Count() {
    return sizeof...(Fs);
  }

  template <typename Combinator>
  void Start(Combinator* combinator) {
    auto consume = [combinator](auto&& ...fs) {
      (..., std::move(fs).Start(combinator->AsConsumer()));
    };

    std::apply(consume, futures_);
  }

 private:
  std::tuple<Fs...> futures_;
};

//////////////////////////////////////////////////////////////////////

template <SomeFuture ... Fs>
auto ToTupleInputs(Fs&& ... futures) {
  // Add cancel state
  return TupleInputs(thunks::Input{std::forward<Fs>(futures)}...);
}

}  // namespace detail

}  // namespace await::futures::lazy
