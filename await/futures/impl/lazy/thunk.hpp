#pragma once

#include <await/futures/impl/consume/consumer.hpp>

#include <await/executors/core/executor.hpp>
#include <carry/context.hpp>

namespace await::futures::lazy {

// https://wiki.haskell.org/Thunk

// A thunk is an object that represents a value (actually, a result)
// that is yet to be evaluated

// Evaluation is only triggered by explicit consume-like operation:
// - synchronous (Await) or
// - asynchronous (promotion of lazy future to eager)

// Consume operation is represented by IConsumer interface

// clang-format off

template <typename T>
concept Thunk = requires (T thunk, IConsumer<typename T::ValueType>* consumer) {
  typename T::ValueType;

  // Start evaluation
  thunk.Start(consumer);

  // Execution context
  { thunk.Executor() } -> std::same_as<executors::IExecutor&>;
  { thunk.SchedulerHint() } -> std::same_as<executors::SchedulerHint>;
  { thunk.UserContext() } -> std::same_as<carry::Context>;
};

// clang-format on

}  // namespace await::futures::lazy
