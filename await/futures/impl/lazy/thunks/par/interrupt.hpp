#pragma once

#include <await/futures/impl/lazy/thunk.hpp>
#include <await/futures/impl/lazy/thunks/seq/box.hpp>

#include <await/futures/impl/consume/consumers/linked.hpp>
#include <await/futures/impl/consume/consumers/tagged.hpp>

#include <await/cancel/states/fork.hpp>

#include <fallible/result/make.hpp>

#include <refer/ref_counted.hpp>

#include <twist/ed/stdlike/atomic.hpp>

namespace await::futures::lazy {

namespace thunks {

//////////////////////////////////////////////////////////////////////

class InterruptStateMachine {
  struct States {
    enum _ {
      Initial = 0,
      Completed = 1,
      Interrupted = 2,
      Cancelled = 4
    };
  };

 public:
  bool Complete() {
    return state_.exchange(States::Completed) == States::Initial;
  }

  bool Interrupt() {
    return state_.exchange(States::Interrupted) == States::Initial;
  }

  bool Cancel() {
    return state_.exchange(States::Cancelled) == States::Initial;
  }

 private:
  twist::ed::stdlike::atomic<uint64_t> state_{States::Initial};
};

//////////////////////////////////////////////////////////////////////

struct InterruptTags {
  struct Result {};
  struct Interrupt {};
};

//////////////////////////////////////////////////////////////////////

// [Await.FutureState]
template <typename Future, typename InterruptFuture>
class [[nodiscard]] WithInterrupt final
    : public IBoxedThunk<typename Future::ValueType>,
      public futures::consumers::ITaggedConsumer<typename Future::ValueType, InterruptTags::Result>,
      public futures::consumers::ITaggedConsumer<wheels::Unit, InterruptTags::Interrupt>,
      public cancel::detail::ForkStateBase,
      public refer::RefCounted<WithInterrupt<Future, InterruptFuture>> {
  using Self = WithInterrupt<Future, InterruptFuture>;

 public:
  using ValueType = typename Future::ValueType;

 public:
  WithInterrupt(Future future, InterruptFuture interrupt,
                fallible::Error error)
      : future_(std::move(future)),
        interrupt_(std::move(interrupt)),
        error_(std::move(error)),
        executor_(future_.Executor()),
        context_(future_.UserContext()) {
  }

  ~WithInterrupt() {
    Self::ReleaseHandlers();
  }

  // Execution context

  executors::IExecutor& Executor() const override {
    return executor_;
  }

  executors::SchedulerHint SchedulerHint() const override {
    return executors::SchedulerHint::UpToYou;
  }

  carry::Context UserContext() const override {
    return context_;
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) override {
    consumer_ = consumers::Link(consumer, CancelSource());

    std::move(future_).Start(AsResultConsumer());
    std::move(interrupt_).Start(AsInterruptConsumer());
  }

  // Result consumer

  IConsumer<ValueType>* AsResultConsumer() {
    Self::AddRef();
    return static_cast<futures::consumers::ITaggedConsumer<ValueType, InterruptTags::Result>*>(this);
  }

  void ConsumeTagged(fallible::Result<ValueType> result,
                     InterruptTags::Result) noexcept override {
    CombineComplete(std::move(result));
    Self::ReleaseRef();
  }

  void CancelTagged(InterruptTags::Result) noexcept override {
    CombineCancel();
    Self::ReleaseRef();
  }

  // Interrupt consumer

  IConsumer<wheels::Unit>* AsInterruptConsumer() {
    Self::AddRef();
    return static_cast<
        futures::consumers::ITaggedConsumer<wheels::Unit, InterruptTags::Interrupt>*>(this);
  }

  void ConsumeTagged(fallible::Status,
                     InterruptTags::Interrupt) noexcept override {
    CombineInterrupt();
    Self::ReleaseRef();
  }

  void CancelTagged(InterruptTags::Interrupt) noexcept override {
    CombineCancel();
    Self::ReleaseRef();
  }

  cancel::Token CancelToken() override {
    return cancel::Token::FromState(this);
  }

 private:
  cancel::Source CancelSource() {
    return cancel::Source::FromState(this);
  }

  void CombineComplete(fallible::Result<ValueType> result) {
    if (bool first = state_.Complete()) {
      // Propagate result upstream
      consumer_->Consume(std::move(result));
    }
  }

  void CombineInterrupt() {
    if (bool first = state_.Interrupt()) {
      // Structured concurrency time!

      // Propagate cancel downstream
      Self::RequestCancel();

      consumer_->Consume(fallible::Fail(error_));
    }
  }

  void CombineCancel() {
    // Cancel from upstream
    assert(Self::CancelRequested());

    if (bool first = state_.Cancel()) {
      consumer_->Cancel();
    }
  }

 private:
  Future future_;
  InterruptFuture interrupt_;
  fallible::Error error_;

  executors::IExecutor& executor_;
  carry::Context context_;

  InterruptStateMachine state_;

  consumers::LinkedConsumer<ValueType> consumer_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
