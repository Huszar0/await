#pragma once

#include <await/futures/impl/consume/consumer.hpp>
#include <await/futures/impl/consume/consumers/linked.hpp>
#include <await/futures/impl/lazy/thunk.hpp>

#include <await/futures/impl/lazy/thunks/seq/box.hpp>

#include <await/cancel/states/fork.hpp>

#include <await/executors/impl/inline.hpp>
#include <carry/empty.hpp>

#include <refer/ref_counted.hpp>

namespace await::futures::lazy {

namespace thunks {

// Join combinator with homogenous inputs

template <typename Inputs, template <typename> class Combinator>
class [[nodiscard]] Join
    : public IBoxedThunk<typename Combinator<typename Inputs::ValueType>::OutputType>,
      public IConsumer<typename Inputs::ValueType>,
      public cancel::detail::ForkStateBase,
      public refer::RefCounted<thunks::Join<Inputs, Combinator>> {
 public:
  using Self = thunks::Join<Inputs, Combinator>;

  using InputType = typename Inputs::ValueType;
  using OutputType = typename Combinator<InputType>::OutputType;

  using ValueType = OutputType;

 public:
  // Arguments for combinator
  template <typename... Args>
  Join(Inputs inputs, Args&&... args)
      : inputs_(std::move(inputs)),
        combinator_(inputs_.Count(), std::forward<Args>(args)...) {
  }

  ~Join() {
    assert(combinator_.IsCompleted());
  }

  // Execution context

  executors::IExecutor& Executor() const override {
    return executors::Inline();
  }

  executors::SchedulerHint SchedulerHint() const override {
    return executors::SchedulerHint::UpToYou;
  }

  carry::Context UserContext() const override {
    return carry::Empty();
  }

  // Lazy protocol

  void Start(IConsumer<OutputType>* consumer) override {
    consumer_ = consumers::Link(consumer, CancelSource());
    inputs_.Start(this);
  }
  
  // IConsumer<InputType>

  void Consume(fallible::Result<InputType> input) noexcept override {
    Combine(std::move(input));
    ReleaseRef();
  }

  void Cancel() noexcept override {
    CombineCancel();
    ReleaseRef();
  }

  cancel::Token CancelToken() override {
    return cancel::Token::FromState(this);
  }

  // Inputs

  IConsumer<InputType>* AsConsumer() {
    AddRef();
    return this;
  }

 private:
  cancel::Source CancelSource() {
    return cancel::Source::FromState(this);
  }

  void Combine(fallible::Result<InputType> input) noexcept {
    if (input.IsOk()) {
      if (auto output = combinator_.CombineValue(*input)) {
        assert(output->IsOk());
        Complete(std::move(*output));
      }
    } else {
      if (auto output = combinator_.CombineError(input.Error())) {
        assert(output->Failed());
        Complete(std::move(*output));
      }
    }
  }

  void CombineCancel() {
    // Cancellation from upstream
    assert(Self::CancelRequested());

    if (combinator_.CombineCancel()) {
      consumer_->Cancel();
    }
  }

  void Complete(fallible::Result<ValueType> output) {
    // Send cancellation request downstream
    Self::RequestCancel();

    // Propagate result upstream
    consumer_->Consume(output);
  }

 private:
  Inputs inputs_;
  Combinator<InputType> combinator_;
  consumers::LinkedConsumer<OutputType> consumer_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
