#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

#include <await/timers/core/timer_keeper.hpp>

#include <await/executors/impl/inline.hpp>

#include <carry/empty.hpp>

#include <fallible/result/make.hpp>

#include <wheels/core/unit.hpp>

namespace await::futures::lazy {

namespace thunks {

class [[nodiscard]] After final : public timers::TimerBase {
 public:
  using ValueType = wheels::Unit;

 public:
  After(timers::Delay delay)
      : delay_(delay) {
  }

  // Non-copyable
  After(const After&) = delete;

  // Movable
  After(After&&) = default;

  // timers::ITimer

  std::chrono::milliseconds Delay() const override {
    return delay_.millis;
  }

  bool Periodic() const override {
    return false;
  }

  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

  // Execution context

  static executors::IExecutor& Executor() {
    return executors::Inline();
  }

  static executors::SchedulerHint SchedulerHint() {
    return executors::SchedulerHint::New;
  }

  static carry::Context UserContext() {
    return carry::Empty();
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;
    delay_.timer_keeper.AddTimer(this);
  }

  // timers::ITimerHandler

  void Ready() noexcept override {
    consumer_->Consume(fallible::Ok());
  }

 private:
  timers::Delay delay_;

  IConsumer<ValueType>* consumer_{nullptr};
};

}  // namespace thunks

}  // namespace await::futures::lazy
