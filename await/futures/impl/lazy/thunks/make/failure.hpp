#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

#include <fallible/result/make.hpp>

#include <await/executors/impl/inline.hpp>
#include <carry/empty.hpp>

namespace await::futures::lazy {

namespace thunks {

//////////////////////////////////////////////////////////////////////

template <typename V>
struct [[nodiscard]] Failure {
  using ValueType = V;

  Failure(fallible::Error e)
      : error(std::move(e)) {
  }

  // Non-copyable
  Failure(const Failure&) = delete;

  // Movable
  Failure(Failure&&) = default;

  // Execution context

  static executors::IExecutor& Executor() {
    return executors::Inline();
  }

  static executors::SchedulerHint SchedulerHint() {
    return executors::SchedulerHint::New;
  }

  static carry::Context UserContext() {
    return carry::Empty();
  }

  // Lazy protocol

  void Start(IConsumer<V>* consumer) {
    consumer->Consume(fallible::Fail(std::move(error)));
  }

  fallible::Error error;
};

}  // namespace thunks

}  // namespace await::futures::lazy
