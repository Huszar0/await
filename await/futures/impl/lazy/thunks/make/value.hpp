#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

#include <fallible/result/make.hpp>

#include <await/executors/impl/inline.hpp>
#include <carry/empty.hpp>

namespace await::futures::lazy {

namespace thunks {

template <typename T>
class [[nodiscard]] Value {
 public:
  using ValueType = T;

 public:
  Value(T value)
      : value_(std::move(value)) {
  }

  // Non-copyable
  Value(const Value&) = delete;

  // Movable
  Value(Value&&) = default;

  // Execution context

  static executors::IExecutor& Executor() {
    return executors::Inline();
  }

  static executors::SchedulerHint SchedulerHint() {
    return executors::SchedulerHint::New;
  }

  static carry::Context UserContext() {
    return carry::Empty();
  }

  // Lazy protocol

  void Start(IConsumer<T>* consumer) {
    consumer->Consume(fallible::Ok(std::move(value_)));
  }

 private:
  T value_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
