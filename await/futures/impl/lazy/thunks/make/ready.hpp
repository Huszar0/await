#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

#include <fallible/result/make.hpp>

#include <await/executors/impl/inline.hpp>
#include <carry/empty.hpp>

namespace await::futures::lazy {

namespace thunks {

//////////////////////////////////////////////////////////////////////

template <typename V>
class [[nodiscard]] Ready {
 public:
  using ValueType = V;

 public:
  Ready(fallible::Result<V>&& result)
      : result_(std::move(result)) {
  }

  // Non-copyable
  Ready(const Ready&) = delete;

  // Movable
  Ready(Ready&&) = default;

  // Execution context

  static executors::IExecutor& Executor() {
    return executors::Inline();
  }

  static executors::SchedulerHint SchedulerHint() {
    return executors::SchedulerHint::New;
  }

  static carry::Context UserContext() {
    return carry::Empty();
  }

  // Lazy protocol

  void Start(IConsumer<V>* consumer) {
    consumer->Consume(std::move(result_));
  }

 private:
  fallible::Result<V> result_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
