#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

#include <await/executors/impl/prison.hpp>
#include <carry/empty.hpp>

#include <await/cancel/detail/handler.hpp>

#include <wheels/core/unit.hpp>

namespace await::futures::lazy {

namespace thunks {

class [[nodiscard]] Never final : public cancel::HandlerBase {
 public:
  using ValueType = wheels::Unit;

 public:
  Never() = default;

  // Execution context

  static executors::IExecutor& Executor() {
    return executors::Prison();
  }

  static executors::SchedulerHint SchedulerHint() {
    return executors::SchedulerHint::UpToYou;  // Does not matter
  }

  static carry::Context UserContext() {
    return carry::Empty();
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;
    consumer_->CancelToken().Subscribe(this);
  }

 private:
  // cancel::IHandler

  void Forward(cancel::Signal signal) override {
    if (signal.CancelRequested()) {
      consumer_->Cancel();
    }
  }

 private:
  IConsumer<ValueType>* consumer_ = nullptr;
};

}  // namespace thunks

}  // namespace await::futures::lazy
