#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

#include <fallible/result/make.hpp>

#include <await/executors/impl/inline.hpp>
#include <carry/empty.hpp>

namespace await::futures::lazy {

namespace thunks {

class [[nodiscard]] JustUnit {
 public:
  using ValueType = wheels::Unit;

 public:
  JustUnit() = default;

  // Non-copyable
  JustUnit(const JustUnit&) = delete;

  // Movable
  JustUnit(JustUnit&&) = default;

  // Execution context

  static executors::IExecutor& Executor() {
    return executors::Inline();
  }

  static executors::SchedulerHint SchedulerHint() {
    return executors::SchedulerHint::New;
  }

  static carry::Context UserContext() {
    return carry::Empty();
  }

  // Lazy protocol

  void Start(IConsumer<wheels::Unit>* consumer) {
    consumer->Consume(fallible::Ok());
  }
};

}  // namespace thunks

}  // namespace await::futures::lazy
