#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

#include <await/futures/impl/eager/state.hpp>

namespace await::futures::lazy {

namespace thunks {

template <typename V>
class Eager final : IConsumer<V> {
 public:
  using ValueType = V;

 public:
  Eager(eager::StateRef<V> state)
      : state_(std::move(state)),
        executor_(state_->Executor()),
        execution_hint_(state_->SchedulerHint()),
        context_(state_->UserContext()) {
  }

  ~Eager() {
    if (state_.IsValid()) {
      Drop();
    }
  }

  // Non-copyable
  Eager(const Eager&) = delete;

  // Movable
  Eager(Eager&&) = default;

  // Execution context

  executors::IExecutor& Executor() const {
    return executor_;
  }

  executors::SchedulerHint SchedulerHint() const {
    return execution_hint_;
  }

  carry::Context UserContext() const {
    return context_;
  }

  // Lazy protocol

  void Start(IConsumer<V>* consumer) {
    ReleaseState()->SetConsumer(consumer);
  }

  // IConsumer<V>

  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

  void Consume(fallible::Result<V> result) noexcept override {
    consumer_->Consume(std::move(result));
  }

  void Cancel() noexcept override {
    consumer_->Cancel();
  }

  // Internal

  eager::StateRef<V> ReleaseState() {
    assert(state_.IsValid());
    return std::move(state_);
  }

  // Manual cancellation

  void RequestCancel() {
    ReleaseState()->Drop();
  }

  // Polling

  bool IsValid() const {
    return state_.IsValid();
  }

  bool HasResult() const {
    return state_->GetReadyState() == race::Ready::HasResult;
  }

  bool IsCancelled() const {
    return state_->GetReadyState() == race::Ready::Cancelled;
  }

  // Statement

  void IAmEager() {
    // No-op
  }

  void Drop() {
    ReleaseState()->Drop();
  }

 private:
  eager::StateRef<V> state_;

  // Execution context of thunk can be accessed after Start
  executors::IExecutor& executor_;
  executors::SchedulerHint execution_hint_;
  carry::Context context_;

  IConsumer<V>* consumer_{nullptr};
};

}  // namespace thunks

}  // namespace await::futures::lazy

