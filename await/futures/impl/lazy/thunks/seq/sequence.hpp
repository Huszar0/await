#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

namespace await::futures::lazy {

namespace thunks {

template <Thunk First, Thunk Then>
class [[nodiscard]] Sequence final : public IConsumer<typename First::ValueType> {
 public:
  using ValueType = typename Then::ValueType;

 public:
  Sequence(First first, Then then)
      : first_(std::move(first)),
        then_(std::move(then)) {
    static_assert(Thunk<Sequence>);
  }

  // Execution context

  executors::IExecutor& Executor() {
    return then_.Executor();
  }

  executors::SchedulerHint SchedulerHint() {
    return then_.SchedulerHint();
  }

  carry::Context UserContext() {
    return then_.UserContext();
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;
    first_.Start(/*consumer=*/this);
  }

 private:
  // IConsumer<First::ValueType>

  void Consume(fallible::Result<typename First::ValueType> /*ignored*/) noexcept override {
    then_.Start(consumer_);
  }

  void Cancel() noexcept override {
    // Skip snd
    consumer_->Cancel();
  }

  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

 private:
  First first_;
  Then then_;

  IConsumer<ValueType>* consumer_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
