#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

namespace await::futures::lazy {

namespace thunks {

template <Thunk Wrapped>
class [[nodiscard]] With {
 public:
  using ValueType = typename Wrapped::ValueType;

 public:
  With(Wrapped&& thunk, carry::Context context)
      : wrapped_(std::move(thunk)),
        context_(std::move(context)) {
  }

  // Non-copyable
  With(const With&) = delete;

  // Movable
  With(With&&) = default;

  // Execution context

  executors::IExecutor& Executor() const {
    return wrapped_.Executor();
  }

  executors::SchedulerHint SchedulerHint() const {
    return wrapped_.SchedulerHint();
  }

  carry::Context UserContext() const {
    return context_;
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    wrapped_.Start(consumer);
  }

 private:
  Wrapped wrapped_;
  carry::Context context_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
