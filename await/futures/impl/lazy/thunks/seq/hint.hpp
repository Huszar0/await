#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

namespace await::futures::lazy {

namespace thunks {

// Scheduling hint

template <Thunk Wrapped>
class [[nodiscard]] Hint {
 public:
  using ValueType = typename Wrapped::ValueType;

 public:
  Hint(Wrapped&& thunk, executors::SchedulerHint hint)
      : wrapped_(std::move(thunk)), hint_(hint) {
  }

  // Non-copyable
  Hint(const Hint&) = delete;

  // Movable
  Hint(Hint&&) = default;

  // Execution context

  executors::IExecutor& Executor() const {
    return wrapped_.Executor();
  }

  executors::SchedulerHint SchedulerHint() const {
    return hint_;
  }

  carry::Context UserContext() const {
    return wrapped_.UserContext();
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    wrapped_.Start(consumer);
  }

 private:
  Wrapped wrapped_;
  executors::SchedulerHint hint_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
