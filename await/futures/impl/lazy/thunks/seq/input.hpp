#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

#include <await/futures/impl/consume/consumers/linked.hpp>

#include <await/cancel/manual.hpp>

namespace await::futures::lazy {

namespace thunks {

// Input for parallel combinator

template <Thunk Wrapped>
class [[nodiscard]] Input final : public IConsumer<typename Wrapped::ValueType> {
 public:
  using ValueType = typename Wrapped::ValueType;

 public:
  Input(Wrapped&& thunk)
      : wrapped_(std::move(thunk)) {
  }

  // Non-copyable
  Input(const Input&) = delete;

  // Movable
  Input(Input&&) = default;

  // Execution context

  executors::IExecutor& Executor() const {
    return wrapped_.Executor();
  }

  executors::SchedulerHint SchedulerHint() const {
    return wrapped_.SchedulerHint();
  }

  carry::Context UserContext() const {
    return wrapped_.UserContext();
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    // Inlined allocation
    cancel_state_.Allocate();

    consumer_ = consumers::Link(consumer, cancel_state_.AsSource());

    wrapped_.Start(this);
  }

  // IConsumer<ValueType>

  cancel::Token CancelToken() override {
    return cancel_state_.MakeToken();
  }

  void Consume(fallible::Result<ValueType> result) noexcept override {
    consumer_->Consume(std::move(result));
  }

  void Cancel() noexcept override {
    consumer_->Cancel();
  }

 private:
  cancel::ManualState<> cancel_state_;
  Wrapped wrapped_;

  consumers::LinkedConsumer<ValueType> consumer_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
