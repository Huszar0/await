#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

#include <await/futures/impl/lazy/thunks/make/failure.hpp>

#include <refer/ref_counted.hpp>

namespace await::futures::lazy {

namespace thunks {

//////////////////////////////////////////////////////////////////////

// Thunk type erasure

//////////////////////////////////////////////////////////////////////

template <typename V>
struct IBoxedThunk : virtual refer::IManaged {
  virtual ~IBoxedThunk() = default;

  // Lazy protocol

  virtual void Start(IConsumer<V>* consumer) = 0;

  // Execution context

  virtual executors::IExecutor& Executor() const = 0;
  virtual executors::SchedulerHint SchedulerHint() const = 0;

  virtual carry::Context UserContext() const = 0;
};

//////////////////////////////////////////////////////////////////////

template <typename V>
using ThunkRef = refer::Ref<IBoxedThunk<V>>;

//////////////////////////////////////////////////////////////////////

namespace detail {

template <Thunk Boxed>
class Box : public IBoxedThunk<typename Boxed::ValueType>,
            public refer::RefCounted<Box<Boxed>> {
 public:
  using ValueType = typename Boxed::ValueType;

 public:
  Box(Boxed&& thunk)
      : thunk_(std::move(thunk)) {
  }

  // Task context

  executors::IExecutor& Executor() const override {
    return thunk_.Executor();
  }

  carry::Context UserContext() const override {
    return thunk_.UserContext();
  }

  // Execution

  executors::SchedulerHint SchedulerHint() const override {
    return thunk_.SchedulerHint();
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) override {
    thunk_.Start(consumer);
  }

 private:
  Boxed thunk_;
};

}  // namespace detail

//////////////////////////////////////////////////////////////////////

template <Thunk T>
ThunkRef<typename T::ValueType> Wrap(T&& thunk) {
  return refer::New<detail::Box<T>>(std::move(thunk));
}

//////////////////////////////////////////////////////////////////////

// Thunk

template <typename V>
class [[nodiscard]] Box final : IConsumer<V> {
 public:
  using ValueType = V;

 public:
  Box(ThunkRef<V> box)
      : box_(std::move(box)),
        executor_(&(box_->Executor())),
        execution_hint_(box_->SchedulerHint()),
        context_(box_->UserContext()) {
  }

  // Auto-Boxing

  template <Thunk T>
  Box(T thunk)
      : Box(Wrap(std::move(thunk))) {
  }

  // Untyped failure
  Box(Failure<wheels::Unit> failure)
    : Box(Wrap(Failure<V>(std::move(failure.error)))) {
  }

  // Non-copyable
  Box(const Box&) = delete;
  Box& operator=(const Box&) = delete;

  // Movable
  Box(Box&&) = default;
  Box& operator=(Box&&) = default;

  // Execution context

  executors::IExecutor& Executor() const {
    return *executor_;
  }

  executors::SchedulerHint SchedulerHint() const {
    return execution_hint_;
  }

  carry::Context UserContext() const {
    return context_;
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;

    auto box = box_;
    box->Start(this);
  }

  // IConsumer<ValueType>

  void Consume(fallible::Result<ValueType> result) noexcept override {
    box_.Reset();
    consumer_->Consume(std::move(result));
  }

  void Cancel() noexcept override {
    box_.Reset();
    consumer_->Cancel();
  }

  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

 private:
  ThunkRef<V> box_;

  // Execution context
  executors::IExecutor* executor_;
  executors::SchedulerHint execution_hint_;
  carry::Context context_;

  IConsumer<ValueType>* consumer_ = nullptr;
};

}  // namespace thunks

}  // namespace await::futures::lazy
