#pragma once

#include <await/futures/impl/race/rendezvous.hpp>

#include <await/futures/impl/lazy/thunk.hpp>

#include <await/cancel/states/join.hpp>
#include <await/cancel/states/strand.hpp>

#include <refer/ref_counted.hpp>

namespace await::futures::lazy {

namespace thunks {

//////////////////////////////////////////////////////////////////////

template <typename T>
struct IForker : virtual refer::IManaged {
  virtual ~IForker() = default;

  virtual void Start(size_t index, IConsumer<T>* consumer) = 0;
  virtual void RequestCancel() = 0;

  virtual executors::IExecutor& Executor() const = 0;
  virtual executors::SchedulerHint SchedulerHint() const = 0;

  virtual carry::Context UserContext() const = 0;
};

//////////////////////////////////////////////////////////////////////

template <Thunk F>
class Forker : public IForker<typename F::ValueType>,
               public IConsumer<typename F::ValueType>,
               public cancel::detail::JoinStateBase,
               public refer::RefCounted<Forker<F>> {

  using ValueType = typename F::ValueType;

  using Self = Forker<F>;

 public:
  Forker(F input)
      : cancel::detail::JoinStateBase(/*inputs=*/2),
        input_(std::move(input)) {
  }

  ~Forker() {
    //
  }

  // IForker

  void Start(size_t index, IConsumer<ValueType>* consumer) override {
    if (bool rendezvous = outputs_[index].SetConsumer(consumers::Link(consumer, CancelSource())); !rendezvous) {
      TryStart();
    }
  }

  void RequestCancel() override {
    cancel::detail::JoinStateBase::RequestCancel();
  }

  executors::IExecutor& Executor() const override {
    return input_.Executor();
  }

  virtual executors::SchedulerHint SchedulerHint() const override {
    return input_.SchedulerHint();
  }

  virtual carry::Context UserContext() const override {
    return input_.UserContext();
  }

  // IConsumer<T>

  cancel::Token CancelToken() override {
    return cancel::Token::FromState(this);
  }

  void Consume(fallible::Result<ValueType> input) noexcept override {
    outputs_[0].SetResult(input);
    outputs_[1].SetResult(input);

    Complete();
  }

  void Cancel() noexcept override {
    outputs_[0].SetCancel();
    outputs_[1].SetCancel();

    Complete();
  }

 private:
  cancel::Source CancelSource() {
    return cancel::Source::FromState(this);
  }

  IConsumer<ValueType>* AsConsumer() {
    this->AddRef();
    return this;
  }

  void Complete() {
    this->ReleaseRef();
  }

  void TryStart() {
    if (!started_.exchange(true)) {
      input_.Start(AsConsumer());
    }
  }

 private:
  F input_;
  race::Rendezvous<ValueType> outputs_[2];
  twist::ed::stdlike::atomic<bool> started_{false};
};

//////////////////////////////////////////////////////////////////////

template <typename T>
using ForkerRef = refer::Ref<IForker<T>>;


template <Thunk Input>
ForkerRef<typename Input::ValueType> Fork(Input input) {
  return refer::New<Forker<Input>>(std::move(input));
}

//////////////////////////////////////////////////////////////////////

template <typename T>
class Dolly {
  using ForkerRef = refer::Ref<IForker<T>>;

 public:
  using ValueType = T;

 public:
  Dolly(ForkerRef forker, size_t index)
      : forker_(forker),
        index_(index),
        executor_(&forker->Executor()),
        hint_(forker->SchedulerHint()),
        context_(forker_->UserContext()) {
    // Ok
  }

  // Non-copyable
  Dolly(const Dolly&) = delete;
  Dolly& operator=(const Dolly&) = delete;

  // Movable
  Dolly(Dolly&&) = default;

  ~Dolly() {
    if (forker_) {
      Release()->RequestCancel();
    }
  }

  void Start(IConsumer<T>* consumer) {
    Release()->Start(index_, consumer);
  }

  void RequestCancel() {
    Release()->RequestCancel();
  }

  executors::IExecutor& Executor() const {
    return *executor_;
  }

  virtual executors::SchedulerHint SchedulerHint() const {
    return hint_;
  }

  virtual carry::Context UserContext() const {
    return context_;
  }

 private:
  ForkerRef Release() {
    return std::move(forker_);
  }

 private:
  ForkerRef forker_;
  size_t index_;

  // Cached
  executors::IExecutor* executor_;
  executors::SchedulerHint hint_;
  carry::Context context_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
