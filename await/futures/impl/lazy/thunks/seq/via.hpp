#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

namespace await::futures::lazy {

namespace thunks {

template <Thunk Wrapped>
class [[nodiscard]] Via {
 public:
  using ValueType = typename Wrapped::ValueType;

 public:
  Via(Wrapped&& thunk, executors::IExecutor& executor)
      : wrapped_(std::move(thunk)),
        executor_(executor) {
  }

  // Non-copyable
  Via(const Via&) = delete;

  // Movable
  Via(Via&&) = default;

  // Execution context

  executors::IExecutor& Executor() const {
    return executor_;
  }

  executors::SchedulerHint SchedulerHint() const {
    return wrapped_.SchedulerHint();
  }

  carry::Context UserContext() const {
    return wrapped_.UserContext();
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    wrapped_.Start(consumer);
  }

 private:
  Wrapped wrapped_;
  await::executors::IExecutor& executor_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
