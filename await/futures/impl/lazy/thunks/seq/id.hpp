#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

namespace await::futures::lazy {

namespace thunks {

template <Thunk Wrapped>
class [[nodiscard]] Id {
 public:
  using ValueType = typename Wrapped::ValueType;

 public:
  Id(Wrapped&& thunk)
      : wrapped_(std::move(thunk)) {
  }

  // Non-copyable
  Id(const Id&) = delete;

  // Movable
  Id(Id&&) = default;

  // Execution context

  executors::IExecutor& Executor() const {
    return wrapped_.Executor();
  }

  executors::SchedulerHint SchedulerHint() const {
    return wrapped_.SchedulerHint();
  }

  carry::Context UserContext() const {
    return wrapped_.UserContext();
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    wrapped_.Start(consumer);
  }

 private:
  Wrapped wrapped_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
