#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

#include <await/executors/task/cancelled.hpp>

#include <await/thread/this.hpp>

#include <fallible/result/make.hpp>
#include <fallible/result/mappers.hpp>
#include <fallible/result/type_traits.hpp>

#include <wheels/core/defer.hpp>
#include <wheels/core/exception.hpp>

#include <cassert>
#include <optional>

namespace await::futures::lazy {

namespace thunks {

// [Await.Task(Manual,User)]
template <Thunk Producer, typename Mapper>
class [[nodiscard]] Map final : public IConsumer<typename Producer::ValueType>,
                                public executors::TaskBase {
 public:
  using InputValueType = typename Producer::ValueType;
  using MapResultType = fallible::MapResult<InputValueType, Mapper>;
  using OutputValueType = typename MapResultType::ValueType;

  using ValueType = OutputValueType;

 public:
  Map(Producer&& parent, Mapper&& mapper)
      : producer_(std::move(parent)),
        mapper_(std::move(mapper)) {
  }

  // Non-copyable
  Map(const Map&) = delete;

  // Movable
  Map(Map&&) = default;

  // Execution context

  executors::IExecutor& Executor() const {
    return producer_.Executor();
  }

  static executors::SchedulerHint SchedulerHint() {
    return executors::SchedulerHint::Next;
  }

  carry::Context UserContext() const {
    return producer_.UserContext();
  }

  // Lazy protocol

  void Start(IConsumer<OutputValueType>* consumer) {
    consumer_ = consumer;
    producer_.Start(/*consumer=*/this);
  }

 private:
  // IConsumer<InputValueType>

  // Propagate
  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

  void Consume(fallible::Result<InputValueType> input) noexcept override {
    if (CancelRequested()) {
      Cancel();
    } else {
      input_ = std::move(input);
      producer_.Executor().Submit(/*task=*/this, producer_.SchedulerHint());
    }
  }

  void Cancel() noexcept override {
    consumer_->Cancel();  // Propagate instantly
  }

  // executors::ITask

  void Run() noexcept override {
    try {
      auto output = RunMapper();
      // Happy path
      consumer_->Consume(std::move(output));
    } catch (executors::task::CancelledException) {
      // Cancellation
      consumer_->Cancel();
    }
  }

 private:
  bool CancelRequested() const {
    return consumer_->CancelToken().CancelRequested();
  }

  void SetupContext(thread::TaskContext* carrier) {
    carrier->SetExecutor(producer_.Executor());
    carrier->SetUserContext(producer_.UserContext());
    carrier->SetCancelToken(consumer_->CancelToken());
  }

  fallible::Result<OutputValueType> RunMapper() {
    auto* carrier = thread::This();

    SetupContext(carrier);

    // Reset context after invocation
    // NB: Before consumer->Cancel/Consume!
    wheels::Defer cleanup([carrier]() {
      carrier->ResetContext();
    });

    assert(!CancelToken().HasLinks());

    // NB: Destroy mapper after invocation!
    return std::move(*input_).Map(std::move(mapper_));
  }

 private:
  Producer producer_;

  std::optional<fallible::Result<InputValueType>> input_;
  Mapper mapper_;

  IConsumer<OutputValueType>* consumer_{nullptr};
};

}  // namespace thunks

}  // namespace await::futures::lazy
