#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

#include <await/futures/impl/consume/consumers/tagged.hpp>

#include <carry/delayed.hpp>

namespace await::futures::lazy {

namespace thunks {

struct FlattenTags {
  struct Future {};
  struct Value {};
};

// Automagically works both for Eager and Lazy!

template <Thunk Producer>
class [[nodiscard]] Flatten final
    : public futures::consumers::ITaggedConsumer<typename Producer::ValueType, FlattenTags::Future>,
      public futures::consumers::ITaggedConsumer<typename Producer::ValueType::ValueType, FlattenTags::Value> {
 public:
  using FutureType = typename Producer::ValueType;
  using ValueType = typename FutureType::ValueType;
  using ResultType = fallible::Result<ValueType>;

 public:
  Flatten(Producer&& thunk)
      : producer_(std::move(thunk)) {
  }

  // Non-copyable
  Flatten(const Flatten&) = delete;

  // Movable
  Flatten(Flatten&&) = default;

  // Execution context

  executors::IExecutor& Executor() const {
    return producer_.Executor();
  }

  static executors::SchedulerHint SchedulerHint() {
    return executors::SchedulerHint::UpToYou;
  }

  carry::Context UserContext() const {
    return context_.Make();
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;
    producer_.Start(AsFutureConsumer());
  }

  // Statement

  void IAmLazy() {
    // No-op
  }

 private:
  // IConsumer<FutureType>

  futures::consumers::ITaggedConsumer<FutureType, FlattenTags::Future>* AsFutureConsumer() {
    return this;
  }

  void ConsumeTagged(fallible::Result<FutureType> result, FlattenTags::Future) noexcept override {
    if (!result.IsOk()) {
      consumer_->Consume(fallible::PropagateError(result));
      return;
    }

    future_.emplace(std::move(*result));

    // Propagate context
    context_.Install(future_->UserContext());

    std::move(*future_).Start(AsValueConsumer());
  }

  void CancelTagged(FlattenTags::Future) noexcept override {
    consumer_->Cancel();
  }

  // IConsumer<ValueType>

  futures::consumers::ITaggedConsumer<ValueType, FlattenTags::Value>* AsValueConsumer() {
    return this;
  }

  void ConsumeTagged(ResultType result, FlattenTags::Value) noexcept override {
    consumer_->Consume(std::move(result));
  }

  void CancelTagged(FlattenTags::Value) noexcept override {
    consumer_->Cancel();
  }

  // Cancellation

  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

 private:
  Producer producer_;

  carry::DelayedContext context_;
  std::optional<FutureType> future_;

  IConsumer<ValueType>* consumer_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
