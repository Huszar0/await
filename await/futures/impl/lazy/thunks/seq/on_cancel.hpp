#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

#include <await/thread/this.hpp>

#include <await/cancel/never.hpp>

#include <wheels/core/defer.hpp>
#include <wheels/core/panic.hpp>

namespace await::futures::lazy {

namespace thunks {

// [Await.Task(Manual,User)]
template <Thunk Observed, typename F>
class [[nodiscard]] OnCancel final : public IConsumer<typename Observed::ValueType>,
                       public executors::TaskBase {
 public:
  using ValueType = typename Observed::ValueType;

 public:
  OnCancel(Observed&& thunk, F&& handler)
      : observed_(std::move(thunk)),
        handler_(std::move(handler)) {
    //
  }

  // Non-copyable
  OnCancel(const OnCancel&) = delete;

  // Movable
  OnCancel(OnCancel&&) = default;

  ~OnCancel() {
    //
  }

  // Execution context

  executors::IExecutor& Executor() const {
    return observed_.Executor();
  }

  static executors::SchedulerHint SchedulerHint() {
    return executors::SchedulerHint::Next;
  }

  carry::Context UserContext() const {
    return observed_.UserContext();
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;
    observed_.Start(/*consumer=*/this);
  }

 private:
  // IConsumer<ValueType>

  // Propagate
  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

  void Consume(fallible::Result<ValueType> result) noexcept override {
    // Propagate result
    consumer_->Consume(std::move(result));
  }

  void Cancel() noexcept override {
    Executor().Submit(this, executors::SchedulerHint::Next);
  }

  // ITask

  void Run() noexcept override {
    RunCancelHandler();
    consumer_->Cancel();  // Propagate cancel
  }

  void SetupContext(thread::TaskContext* carrier) {
    carrier->SetExecutor(Executor());
    carrier->SetUserContext(UserContext());

    // Disable cancellation for cancel handler
    carrier->SetCancelToken(cancel::Never());
  }

  void RunCancelHandler() {
    auto* carrier = thread::This();

    SetupContext(carrier);

    wheels::Defer cleanup([carrier]() {
      carrier->ResetContext();
    });

    try {
      handler_();
    } catch (...) {
      WHEELS_PANIC("Unhandled exception in Future cancel handler");
    }
  }

 private:
  Observed observed_;
  F handler_;

  IConsumer<ValueType>* consumer_;
};

}  // namespace thunks

}  // namespace await::futures::lazy
