#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

#include <wheels/core/unit.hpp>

namespace await::futures::lazy {

namespace thunks {

template <Thunk Wrapped>
class [[nodiscard]] ToUnit final : public IConsumer<typename Wrapped::ValueType>{
  using InputValueType = typename Wrapped::ValueType;
 public:
  using ValueType = wheels::Unit;

 public:
  ToUnit(Wrapped&& thunk)
      : wrapped_(std::move(thunk)) {
  }

  // Non-copyable
  ToUnit(const ToUnit&) = delete;

  // Movable
  ToUnit(ToUnit&&) = default;

  // Execution context

  executors::IExecutor& Executor() const {
    return wrapped_.Executor();
  }

  executors::SchedulerHint SchedulerHint() const {
    return wrapped_.SchedulerHint();
  }

  carry::Context UserContext() const {
    return wrapped_.UserContext();
  }

  // Lazy protocol

  void Start(IConsumer<ValueType>* consumer) {
    consumer_ = consumer;
    wrapped_.Start(this);
  }

  // IConsumer<InputValueType>

  void Consume(fallible::Result<InputValueType> result) noexcept override {
    consumer_->Consume(std::move(result).JustStatus());
  }

  void Cancel() noexcept override {
    consumer_->Cancel();
  }

  cancel::Token CancelToken() override {
    return consumer_->CancelToken();
  }

 private:
  Wrapped wrapped_;
  IConsumer<wheels::Unit>* consumer_ = nullptr;
};

}  // namespace thunks

}  // namespace await::futures::lazy
