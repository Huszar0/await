#pragma once

#include <await/futures/impl/race/state_machine.hpp>
#include <await/futures/impl/race/ready.hpp>

#include <await/futures/impl/consume/consumer.hpp>
#include <await/futures/impl/consume/consumers/linked.hpp>

#include <await/cancel/token.hpp>

#include <fallible/result/make.hpp>

#include <optional>

namespace await::futures {

namespace race {

//////////////////////////////////////////////////////////////////////

// Producer: SetResult / SetCancel
// Consumer: SetConsumer
// Progress guarantee: Wait-freedom

template <typename T>
class Rendezvous {
 public:
  bool SetConsumer(consumers::LinkedConsumer<T>&& consumer) {
    consumer_ = std::move(consumer);

    if (rendezvous_.Consume()) {
      MakeRendezvous();
      return true;
    } else {
      return false;
    }
  }

  Ready GetState() const {
    if (!rendezvous_.Produced()) {
      return Ready::NotReady;
    }

    // Produced
    if (HasResult()) {
      return Ready::HasResult;
    } else {
      return Ready::Cancelled;
    }
  }

  // Producer

  void SetResult(fallible::Result<T> result) {
    maybe_result_.emplace(std::move(result));
    DoProduce();
  }

  void SetError(fallible::Error error) {
    SetResult(fallible::Fail(std::move(error)));
  }

  void SetCancel() {
    DoProduce();
  }

 private:
  void DoProduce() {
    if (rendezvous_.Produce()) {
      MakeRendezvous();
    }
  }

  bool HasResult() const {
    return maybe_result_.has_value();
  }

  void MakeRendezvous() {
    if (HasResult()) {
      // Consume produced result
      consumer_->Consume(std::move(*maybe_result_));
    } else {
      // Cancel consumer
      consumer_->Cancel();
    }
  }

 private:
  RendezvousStateMachine rendezvous_;
  std::optional<fallible::Result<T>> maybe_result_;
  consumers::LinkedConsumer<T> consumer_;
};

}  // namespace race

}  // namespace await::futures
