#pragma once

namespace await::futures {

namespace race {

enum class Ready {
  NotReady = 0,
  HasResult = 1,
  Cancelled = 2
};

}  // namespace race

}  // namespace await::futures
