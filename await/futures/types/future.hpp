#pragma once

#include <await/futures/impl/lazy/thunk.hpp>

#include <wheels/core/unit.hpp>

#include <concepts>

namespace await::futures {

template <typename F>
concept SomeFuture = lazy::Thunk<F>;

template <typename F, typename V>
concept Future = SomeFuture<F> && std::same_as<typename F::ValueType, V>;

template <typename F>
concept UnitFuture = Future<F, wheels::Unit>;

}  // namespace await::futures
