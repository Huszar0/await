#pragma once

#include <await/futures/types/future.hpp>
#include <await/futures/types/eager.hpp>

#include <await/futures/types/traits/value_of.hpp>

namespace await::futures {

template <typename Future>
concept IsEager = std::same_as<Future, EagerFuture<ValueOf<Future>>>;

template <typename Future>
concept IsLazy = SomeFuture<Future> && !IsEager<Future>;

}  // namespace await::futures
