#pragma once

#include <await/futures/types/future.hpp>

namespace await::futures {

template <SomeFuture Future>
using ValueOf = typename Future::ValueType;

}  // namespace await::futures
