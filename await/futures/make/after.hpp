#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/impl/lazy/thunks/make/after.hpp>

namespace await::futures {

inline UnitFuture auto After(timers::Delay delay) {
  return lazy::thunks::After(delay);
}

}  // namespace await::futures
