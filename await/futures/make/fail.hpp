#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/impl/lazy/thunks/make/failure.hpp>

#include <wheels/core/unit.hpp>

namespace await::futures {

template <typename T = wheels::Unit>
Future<T> auto Fail(fallible::Error error) {
  return lazy::thunks::Failure<T>(std::move(error));
}

}  // namespace await::futures
