#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/impl/lazy/thunks/make/value.hpp>

namespace await::futures {

// Ready value

template <typename T>
Future<T> auto Value(T value) {
  return lazy::thunks::Value(std::move(value));
}

}  // namespace await::futures
