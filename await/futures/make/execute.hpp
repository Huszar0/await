#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/make/just.hpp>

#include <await/futures/combine/seq/via.hpp>
#include <await/futures/combine/seq/with.hpp>
#include <await/futures/combine/seq/map.hpp>

#include <await/executors/impl/inline.hpp>

// Current
#include <await/executors/task/context.hpp>
#include <await/executors/task/executor.hpp>

namespace await::futures {

//////////////////////////////////////////////////////////////////////

/*
 * Executes `fun` via executor `where`
 *
 * Shortcut for
 *  Just() |
 *    Via(where) |
 *    With(context::Current()) |
 *    Map([fun](Unit) { return fun(); })
 *
 * Example:
 *
 * auto exe = futures::Execute(pool, []() -> int {
 *   return 42;
 * });
 *
 * auto result = Await(std::move(exe));
 */

template <typename F>
SomeFuture auto Execute(executors::IExecutor& where, F fun) {
  // clang-format off

  return Just() |
    Via(where) |
    With(executors::task::Context()) |
    Map(std::move(fun));

  // clang-format on
}

//////////////////////////////////////////////////////////////////////

/*
 * Executes `fun` via the current executor
 *
 * Shortcut for Execute(executors::task::Executor(), fun)
 */

template <typename F>
SomeFuture auto Spawn(F fun) {
  return Execute(executors::task::Executor(), std::move(fun));
}

//////////////////////////////////////////////////////////////////////

/*
 * Executes `fun` via `Inline` executor
 */

template <typename F>
SomeFuture auto Invoke(F fun) {
  return Execute(executors::Inline(), std::move(fun));
}

}  // namespace await::futures
