#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/impl/lazy/thunks/make/just.hpp>

#include <fallible/result/make.hpp>

namespace await::futures {

/*
 * Represents ready Unit value
 *
 * Idiomatic way to schedule user task in Await:
 *
 * auto task = Just() |
 *             Via(executor) |
 *             With(context) |
 *             Map([] { fmt::println("Running"); });
 */

inline UnitFuture auto Just() {
  return lazy::thunks::JustUnit{};
}

}  // namespace await::futures
