#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/impl/lazy/thunks/make/ready.hpp>

namespace await::futures {

// Ready result

template <typename T>
Future<T> auto Ready(fallible::Result<T> result) {
  return lazy::thunks::Ready(std::move(result));
}

}  // namespace await::futures
