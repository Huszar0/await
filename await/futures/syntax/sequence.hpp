#pragma once

#include <await/futures/types/future.hpp>

#include "await/futures/combine/seq/sequence.hpp"

/*
 * "Sequence" operator (>>)
 * (f >> g) means Await(f) ; g
 *
 * Sequences two asynchronous operations
 *
 * Example:
 *
 * After(1s) >> After(2s) >> Invoke([] {
 *   fmt::println("3s later");
 * });
 *
 */

template <await::futures::SomeFuture FirstFuture, await::futures::SomeFuture ThenFuture>
auto operator >>(FirstFuture first, ThenFuture then) {
  return await::futures::Sequence(std::move(first), std::move(then));
}
