#pragma once

#include <await/futures/combine/par/join.hpp>

/*
 * "Join" or "both" operator (&&, and)
 * (f and g) means Join(f, g)
 *
 */

template <await::futures::SomeFuture LeftFuture, await::futures::SomeFuture RightFuture>
await::futures::UnitFuture auto operator &&(LeftFuture lhs, RightFuture rhs) {
  return await::futures::Join(std::move(lhs), std::move(rhs));
}
