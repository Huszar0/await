#pragma once

#include <await/futures/types/future.hpp>

#include <await/futures/run/go.hpp>

/*
 * "Go" operator (~)
 *
 * Starts lazy Future / detaches operation
 *
 * Example:
 *
 * ~futures::Execute(pool, [] {
 *   fmt::println("Free-floating task");
 * });
 *
 */

template <await::futures::SomeFuture Future>
void operator ~(Future f) {
  std::move(f) | await::futures::Go();
}
