#pragma once

#include <await/futures/combine/par/all.hpp>

#include <await/futures/types/future.hpp>

/*
 * "Sum" operator (+)
 * (f + g) means All(f, g)
 *
 */

template <await::futures::SomeFuture LeftFuture, await::futures::SomeFuture RightFuture>
await::futures::SomeFuture auto operator +(LeftFuture lhs, RightFuture rhs) {
  return await::futures::All(std::move(lhs), std::move(rhs));
}
