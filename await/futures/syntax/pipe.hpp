#pragma once

#include <await/futures/types/future.hpp>

/*
 * "Pipe" operator (|) for building Future pipelines
 *
 * Example:
 *
 * auto f = futures::Value(7) | futures::AndThen([](int value) {
 *   return value + 1;
 * });
 *
 */

template <await::futures::SomeFuture Future, typename Builder>
auto operator |(Future&& f, Builder b) {
  return b.Pipe(std::move(f));
}
