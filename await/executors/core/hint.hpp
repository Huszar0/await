#pragma once

namespace await::executors {

enum class SchedulerHint {
  UpToYou = 1,  // Rely on executor scheduling decision
  Next = 2,     // LIFO scheduling
  New = 3,      // New task chain
  Yield = 4,    // Yield control due to lack of work
};

}  // namespace await::executors
