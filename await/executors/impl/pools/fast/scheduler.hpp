#pragma once

#include <await/executors/core/task.hpp>

namespace await::executors {

// Task scheduling algorithm
// (e.g. shared queue or work-stealing)

// See also IWorkerRunner

struct IScheduler {
  virtual ~IScheduler() = default;

  virtual bool StopRequested() const = 0;

  // Pick next task to run
  // Returns nullptr if stop requested
  virtual TaskBase* PickTask() = 0;

  // Report progress back
  virtual void TaskCompleted() = 0;
};

}  // namespace await::executors
