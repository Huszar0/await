#pragma once

#include <twist/ed/stdlike/atomic.hpp>
#include <twist/ed/stdlike/mutex.hpp>

#include <wheels/intrusive/list.hpp>

namespace await::executors::pools::fast {

class Worker;

class Coordinator {
 public:
  explicit Coordinator(size_t threads)
      : threads_(threads) {
  }

  // NB: Invoked on every Submit, so do not write to
  // memory locations shared between worker threads on fast path!
  void NotifyNewTask() {
    if (ShouldWakeWorker()) {
      WakeWorker();
    }
  }

  void AddActiveWorker() {
    active_.fetch_add(1);
  }

  void RemoveActiveWorker() {
    active_.fetch_sub(1);
  }

  void StepDownAsActiveWorker(Worker* worker);

  void BecomeActiveWorker(Worker* worker);

  void WakeWorker();

  bool StartSpinning() {
    size_t curr = spinning_.load(std::memory_order::relaxed);
    while (curr * 2 < threads_) {
      if (spinning_.compare_exchange_weak(curr, curr + 1,
                                          std::memory_order::relaxed)) {
        return true;
      }
    }
    return false;
  }

  // Returns true for last spinning worker
  bool StopSpinning() {
    return spinning_.fetch_sub(1) == 1;
  }

  // Stop

  void RequestStop() {
    stop_requested_.store(true);
    WakeAll();
  }

  bool StopRequested() const {
    return stop_requested_.load();
  }

 private:
  bool ShouldWakeWorker() const {
    // 1) Active workers < threads
    // 2) Number of spinning (stealing) threads == 0

    return (active_.load() < threads_) && (spinning_.load() == 0);
  }

  void WakeAll();

 private:
  const size_t threads_;

  twist::ed::stdlike::atomic<uint32_t> active_{0};
  twist::ed::stdlike::atomic<size_t> spinning_{0};

  twist::ed::stdlike::mutex mutex_;
  wheels::IntrusiveList<Worker> sleepers_;

  twist::ed::stdlike::atomic<bool> stop_requested_{false};
};

}  // namespace await::executors::pools::fast
