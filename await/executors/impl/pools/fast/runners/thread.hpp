#pragma once

#include <await/executors/impl/pools/fast/runner.hpp>

namespace await::executors::runners {

class ThreadWorkerRunner : public IWorkerRunner {
 public:
  void RunWorker(IScheduler& scheduler) override {
    while (TaskBase* next = scheduler.PickTask()) {
      next->Run();
      scheduler.TaskCompleted();
    }
  }

  static ThreadWorkerRunner& Instance() {
    static ThreadWorkerRunner single;
    return single;
  }
};

}  // namespace await::executors::runners
