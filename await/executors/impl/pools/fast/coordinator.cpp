#include <await/executors/impl/pools/fast/coordinator.hpp>

#include <await/executors/impl/pools/fast/worker.hpp>

namespace await::executors::pools::fast {

void Coordinator::StepDownAsActiveWorker(Worker* worker) {
  std::lock_guard locker(mutex_);

  sleepers_.PushBack(worker);
  active_.fetch_sub(1);
}

void Coordinator::BecomeActiveWorker(Worker* worker) {
  std::lock_guard locker(mutex_);

  if (worker->IsLinked()) {
    worker->Unlink();
    active_.fetch_add(1);
  }
}

void Coordinator::WakeWorker() {
  std::lock_guard locker(mutex_);

  if (sleepers_.IsEmpty()) {
    return;
  }

  Worker* worker = sleepers_.PopBack();
  active_.fetch_add(1);
  worker->Wake();
}

void Coordinator::WakeAll() {
  std::lock_guard locker(mutex_);

  while (!sleepers_.IsEmpty()) {
    active_.fetch_add(1);
    Worker* worker = sleepers_.PopBack();
    worker->Wake();
  }
}

}  // namespace await::executors::pools::fast
