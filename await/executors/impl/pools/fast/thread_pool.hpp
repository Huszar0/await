#pragma once

#include <await/executors/core/executor.hpp>

#include <await/executors/impl/pools/fast/queues/global_queue.hpp>
#include <await/executors/impl/pools/fast/coordinator.hpp>
#include <await/executors/impl/pools/fast/worker.hpp>

#include <await/executors/impl/pools/fast/task_counters/shared.hpp>

#include <await/infra/support/task_count.hpp>

// random_device
#include <twist/ed/stdlike/random.hpp>
#include <twist/ed/stdlike/atomic.hpp>

#include <deque>

namespace await::executors::pools::fast {

struct Delayed {};

// [Await.Executor]
class ThreadPool : public IExecutor {
  friend class Worker;

 public:
  explicit ThreadPool(size_t threads, Delayed);

  // Create and start
  explicit ThreadPool(size_t threads);

  ~ThreadPool();

  // Non-copyable
  ThreadPool(const ThreadPool&) = delete;
  ThreadPool& operator=(const ThreadPool&) = delete;

  // Non-movable
  ThreadPool(ThreadPool&&) = delete;
  ThreadPool& operator=(ThreadPool&&) = delete;

  // Setup (Delayed + Start)
  void SetRunner(IWorkerRunner& runner);
  void SetTaskCounter(ITaskCounter& counter);

  void Start();

  // IExecutor
  void Submit(TaskBase* task, SchedulerHint hint) override;

  static ThreadPool* Current();

  size_t ThreadCount() const;

  void WaitIdle();

  // Hard shutdown: stop workers and discard pending tasks
  void Stop();

  // After Stop
  PoolMetrics GetMetrics() const {
    return metrics_;
  }

 private:
  ITaskCounter& TaskCounter();
  IWorkerRunner& Runner();

  bool FromThisPool(Worker* submitter) const;
  void Push(TaskBase* task, SchedulerHint hint);

 private:
  const size_t threads_;
  IWorkerRunner* runner_;

  Coordinator coordinator_;
  std::deque<Worker> workers_;
  GlobalQueue<TaskBase> global_tasks_;

  // TODO: Better
  SharedCounter shared_task_counter_;
  ITaskCounter* task_counter_;

  twist::ed::stdlike::random_device random_device_;

  PoolMetrics metrics_;
};

}  // namespace await::executors::pools::fast
