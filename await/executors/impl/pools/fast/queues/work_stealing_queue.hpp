#pragma once

#include <await/infra/support/power2.hpp>

#include <twist/ed/stdlike/atomic.hpp>

#include <array>
#include <span>
#include <cassert>

namespace await::executors::pools::fast {

// Single Producer / Multiple Consumers ring buffer

template <typename Uint>
constexpr size_t PerformantLocalQueueCapacity(Uint at_least) {
  return support::NextPowerOf2(at_least) - 1;
}

template <typename T, size_t Capacity>
class WorkStealingQueue {
  struct Slot {
    twist::ed::stdlike::atomic<T*> item;
  };

 public:
  size_t SizeUpperBound() const {
    size_t curr_head = head_.load(std::memory_order::relaxed);
    size_t curr_tail = tail_.load(std::memory_order::relaxed);
    return curr_tail - curr_head;
  }

  size_t SpaceLowerBound() const {
    return Capacity - SizeUpperBound();
  }

  // Single producer

  // Should always succeed
  void PushMany(std::span<T*> buffer) {
    size_t curr_head = head_.load(std::memory_order::acquire);
    size_t curr_tail = tail_.load(std::memory_order::relaxed);

    size_t size = curr_tail - curr_head;
    [[maybe_unused]] size_t space = Capacity - size;
    assert(space >= buffer.size());

    for (size_t k = 0; k < buffer.size(); ++k) {
      buffer_[ToIndex(curr_tail + k)].item.store(buffer[k], std::memory_order::relaxed);
    }
    tail_.store(curr_tail + buffer.size(), std::memory_order::release);
  }

  bool TryPush(T* item) {
    size_t curr_head = head_.load(std::memory_order::acquire);
    size_t curr_tail = tail_.load(std::memory_order::relaxed);

    if (IsFull(curr_head, curr_tail)) {
      return false;
    }

    assert(item != nullptr);

    buffer_[ToIndex(curr_tail)].item.store(item, std::memory_order::relaxed);
    tail_.store(curr_tail + 1, std::memory_order::release);
    return true;
  }

  // Multiple consumers

  T* TryPop() {
    while (true) {
      size_t curr_head = head_.load(std::memory_order::acquire);
      size_t curr_tail = tail_.load(std::memory_order::relaxed);

      assert(curr_head <= curr_tail);

      if (IsEmpty(curr_head, curr_tail)) {
        return nullptr;
      }

      T* obj = buffer_[ToIndex(curr_head)].item.load(std::memory_order::relaxed);
      if (head_.compare_exchange_weak(curr_head, curr_head + 1,
                                      std::memory_order::release)) {
        return obj;
      }
    }
  }

  size_t Grab(std::span<T*> out_buffer) {
    while (true) {
      size_t curr_head = head_.load(std::memory_order::acquire);
      size_t curr_tail = tail_.load(std::memory_order::acquire);

      assert(curr_head <= curr_tail);

      size_t size = curr_tail - curr_head;

      if (size == 0) {
        return 0;
      }

      size_t to_grab =
          (out_buffer.size() < size) ? out_buffer.size() : size;

      for (size_t i = 0; i < to_grab; ++i) {
        out_buffer[i] = buffer_[ToIndex(curr_head + i)].item.load(
            std::memory_order::relaxed);
      }

      if (head_.compare_exchange_weak(curr_head, curr_head + to_grab,
                                      std::memory_order::release)) {
        return to_grab;
      }
    }
  }

  static constexpr size_t BufferSize() {
    return Capacity + 1;
  }

  static constexpr bool IsBufferSizePerformant() {
    return support::IsPowerOf2(BufferSize());
  }

 private:
  static size_t ToIndex(size_t pos) {
    if constexpr (support::IsPowerOf2(BufferSize())) {
      return pos & (BufferSize() - 1);
    } else {
      // Stress tests can set arbitrary Capacity
      return pos % BufferSize();
    }
  }

  static bool IsEmpty(size_t head, size_t tail) {
    return head == tail;
  }

  static bool IsFull(size_t head, size_t tail) {
    return tail - head + 1 >= BufferSize();
  }

 private:
  std::array<Slot, Capacity + 1> buffer_;
  twist::ed::stdlike::atomic<size_t> head_{0};
  twist::ed::stdlike::atomic<size_t> tail_{0};
};

}  // namespace await::executors::pools::fast
