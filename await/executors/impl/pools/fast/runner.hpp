#pragma once

#include <await/executors/impl/pools/fast/scheduler.hpp>

namespace await::executors {

// Runner runs tasks from the provided `scheduler`
// as a separate worker thread

struct IWorkerRunner {
  virtual ~IWorkerRunner() = default;

  // Run tasks until stop requested
  virtual void RunWorker(IScheduler& scheduler) = 0;
};

}  // namespace await::executors
