#include <await/executors/impl/pools/fast/thread_pool.hpp>

#include <await/executors/impl/pools/fast/runners/thread.hpp>

namespace await::executors::pools::fast {

ThreadPool::ThreadPool(size_t threads)
    : ThreadPool(threads, Delayed{}) {
  Start();
}

ThreadPool::ThreadPool(size_t threads, Delayed)
    : threads_(threads),
      runner_(&runners::ThreadWorkerRunner::Instance()),
      coordinator_(threads),
      task_counter_(&shared_task_counter_) {
}

ThreadPool::~ThreadPool() {
  WHEELS_ASSERT(workers_.empty(), "Explicit Stop required");
}

void ThreadPool::SetTaskCounter(ITaskCounter& counter) {
  task_counter_ = &counter;
}

void ThreadPool::SetRunner(IWorkerRunner& runner) {
  runner_ = &runner;
}

void ThreadPool::Start() {
  for (size_t i = 0; i < threads_; ++i) {
    workers_.emplace_back(*this, i);
  }

  for (auto& worker : workers_) {
    worker.Start();
  }
}

void ThreadPool::Submit(TaskBase* task, SchedulerHint hint) {
  TaskCounter().Add(1);
  Push(task, hint);
  coordinator_.NotifyNewTask();
}

ITaskCounter& ThreadPool::TaskCounter() {
  return *task_counter_;
}

IWorkerRunner& ThreadPool::Runner() {
  return *runner_;
}

bool ThreadPool::FromThisPool(Worker* submitter) const {
  return (submitter != nullptr) && (submitter->Host() == this);
}

void ThreadPool::Push(TaskBase* task, SchedulerHint hint) {
  //  if (hint == Hint::Yield) {
  //    global_tasks_.PushOne(task);
  //    return;
  //  }

  Worker* submitter = Worker::Current();

  if (FromThisPool(submitter)) {
    // Task submitted from worker thread
    submitter->LocalPush(task, hint);
  } else {
    // From external thread
    global_tasks_.PushOne(task);
  }
}

void ThreadPool::WaitIdle() {
  TaskCounter().WaitIdle();
}

void ThreadPool::Stop() {
  coordinator_.RequestStop();

  for (auto& worker : workers_) {
    worker.Join();
  }

  // Expect empty queue
  WHEELS_ASSERT(global_tasks_.TryPopOne() == nullptr, "Cannot Stop thread pool with pending tasks");

  for (auto& worker : workers_) {
    metrics_.Merge(worker.Metrics());
  }

  workers_.clear();
}

ThreadPool* ThreadPool::Current() {
  if (auto* this_worker = Worker::Current()) {
    return this_worker->Host();
  } else {
    return nullptr;
  }
}

size_t ThreadPool::ThreadCount() const {
  return workers_.size();
}

}  // namespace await::executors::pools::fast
