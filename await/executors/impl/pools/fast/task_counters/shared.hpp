#pragma once

#include <await/executors/impl/pools/fast/task_counters/counter.hpp>

// TODO: move to executors
#include <await/infra/support/task_count.hpp>

namespace await::executors::pools::fast {

// For tests

class SharedCounter : public ITaskCounter {
 public:
  void Add(size_t count) override {
    impl_.Add(count);
  }

  void CompleteOne() override {
    impl_.Done();
  }

  void WaitIdle() override {
    impl_.WaitIdle();
  }

 private:
  support::TaskCount impl_;
};

}  // namespace await::executors::pools::fast
