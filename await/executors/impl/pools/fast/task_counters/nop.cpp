#include <await/executors/impl/pools/fast/task_counters/nop.hpp>

namespace await::executors::pools::fast {

// For benchmarking

struct NopCounter : ITaskCounter {
  virtual void Add(size_t) {
    // Nop
  }

  virtual void CompleteOne() {
    // Nop
  }

  virtual void WaitIdle() {
    // Nop
  }
};

ITaskCounter& NopCounter() {
  static struct NopCounter instance;
  return instance;
}

}  // await::executors::pools::fast