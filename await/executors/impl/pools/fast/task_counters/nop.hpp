#pragma once

#include <await/executors/impl/pools/fast/task_counters/counter.hpp>

namespace await::executors::pools::fast {

ITaskCounter& NopCounter();

}  // namespace await::executors::pools::fast
