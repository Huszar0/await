#pragma once

#include <cstdlib>

namespace await::executors::pools::fast {

struct ITaskCounter {
  virtual void Add(size_t count) = 0;
  virtual void CompleteOne() = 0;

  // Block current thread until task counter reaches zero
  virtual void WaitIdle() = 0;
};

}  // namespace await::executors::pools::fast
