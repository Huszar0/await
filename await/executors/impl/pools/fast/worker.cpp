#include <await/executors/impl/pools/fast/worker.hpp>
#include <await/executors/impl/pools/fast/thread_pool.hpp>

#include <await/thread/this.hpp>

#include <twist/ed/local/ptr.hpp>

#include <twist/ed/wait/sys.hpp>

#include <cassert>

namespace await::executors::pools::fast {

TWIST_DECLARE_TL_PTR(Worker, this_worker)

Worker::Worker(ThreadPool& host, size_t index)
    : host_(host),
      index_(index) {
}

void Worker::Start() {
  host_.coordinator_.AddActiveWorker();

  thread_.emplace([this]() {
    Work();
  });
}

void Worker::Join() {
  thread_->join();
}

void Worker::Wake() {
  ++metrics_.wakes;

  parking_lot_.Wake();
}

size_t Worker::StealTasks(std::span<TaskBase*> out_buffer) {
  return local_tasks_.Grab(out_buffer);
}

Worker* Worker::Current() {
  return this_worker;
}

// Precondition: Worker::Current() == this
// Single-writer
void Worker::LocalPush(TaskBase* task, SchedulerHint hint) {
  if (hint == SchedulerHint::Next) {
    PushToLifoSlot(task);
  } else {
    // Fifo by default
    PushToLocalQueue(task);
  }
}

void Worker::PushToLifoSlot(TaskBase* task) {
  TaskBase* evicted = std::exchange(lifo_slot_, task);
  if (evicted != nullptr) {
    PushToLocalQueue(evicted);
  }
}

void Worker::PushToLocalQueue(TaskBase* task) {
  static_assert(LocalTaskQueue::IsBufferSizePerformant());

  if (!local_tasks_.TryPush(task)) {
    OffloadTasksToGlobalQueue(task);
  }
}

void Worker::OffloadTasksToGlobalQueue(TaskBase* overflow) {
  // Precondition: local queue is full

  size_t to_offload = local_tasks_.SizeUpperBound() / 2 + 1;

  size_t batch_size = local_tasks_.Grab({buffer_, to_offload});
  buffer_[batch_size++] = overflow;

  host_.global_tasks_.Offload({buffer_, batch_size});
}

TaskBase* Worker::TryPickTaskFromLifoSlot() {
  static const size_t kLifoStepsLimit = 5;

  TaskBase* next = std::exchange(lifo_slot_, nullptr);

  if (next == nullptr) {
    return nullptr;
  }

  if (++lifo_steps_ > kLifoStepsLimit) {
    // Fairness

    lifo_steps_ = 0;
    PushToLocalQueue(next);
    return nullptr;
  } else {
    return next;
  }
}

TaskBase* Worker::GrabTasksFromGlobalQueue() {
  size_t space = local_tasks_.SpaceLowerBound();

  size_t to_grab = std::min<size_t>(space / 2 + 1, 48);

  size_t grab_count =
      host_.global_tasks_.Grab({buffer_, to_grab}, host_.threads_);

  TaskBase* next = nullptr;

  if (grab_count > 0) {
    next = buffer_[0];

    local_tasks_.PushMany({buffer_ + 1, grab_count - 1});

    metrics_.global_grabs++;
    metrics_.tasks_grabbed += grab_count;
  }

  return next;
}

TaskBase* Worker::TryStealTasks(size_t series) {
  TaskBase** steal_start = &buffer_[0];
  size_t to_steal = 7;
  size_t stolen = 0;

  for (size_t t = 0; (t < series) && (to_steal > 0); ++t) {
    size_t k = twister_();
    for (size_t i = 0; (i < host_.threads_) && (to_steal > 0); ++i) {
      size_t steal_target_index = (k + i) % host_.threads_;

      if (steal_target_index == index_) {
        continue;
      }

      Worker& steal_target = host_.workers_[steal_target_index];

      size_t stolen_from_target =
          steal_target.StealTasks({steal_start, to_steal});

      if (stolen_from_target > 0) {
        steal_start += stolen_from_target;
        to_steal -= stolen_from_target;
        stolen += stolen_from_target;
        metrics_.steals++;
      }
    }
  }

  metrics_.tasks_stolen += stolen;

  if (stolen > 0) {
    if (stolen > 1) {
      local_tasks_.PushMany({buffer_ + 1, stolen - 1});
    }
    return buffer_[0];
  } else {
    return nullptr;
  }
}

TaskBase* Worker::TryPickTask() {
  if (iter_ % 61 == 0) {
    if (TaskBase* next = host_.global_tasks_.TryPopOne(); next != nullptr) {
      return next;
    }
  }

  if (TaskBase* next = TryPickTaskFromLifoSlot(); next != nullptr) {
    ++metrics_.tasks_lifo;
    return next;
  }

  if (TaskBase* next = local_tasks_.TryPop(); next != nullptr) {
    ++metrics_.tasks_local;
    return next;
  }

  if (TaskBase* next = GrabTasksFromGlobalQueue(); next != nullptr) {
    return next;
  }

  if (host_.coordinator_.StartSpinning()) {
    static const size_t kTries = 4;

    TaskBase* next = TryStealTasks(kTries);
    bool last_spinner = host_.coordinator_.StopSpinning();

    if (next != nullptr && last_spinner) {
      host_.coordinator_.WakeWorker();
    }

    if (next != nullptr) {
      return next;
    }
  }

  return nullptr;
}

TaskBase* Worker::TryPickTaskBeforePark() {
  if (TaskBase* next = host_.global_tasks_.TryPopOne(); next != nullptr) {
    return next;
  }

  if (TaskBase* next = TryStealTasks(/*tries=*/1); next != nullptr) {
    return next;
  }

  return nullptr;
}

// IScheduler

bool Worker::StopRequested() const {
  return host_.coordinator_.StopRequested();
}

TaskBase* Worker::PickTask() {
  ++iter_;

  while (!host_.coordinator_.StopRequested()) {
    if (TaskBase* next = TryPickTask()) {
      return next;
    }

    // Announce parking

    auto epoch = parking_lot_.Prepare();

    host_.coordinator_.StepDownAsActiveWorker(this);

    // Recheck condition
    if (TaskBase* next = TryPickTaskBeforePark()) {
      host_.coordinator_.BecomeActiveWorker(this);
      return next;
    }

    if (host_.coordinator_.StopRequested()) {
      host_.coordinator_.BecomeActiveWorker(this);
      return nullptr;
    }

    // Park
    ++metrics_.parks;
    parking_lot_.ParkIf(epoch);
  }

  return nullptr;
}

// IScheduler
void Worker::TaskCompleted() {
  metrics_.tasks++;
  host_.TaskCounter().CompleteOne();
}

void Worker::Work() {
  this_worker = this;

  thread::Scope scope(this);

  // Deterministic randomness
  twister_.seed(host_.random_device_());

  // Runner
  host_.Runner().RunWorker(/*scheduler=*/*this);

  Stop();
}

void Worker::Stop() {
  host_.coordinator_.RemoveActiveWorker();

  // Expect empty queues
  assert(lifo_slot_ == nullptr);
  assert(local_tasks_.TryPop() == nullptr);
}

}  // namespace await::executors::pools::fast
