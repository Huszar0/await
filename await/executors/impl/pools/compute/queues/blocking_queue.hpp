#pragma once

#include <twist/ed/stdlike/mutex.hpp>
#include <twist/ed/stdlike/condition_variable.hpp>

#include <wheels/intrusive/forward_list.hpp>

#include <optional>
#include <utility>

namespace await::executors::pools::compute::detail {

//////////////////////////////////////////////////////////////////////

// Multi-producer/multi-consumer unbounded blocking queue

template <typename T>
class MPMCBlockingQueue {
 public:
  // Returns false iff queue is closed for producers
  bool Put(T* item) {
    {
      std::lock_guard lock(mutex_);
      if (closed_) {
        return false;
      }
      items_.PushBack(item);
    }
    not_empty_.notify_one();
    return true;
  }

  // Await and take next item
  // Returns nullptr iff queue is both 1) drained and 2) closed
  T* Take() {
    std::unique_lock lock(mutex_);
    while (items_.IsEmpty() && !closed_) {
      not_empty_.wait(lock);
    }
    return items_.PopFront();
  }

  // Close queue for producers
  void Close() {
    std::lock_guard lock(mutex_);
    closed_ = true;
    not_empty_.notify_all();
  }

 private:
  wheels::IntrusiveForwardList<T> items_;
  bool closed_{false};
  twist::ed::stdlike::mutex mutex_;
  twist::ed::stdlike::condition_variable not_empty_;
};

}  // namespace await::executors::pools::compute::detail
