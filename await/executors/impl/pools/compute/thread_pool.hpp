#pragma once

#include <await/executors/core/executor.hpp>

#include <await/executors/impl/pools/compute/queues/blocking_queue.hpp>
#include <await/infra/support/task_count.hpp>

#include <twist/ed/stdlike/thread.hpp>
#include <twist/ed/stdlike/atomic.hpp>

#include <cstdlib>
#include <string>
#include <vector>

namespace await::executors::pools::compute {

// Executor for independent CPU-bound tasks
// Fixed pool of threads + shared unbounded blocking queue as a scheduler

// [Await.Executor]
class ThreadPool final : public IExecutor {
 public:
  explicit ThreadPool(size_t threads);

  ~ThreadPool();

  // Non-copyable
  ThreadPool(const ThreadPool&) = delete;
  ThreadPool& operator=(const ThreadPool&) = delete;

  // Non-movable
  ThreadPool(ThreadPool&&) = delete;
  ThreadPool& operator=(ThreadPool&&) = delete;

  // IExecutor
  void Submit(TaskBase* task, SchedulerHint) override;

  // Wait until the pool has no more outstanding work
  void WaitIdle();

  // Hard shutdown
  // Just stop as soon as possible ignoring all scheduled tasks
  void Stop();

  static ThreadPool* Current();

  size_t ThreadCount() const;

 private:
  // Scheduler
  TaskBase* PickTask();
  void TaskCompleted();

  void LaunchWorkers(size_t count);
  void JoinWorkers();

  void WorkerRoutine();

  void DoStop();

 private:
  detail::MPMCBlockingQueue<TaskBase> tasks_;
  std::vector<twist::ed::stdlike::thread> workers_;
  support::TaskCount task_count_;
};

}  // namespace await::executors::pools::compute
