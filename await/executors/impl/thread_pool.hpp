#pragma once

#include <await/executors/impl/pools/compute/thread_pool.hpp>
#include <await/executors/impl/pools/fast/thread_pool.hpp>

namespace await::executors {

// Default thread pool
using ThreadPool = pools::fast::ThreadPool;

}  // namespace await::executors
