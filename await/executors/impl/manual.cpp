#include <await/executors/impl/manual.hpp>

#include <await/executors/impl/manual/budget/limit.hpp>
#include <await/executors/impl/manual/budget/unlimited.hpp>

#include <wheels/core/assert.hpp>

namespace await::executors {

using namespace manual;

ManualExecutor::~ManualExecutor() {
  WHEELS_ASSERT(tasks_.IsEmpty(),
                "Destroying Manual executor with non-empty task queue");
}

size_t ManualExecutor::RunAtMost(size_t count) {
  budget::Limit limit(count);
  return Run(limit);
}

size_t ManualExecutor::Drain() {
  budget::Unlimited unlimited;
  return Run(unlimited);
}

size_t ManualExecutor::Run(IBudget& budget) {
  size_t count = 0;

  while (budget.Allow() && tasks_.NonEmpty()) {
    TaskBase* next = tasks_.PopFront();
    budget.Withdraw();

    next->Run();

    ++count;
  }

  return count;
}

}  // namespace await::executors
