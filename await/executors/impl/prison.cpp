#include <await/executors/impl/prison.hpp>

#include <wheels/core/panic.hpp>

namespace await::executors {

//////////////////////////////////////////////////////////////////////

// [Await.Executor]
class PrisonExecutor final : public IExecutor {
 public:
  void Submit(TaskBase*, SchedulerHint) override {
    WHEELS_PANIC("Task has been submitted to Prison executor");
  }
};

//////////////////////////////////////////////////////////////////////

IExecutor& Prison() {
  static PrisonExecutor instance;
  return instance;
}

}  // namespace await::executors
