#pragma once

#include <await/executors/core/executor.hpp>

namespace await::executors {

// Runs scheduled tasks immediately in the current thread
IExecutor& Inline();

};  // namespace await::executors
