#include <await/executors/core/executor.hpp>

namespace await::executors {

// For tasks that should not run
IExecutor& Prison();

}  // namespace await::executors
