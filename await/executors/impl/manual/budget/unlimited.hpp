#pragma once

#include <await/executors/impl/manual/budget.hpp>

namespace await::executors::manual::budget {

struct Unlimited final : IBudget {
  bool Allow() const override {
    return true;
  }

  void Withdraw() override {
    // No-op
  }
};

}  // namespace await::executors::manual::budget
