#pragma once

#include <await/executors/impl/manual/budget.hpp>

#include <wheels/core/assert.hpp>

#include <cstdlib>

namespace await::executors::manual::budget {

struct Limit final : IBudget {
  Limit(size_t count)
      : count_(count) {
  }

  bool Allow() const override {
    return count_ > 0;
  }

  void Withdraw() override {
    WHEELS_ASSERT(Allow(), "Budget spent");
    --count_;
  }

 private:
  size_t count_;
};

}  // namespace await::executors::manual::budget
