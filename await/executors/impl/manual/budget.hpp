#pragma once

namespace await::executors::manual {

struct IBudget {
  virtual ~IBudget() = default;

  virtual bool Allow() const = 0;

  // Precondition: Allow() == true
  virtual void Withdraw() = 0;
};

}  // namespace await::executors::manual
