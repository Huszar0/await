#include <await/executors/impl/strand.hpp>

#include <wheels/core/panic.hpp>

namespace await::executors {

void Strand::Impl::Submit(TaskBase* task) {
  tasks_.Put(task);
  if (count_.fetch_add(1) == 0) {
    SubmitSelf();
  }
}

void Strand::Impl::SubmitSelf() {
  AddRef();
  underlying_->Submit(this, SchedulerHint::UpToYou);
}

void Strand::Impl::Run() noexcept {
  const size_t done = RunTasks(tasks_.TakeAll());

  const size_t left = count_.fetch_sub(done);
  if (left > done) {
    SubmitSelf();
  }

  // Memory management:
  ReleaseRef();  // Shared
}

size_t Strand::Impl::RunTasks(TaskBatch batch) {
  size_t count = 0;

  while (!batch.IsEmpty()) {
    TaskBase* task = batch.PopFront();
    task->Run();
    ++count;
  }

  return count;
}

}  // namespace await::executors
