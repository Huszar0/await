#include <await/executors/impl/inline.hpp>

namespace await::executors {

//////////////////////////////////////////////////////////////////////

// [Await.Executor]
class InlineExecutor final : public IExecutor {
 public:
  // IExecutor
  void Submit(TaskBase* task, SchedulerHint) override {
    task->Run();
  }
};

//////////////////////////////////////////////////////////////////////

IExecutor& Inline() {
  static InlineExecutor instance;
  return instance;
}

};  // namespace await::executors
