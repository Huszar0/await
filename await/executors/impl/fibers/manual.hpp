#pragma once

#include <await/executors/impl/manual/executor.hpp>
#include <await/executors/impl/manual/budget.hpp>

#include <await/fibers/core/fwd.hpp>
#include <await/fibers/core/runnable.hpp>
#include <await/fibers/core/resource_manager.hpp>

#include <await/thread/awaiter.hpp>

namespace await::executors::fibers {

// [Await.Executor]
class ManualExecutor final : public IManualExecutor,
                             private await::fibers::IRunnable,
                             private await::thread::ISuspendingAwaiter {
 public:
  ManualExecutor();
  ManualExecutor(await::fibers::IResourceManager& manager);

  ~ManualExecutor();

  // IExecutor
  void Submit(TaskBase* task, SchedulerHint) override {
    tasks_.PushBack(task);
  }

  size_t Drain() override;

  size_t RunAtMost(size_t limit) override;

  size_t TaskCount() const override {
    return tasks_.Size();
  }

  bool HasTasks() const override {
    return !tasks_.IsEmpty();
  }

  void Stop();

  // Statistics

  size_t FiberCount() const {
    return fiber_count_;
  }

 private:
  size_t Run(manual::IBudget& budget);

  void RunCarrier(await::fibers::Fiber* fiber) noexcept;

  // IRunnable
  // Carrier routine
  void RunCoro(void* bootstrap) noexcept override;

  // Scheduler
  TaskBase* PickTask();

  // ISuspendingAwaiter for carrier fibers
  void AwaitSuspend(await::thread::Handle carrier) override;

  await::fibers::Fiber* TakeCarrier();
  await::fibers::Fiber* CreateCarrier();

 private:
  await::fibers::IResourceManager& manager_;

  bool stop_requested_{false};

  wheels::IntrusiveForwardList<TaskBase> tasks_;

  // Pool of idle carrier fibers
  wheels::IntrusiveForwardList<await::fibers::Fiber> carriers_;

  // Validation
  size_t fiber_count_ = 0;

  size_t steps_ = 0;
  manual::IBudget* budget_;
};

}  // namespace await::executors::fibers
