#include <await/executors/impl/fibers/pool/pool.hpp>

namespace await::executors::fibers {

Pool::Pool(size_t threads, await::fibers::IResourceManager& manager)
    : runners::FiberWorkerRunner(manager),
      executors::pools::fast::ThreadPool(threads, {}) {
  SetRunner(*this);
  Start();
}

Pool::Pool(size_t threads)
    : Pool(threads, await::fibers::GlobalResourceManager()) {
}

}  // namespace await::executors::fibers
