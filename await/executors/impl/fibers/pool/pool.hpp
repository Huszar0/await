#pragma once

#include <await/executors/impl/pools/fast/thread_pool.hpp>

#include <await/executors/impl/fibers/pool/runner.hpp>
#include <await/fibers/core/resource_manager.hpp>

namespace await::executors::fibers {

// [Await.Executor]
class Pool final : private runners::FiberWorkerRunner,
                   public executors::pools::fast::ThreadPool {
 public:
  Pool(size_t threads, await::fibers::IResourceManager& manager);

  // Use global fiber manager
  explicit Pool(size_t threads);

  // Non-copyable
  Pool(const Pool&) = delete;
  Pool& operator=(const Pool&) = delete;

  // Non-movable
  Pool(Pool&&) = delete;
  Pool& operator=(Pool&&) = delete;
};

}  // namespace await::executors::fibers
