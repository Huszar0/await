#include <await/executors/impl/fibers/manual.hpp>

#include <await/executors/impl/manual/budget/limit.hpp>
#include <await/executors/impl/manual/budget/unlimited.hpp>

#include <await/fibers/core/fiber.hpp>
#include <await/fibers/core/suspend_guard.hpp>

#include <await/executors/impl/prison.hpp>

#include <wheels/core/assert.hpp>

using await::fibers::Fiber;

namespace await::executors::fibers {

using namespace manual;

ManualExecutor::ManualExecutor(await::fibers::IResourceManager& manager)
    : manager_(manager) {
}

ManualExecutor::ManualExecutor()
    : ManualExecutor(await::fibers::GlobalResourceManager()) {
}

ManualExecutor::~ManualExecutor() {
  if (!stop_requested_) {
    Stop();
  }
  WHEELS_ASSERT(fiber_count_ == 0, "Fibers leaked");
}

size_t ManualExecutor::Drain() {
  budget::Unlimited unlimited;
  return Run(unlimited);
}

size_t ManualExecutor::RunAtMost(size_t count) {
  budget::Limit limit(count);
  return Run(limit);
}

size_t ManualExecutor::Run(IBudget& budget) {
  budget_ = &budget;

  while (budget_->Allow() && !tasks_.IsEmpty()) {
    // Take from pool (or create fresh fiber)
    TaskBase* carrier_ = TakeCarrier();

    // Run as worker
    carrier_->Run();
  }

  return std::exchange(steps_, 0);
}

void ManualExecutor::Stop() {
  assert(tasks_.IsEmpty());

  stop_requested_ = true;

  // Clear pool
  while (carriers_.NonEmpty()) {
    carriers_.PopFront()->Run();
  }
}

TaskBase* ManualExecutor::PickTask() {
  if (budget_->Allow() && tasks_.NonEmpty()) {
    budget_->Withdraw();
    return tasks_.PopFront();
  } else {
    return nullptr;
  }
}

// Carrier routine
void ManualExecutor::RunCarrier(await::fibers::Fiber* self) noexcept {
  while (!stop_requested_) {
    self->SetName("Carrier");

    while (TaskBase* next = PickTask()) {
      await::fibers::SuspendGuard guard{*self};

      ++steps_;

      next->Run();

      if (guard.HasBeenSuspended()) {
        // Last task has suspended this carrier
        break;
      }
    }

    // 1) No more tasks to run (budget exhausted or task queue is empty)
    // 2) Or carrier is detached
    self->Suspend(/*awaiter=*/this);
  }

  --fiber_count_;
}

void ManualExecutor::RunCoro(void* bootstrap) noexcept {
  RunCarrier((await::fibers::Fiber*)bootstrap);
}

void ManualExecutor::AwaitSuspend(thread::Handle h) {
  // Add to pool
  carriers_.PushBack(h.GetFiber());
}

Fiber* ManualExecutor::TakeCarrier() {
  if (carriers_.NonEmpty()) {
    return carriers_.PopFront();
  }
  return CreateCarrier();
}

Fiber* ManualExecutor::CreateCarrier() {
  // Allocate resources

  auto stack = manager_.AllocateStack();
  auto id = manager_.GenerateId();

  // clang-format off

  auto* carrier = new await::fibers::Fiber(
      /*runnable=*/this,
      std::move(stack),
      manager_,
      // Carrier fiber should not run by itself
      executors::Prison(),
      id);

  // clang-format on

  ++fiber_count_;

  return carrier;
}

}  // namespace await::executors::fibers
