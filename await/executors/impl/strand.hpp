#pragma once

#include <await/executors/core/executor.hpp>

#include <await/infra/queues/mpsc_lock_free_queue.hpp>

#include <refer/ref_counted.hpp>

namespace await::executors {

// Strand / serial executor / asynchronous mutex
// Strand executes (via underlying executor) tasks (critical sections)
// non-concurrently and in the order they were submitted

// [Await.Executor]
class Strand final : public IExecutor {
 private:
  // [Await.Task(Shared,Service)]
  class Impl : public TaskBase,
               public refer::RefCounted<Impl> {
   private:
    using TaskQueue = queues::MPSCUnboundedLockFreeQueue<TaskBase>;
    using TaskBatch = TaskQueue::List;

   public:
    explicit Impl(IExecutor& executor)
        : underlying_(&executor) {
    }

    void Submit(TaskBase* critical_section);

    // ITask, service task
    void Run() noexcept override;

   private:
    void SubmitSelf();

    static size_t RunTasks(TaskBatch batch);

   private:
    IExecutor* underlying_{nullptr};

    TaskQueue tasks_;
    twist::ed::stdlike::atomic<size_t> count_{0};
  };

 public:
  explicit Strand(IExecutor& executor)
      : impl_(refer::New<Impl>(executor)) {
  }

  // Non-copyable
  Strand(const Strand&) = delete;
  Strand& operator=(const Strand&) = delete;

  // Non-movable
  Strand(Strand&&) = delete;
  Strand& operator=(Strand&&) = delete;

  IExecutor& AsExecutor() {
    return *this;
  }

  // IExecutor
  void Submit(TaskBase* critical_section, SchedulerHint) override {
    return impl_->Submit(critical_section);
  }

 private:
  refer::Ref<Impl> impl_;
};

}  // namespace await::executors
