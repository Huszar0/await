#pragma once

#include <await/executors/task/cancelled.hpp>

namespace await::executors::task {

// Checks for cancellation request
void Checkpoint();

inline bool KeepRunning() {
  Checkpoint();
  return true;
}

}  // namespace await::executors::task
