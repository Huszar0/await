#include <await/executors/task/context.hpp>
#include <await/executors/task/checkpoint.hpp>
#include <await/executors/task/executor.hpp>

#include <await/executors/task/cancelled.hpp>

#include <await/thread/this.hpp>

namespace await::executors::task {

IExecutor& Executor() {
  return thread::This()->GetExecutor();
}

carry::Context Context() {
  return thread::This()->GetContext();
}

void Checkpoint() {
  if (thread::This()->CancelRequested()) {
    throw CancelledException{};
  }
}

}  // namespace await::executors::task
