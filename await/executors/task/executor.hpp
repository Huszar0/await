#pragma once

#include <await/executors/core/executor.hpp>

namespace await::executors::task {

// Executor of the current task
IExecutor& Executor();

}  // namespace await::executors::task
