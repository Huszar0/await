#pragma once

#include <fallible/result/ignore.hpp>

namespace await::executors::task {

// Do not catch this exception (or rethrow after some cleanup)
struct CancelledException : fallible::IgnoreThisException {
  //
};

}  // namespace await::executors::task
