#pragma once

#include <carry/context.hpp>

namespace await::executors::task {

// Context of the current task
carry::Context Context();

}  // namespace await::executors::task
