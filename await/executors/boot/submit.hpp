#pragma once

#include <await/futures/make/execute.hpp>
#include <await/futures/run/go.hpp>

namespace await::executors {

//////////////////////////////////////////////////////////////////////

// For library bootstrapping
// Use futures::Execute instead

//////////////////////////////////////////////////////////////////////

/*
 * Usage:
 *
 * executors::Submit(thread_pool, []() {
 *   fmt::println("Running in thread pool");
 * });
 *
 */

template <typename F>
void Submit(IExecutor& where, F fun) {
  futures::Execute(where, std::forward<F>(fun)) | futures::Go();
}

}  // namespace await::executors
