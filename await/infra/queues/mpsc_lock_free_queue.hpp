#pragma once

#include <twist/ed/stdlike/atomic.hpp>

#include <wheels/intrusive/forward_list.hpp>

#include <algorithm>
#include <vector>

namespace await::queues {

//////////////////////////////////////////////////////////////////////

// Multi-producer/single-consumer (MPSC) unbounded lock-free queue

template <typename T>
class MPSCUnboundedLockFreeStack {
  using Node = wheels::IntrusiveForwardListNode<T>;

 public:
  ~MPSCUnboundedLockFreeStack() {
    // TODO
    ConsumeAll([](T* /*ignore*/) {});
  }

  void Push(T* item) {
    Node* node = item;
    while (!top_.compare_exchange_weak(node->next_, node,
                                       std::memory_order::release,
                                       std::memory_order::relaxed)) {
    }
  }

  template <typename F>
  void ConsumeAll(F handler) {
    auto top = top_.exchange(nullptr, std::memory_order::acquire);

    Node* curr = top;
    while (curr != nullptr) {
      Node* next = curr->next_;
      handler(curr->AsItem());
      curr = next;
    }
  }

  bool IsEmpty() const {
    return top_.load(std::memory_order::acquire) == nullptr;
  }

 private:
  twist::ed::stdlike::atomic<Node*> top_{nullptr};
};

//////////////////////////////////////////////////////////////////////

template <typename T>
class MPSCUnboundedLockFreeQueue {
 public:
  using List = wheels::IntrusiveForwardList<T>;

  void Put(T* value) {
    stack_.Push(value);
  }

  List TakeAll() {
    List reversed;
    stack_.ConsumeAll([&reversed](T* value) {
      reversed.PushFront(value);
    });
    return reversed;
  }

  bool IsEmpty() const {
    return stack_.IsEmpty();
  }

 private:
  MPSCUnboundedLockFreeStack<T> stack_;
};

}  // namespace await::queues
