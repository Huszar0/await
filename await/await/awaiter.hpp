#pragma once

#include <await/thread/awaiter.hpp>

#include <concepts>

namespace await {

// Awaiter

template <typename A>
concept Awaiter = requires(A awaiter) {
  // Result type
  typename A::ResultType;

  // Implements IAwaiter
  requires std::is_base_of_v<thread::IAwaiter, A>;

  { awaiter.AwaitReady() } -> std::same_as<bool>;
  { awaiter.AwaitResume() } -> std::same_as<typename A::ResultType>;
};

}  // namespace await
