#pragma once

#include <await/await/awaiter.hpp>
#include <await/await/awaitable.hpp>

#include <await/thread/this.hpp>

#include <utility>

namespace await {

//////////////////////////////////////////////////////////////////////

// Generic Await algorithm
// https://lewissbaker.github.io/2017/11/17/understanding-operator-co-await

//////////////////////////////////////////////////////////////////////

template <Awaitable A>
auto Await(A&& awaitable) {
  Awaiter auto awaiter = GetAwaiter(std::forward<A>(awaitable), ers::Lookup{});

  if (!awaiter.AwaitReady()) {
    thread::This()->Suspend(&awaiter);
  }
  return awaiter.AwaitResume();
}

}  // namespace await
