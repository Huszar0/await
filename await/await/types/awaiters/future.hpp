#pragma once

#include <await/thread/awaiter.hpp>

#include <await/futures/types/future.hpp>

#include <await/cancel/never.hpp>
#include <await/executors/task/cancelled.hpp>

#include <optional>

namespace await::ers {

//////////////////////////////////////////////////////////////////////

template <futures::SomeFuture Future>
class FutureAwaiter final : public thread::ISuspendingAwaiter,
                            public futures::IConsumer<typename Future::ValueType> {
 public:
  using ValueType = typename Future::ValueType;
  using ResultType = fallible::Result<ValueType>;

 public:
  explicit FutureAwaiter(Future&& future)
      : future_(std::move(future)),
        cancel_token_(cancel::Never()) {
  }

  // Awaiter protocol

  bool AwaitReady() {
    return false;

    /*
    if constexpr (Future::IsLazy) {
      return false;
    } else {
      if (future_.HasResult()) {
        maybe_result_.emplace(std::move(future_).GetReadyResult());
        return true;
      }
      return false;
    }
    */
  }

  void AwaitSuspend(thread::Handle waiter) override {
    waiter_ = waiter;
    cancel_token_ = waiter.CancelToken();
    how_to_resume_ = future_.SchedulerHint();

    std::move(future_).Start(this);
  }

  ResultType AwaitResume() {
    if (HasResult() && !cancel_token_.CancelRequested()) {
      return std::move(*maybe_result_);
    } else {
      // Propagate cancellation
      throw executors::task::CancelledException{};
    }
  }

  // futures::IConsumer<ValueType>

  cancel::Token CancelToken() override {
    return cancel_token_;
  }

  void Consume(fallible::Result<ValueType> result) noexcept override {
    maybe_result_.emplace(std::move(result));
    ResumeWaiter();
  }

  void Cancel() noexcept override {
    ResumeWaiter();  // <- Already has fired cancel token, so will throw
                     // CancelledException after resuming
  }

 private:
  bool HasResult() const {
    return maybe_result_.has_value();
  }

  void ResumeWaiter() {
    assert(!waiter_.CancelToken().HasLinks());
    waiter_.Resume(how_to_resume_);
  }

 private:
  Future future_;
  thread::Handle waiter_;
  executors::SchedulerHint how_to_resume_;
  cancel::Token cancel_token_;
  std::optional<ResultType> maybe_result_;
};

}  // namespace await::ers
