#pragma once

#include <await/futures/types/future.hpp>

#include <await/await/types/awaiters/future.hpp>

#include <await/await/awaitable.hpp>

/*
 * Synchronously run Future
 *
 * Example:
 *
 * auto result = await::Await(std::move(future));
 *
 */

namespace await::ers {

template <futures::SomeFuture Future>
Awaiter auto GetAwaiter(Future&& f, Lookup) {
  return FutureAwaiter<Future>(std::move(f));
}

}  // namespace await::ers
