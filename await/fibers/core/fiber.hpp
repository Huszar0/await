#pragma once

#include <await/executors/core/executor.hpp>

#include <await/fibers/core/coroutine.hpp>
#include <await/fibers/core/resource_manager.hpp>

#include <await/thread/thread.hpp>
#include <await/thread/awaiter.hpp>

#include <optional>
#include <string>

namespace await::fibers {

// Fiber = stackful coroutine + executor

// [Await.Task(Custom,User)]
class Fiber final : public executors::TaskBase,
                    public thread::IThread,
                    public wheels::IntrusiveForwardListNode<Fiber> {
 public:
  Fiber(IRunnable* runnable,
        wheels::MutableMemView stack,
        IResourceManager& manager,
        executors::IExecutor& executor,
        FiberId id);

  ~Fiber();

  // Expects fiber context
  static Fiber& Self();

  // Returns nullptr if not in fiber context
  static Fiber* TrySelf();

  IResourceManager& GetManager() {
    return resource_manager_;
  }

  void Schedule(executors::SchedulerHint hint);

  // thread::IThread

  bool IsFiber() const override {
    return true;
  }
  void Suspend(thread::IAwaiter* awaiter) override;
  void Resume(executors::SchedulerHint hint) override;

  // ITask
  void Run() noexcept override;

  FiberId GetId() const {
    return id_;
  }

  std::optional<std::string> GetName() const override;
  void SetName(std::string name) override;

  size_t GetSelectTwister() {
    return ++select_twister_;
  }

  // FiberWorkerRunner

  size_t GetSuspendCount() const {
    return suspend_count_;
  }

 private:
  // Symmetric transfer
  static void RunChain(Fiber* start);
  // Run this fiber and returns next fiber to run
  Fiber* DoRun() noexcept;

  void Stop();
  void Step();
  void DestroySelf();
  void ReleaseResources();

 private:
  wheels::MutableMemView stack_;
  Coroutine coroutine_;
  IResourceManager& resource_manager_;
  FiberId id_;
  thread::IAwaiter* awaiter_{nullptr};
  std::optional<std::string> name_;
  size_t suspend_count_{0};
  // TODO: Fast random
  size_t select_twister_{0};
};

}  // namespace await::fibers
