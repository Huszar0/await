#pragma once

#include <await/fibers/core/runnable.hpp>

#include <sure/context.hpp>

namespace await::fibers {

// Stackful asymmetric coroutine impl
// - Does not manage stacks
// - Does not support exceptions propagation
// - Unsafe Suspend

class Coroutine : private sure::ITrampoline {
 public:
  Coroutine(wheels::MutableMemView stack,
            IRunnable* runnable,
            void* bootstrap);

  // Context: Caller
  void Resume() noexcept;

  // Context: Coroutine
  void Suspend();

  // Context: Caller
  bool IsCompleted() const {
    return completed_;
  }

 private:
  // sure::ITrampoline
  [[noreturn]] void Run() noexcept override;

  // In trampoline:
  [[noreturn]] void Complete();

 private:
  IRunnable* runnable_;
  void* bootstrap_;

  sure::ExecutionContext context_;
  sure::ExecutionContext caller_context_;
  bool completed_{false};
};

}  // namespace await::fibers
