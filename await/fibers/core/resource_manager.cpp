#include <await/fibers/core/resource_manager.hpp>

#include <sure/stack/mmap.hpp>

#include <twist/ed/stdlike/atomic.hpp>

#include <wheels/core/size_literals.hpp>

using namespace wheels::size_literals;

namespace await::fibers {

//////////////////////////////////////////////////////////////////////

class StackAllocator {
  using StackView = wheels::MutableMemView;

 public:
  StackAllocator()
      : stack_size_(DefaultStackSize()) {
  }

  virtual void SetStackSize(size_t bytes) {
    stack_size_ = bytes;
  }

  wheels::MutableMemView Allocate() {
    return AllocateNew();
  }

  void Release(StackView stack) {
    Free(stack);
  }

 private:
  StackView AllocateNew() {
    return sure::Stack::AllocateBytes(stack_size_).Release();
  }

  void Free(StackView stack_view) {
    // Memory will be released on temporary destruction
    sure::Stack::Acquire(stack_view);
  }

  static size_t DefaultStackSize() {
#ifdef NDEBUG
    return 64_KiB;
#else
    return 128_KiB;
#endif
  }

 private:
  size_t stack_size_;
};

//////////////////////////////////////////////////////////////////////

class ResourceManager : public IResourceManager {
 public:
  FiberId GenerateId() override {
    return ids_.fetch_add(1) + 1;
  }

  void SetStackSize(size_t bytes) override {
    stacks_.SetStackSize(bytes);
  }

  wheels::MutableMemView AllocateStack() override {
    return stacks_.Allocate();
  }

  void ReleaseStack(wheels::MutableMemView stack) override {
    stacks_.Release(stack);
  }

 private:
  twist::ed::stdlike::atomic<size_t> ids_;
  StackAllocator stacks_;
};

//////////////////////////////////////////////////////////////////////

static ResourceManager global_manager;

IResourceManager& GlobalResourceManager() {
  return global_manager;
}

}  // namespace await::fibers
