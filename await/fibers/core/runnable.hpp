#pragma once

namespace await::fibers {

struct IRunnable {
  virtual ~IRunnable() = default;

  virtual void RunCoro(void* bootstrap) noexcept = 0;
};

}  // namespace await::fibers
