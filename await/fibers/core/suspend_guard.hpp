#pragma once

#include <await/fibers/core/fwd.hpp>

#include <cstdlib>

namespace await::fibers {

class SuspendGuard {
 public:
  // Hint to bypass thread local access
  explicit SuspendGuard(fibers::Fiber& self);

  SuspendGuard();

  bool HasBeenSuspended();

 private:
  fibers::Fiber& self_;
  size_t init_suspend_count_;
};

}  // namespace await::fibers
