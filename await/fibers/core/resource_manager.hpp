#pragma once

#include <await/fibers/core/id.hpp>
#include <await/fibers/core/stack_allocator.hpp>

namespace await::fibers {

//////////////////////////////////////////////////////////////////////

// Resource management

struct IResourceManager : stacks::IStackAllocator {
  virtual ~IResourceManager() = default;

  // Ids
  virtual FiberId GenerateId() = 0;

  // Stacks

  // IStackAllocator
};

//////////////////////////////////////////////////////////////////////

IResourceManager& GlobalResourceManager();

}  // namespace await::fibers
