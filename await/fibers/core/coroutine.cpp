#include <await/fibers/core/coroutine.hpp>

#include <wheels/core/assert.hpp>
#include <wheels/core/compiler.hpp>
#include <wheels/core/exception.hpp>

namespace await::fibers {

Coroutine::Coroutine(wheels::MutableMemView stack,
                     IRunnable* runnable,
                     void* bootstrap)
    : runnable_(runnable),
      bootstrap_(bootstrap) {
  context_.Setup(stack, /*trampoline=*/this);
}

void Coroutine::Run() noexcept {
  try {
    runnable_->RunCoro(bootstrap_);
  } catch (...) {
    WHEELS_PANIC("Unhandled user exception: " << wheels::CurrentExceptionMessage());
  }

  Complete();  // Never returns
  WHEELS_UNREACHABLE();
}

void Coroutine::Resume() noexcept {
  WHEELS_ASSERT(!completed_, "Coroutine completed");

  caller_context_.SwitchTo(context_);
}

void Coroutine::Suspend() {
  context_.SwitchTo(caller_context_);
}

void Coroutine::Complete() {
  completed_ = true;
  context_.ExitTo(caller_context_);
}

}  // namespace await::fibers
