#pragma once

namespace await::fibers {

// Reschedule current fiber
void Yield();

// Reschedule current fiber (via lazy futures)
// NB: For measuring overhead
void YieldFuture();

}  // namespace await::fibers
