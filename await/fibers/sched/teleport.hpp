#pragma once

#include <await/executors/core/executor.hpp>

namespace await::fibers {

//////////////////////////////////////////////////////////////////////

// Reschedule current fiber to executor `target`
void TeleportTo(executors::IExecutor& target);

//////////////////////////////////////////////////////////////////////

/*
 * Example:
 *
 * {
 *    TeleportGuard guard(strand);
 *
 *    // Serialized with other strand tasks,
 *    // access shared data structures
 * }
 *
 */

class TeleportGuard {
 public:
  explicit TeleportGuard(executors::IExecutor& to);

  ~TeleportGuard();

 private:
  executors::IExecutor& home_;
};

}  // namespace await::fibers
