#include <await/fibers/sched/sleep_for.hpp>

#include <await/futures/make/after.hpp>

#include <await/await.hpp>

namespace await::fibers {

void SleepFor(timers::Delay delay) {
  Await(futures::After(delay)).ExpectOk();
}

}  // namespace await::fibers
