#include <await/fibers/sched/yield.hpp>

#include <await/executors/task/checkpoint.hpp>

#include <await/await.hpp>

#include <await/futures/make/just.hpp>
#include <await/futures/combine/seq/yield.hpp>

#include <await/thread/suspend.hpp>

#include <wheels/core/assert.hpp>

namespace await::fibers {

namespace {

struct YieldAwaiter : thread::ISuspendingAwaiter {
  void AwaitSuspend(thread::Handle h) override {
    WHEELS_ASSERT(h.IsFiber(), "Yield failed: not a Fiber");
    h.Resume(executors::SchedulerHint::Yield);
  }
};

}  // namespace

void Yield() {
  executors::task::Checkpoint();

  YieldAwaiter awaiter;
  thread::Suspend(awaiter);

  executors::task::Checkpoint();
}

void YieldFuture() {
  Await(futures::Just() | futures::Yield()).Ignore("Cannot fail");
}

}  // namespace await::fibers
