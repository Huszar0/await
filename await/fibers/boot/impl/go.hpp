#pragma once

#include <await/executors/core/executor.hpp>
#include <await/fibers/core/runnable.hpp>

namespace await::fibers::boot {

void Go(IRunnable* runnable);

void Go(executors::IExecutor& where, IRunnable* runnable);

}  // namespace await::fibers::boot
