#pragma once

#include <await/fibers/core/runnable.hpp>

#include <wheels/core/panic.hpp>

namespace await::fibers::boot {

template <typename Fun>
struct Routine : IRunnable {
  Routine(Fun fun)
      : fun_(fun) {
  }

  void RunCoro(void*) noexcept override {
    try {
      fun_();
    } catch (...) {
      WHEELS_PANIC("Unhandled exception in fiber routine");
    }
    delete this;
  }

  Fun fun_;
};

template <typename Fun>
IRunnable* MakeRoutine(Fun fun) {
  return new Routine(std::move(fun));
}

}  // namespace await::fibers::boot
