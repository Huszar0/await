#pragma once

#include <await/fibers/boot/impl/routine.hpp>
#include <await/fibers/boot/impl/go.hpp>

namespace await::fibers {

//////////////////////////////////////////////////////////////////////

// For library bootstrapping!
// Use fibers::Pool + futures::Execute instead!

//////////////////////////////////////////////////////////////////////

// Go Considered Harmful
// https://vorpus.org/blog/notes-on-structured-concurrency-or-go-statement-considered-harmful/

// Spawn new fiber
// Precondition: AmIFiber() == true
// Inherit resource manager and executor from the current fiber

template <typename Fun>
void Go(Fun fun) {
  boot::Go(boot::MakeRoutine(std::move(fun)));
}

// Spawn new fiber in executor `where`

template <typename Fun>
void Go(executors::IExecutor& where, Fun fun) {
  boot::Go(where, boot::MakeRoutine(std::move(fun)));
}

}  // namespace await::fibers
