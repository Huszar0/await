#pragma once

#include <await/thread/awaiter.hpp>
#include <await/thread/suspend.hpp>

#include <await/infra/wait_queue/wake_all.hpp>

#include <wheels/core/assert.hpp>

namespace await::fibers {

class OneShotEvent {
 private:
  enum class WaitEither {
    Fired,
    Wait,
  };

  struct Waiter : thread::IMaybeSuspendingAwaiter {
    explicit Waiter(OneShotEvent& host)
        : event(host) {
    }

    thread::AwaitEither AwaitMaybeSuspend(thread::Handle h) {
      WHEELS_ASSERT(h.IsFiber(), "Not in fiber context");

      handle = h;

      auto either = event.Wait(this);

      if (either == WaitEither::Fired) {
        return thread::AwaitEither::Resume;
      } else {
        return thread::AwaitEither::Suspend;
      }
    }

    OneShotEvent& event;
    thread::Handle handle;
    // Intrusive list node
    Waiter* next{nullptr};
  };

 public:
  OneShotEvent() {
  }

  ~OneShotEvent() {
    WHEELS_ASSERT(waiters_.Sealed(), "Unexpected event state");
  }

  // One-shot
  void Fire() {
    ResumeWaiters(waiters_.Seal());
  }

  void Wait() {
    if (waiters_.Sealed()) {
      return;
    } else {
      Waiter waiter{*this};
      thread::Suspend(waiter);
    }
  }

 private:
  WaitEither Wait(Waiter* waiter) {
    return waiters_.TryAdd(waiter)
               ? WaitEither::Wait
               : WaitEither::Fired;
  }

  static void ResumeWaiters(Waiter* head) {
    Waiter* curr = head;

    while (curr != nullptr) {
      Waiter* next = curr->next;
      ResumeWaiter(curr->handle);
      curr = next;
    }
  }

  static void ResumeWaiter(thread::Handle fiber) {
    fiber.Resume(executors::SchedulerHint::Next);
  }

 private:
  wait_queue::SealableAtomicQueue<Waiter> waiters_;
};

}  // namespace await::fibers
