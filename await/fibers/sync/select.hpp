#pragma once

#include <await/fibers/sync/channel.hpp>
#include <await/fibers/core/check.hpp>

#include <twist/ed/spin/lock.hpp>
#include <twist/ed/mutex/locker.hpp>

#include <variant>
#include <array>

// https://golang.org/src/runtime/chan.go
// https://golang.org/src/runtime/select.go

namespace await::fibers {

namespace detail {

size_t GetSelectTwister();

// Allocation-free!

template <typename X, typename Y>
class SelectState {
  using SelectedValue = std::variant<X, Y>;
  using MaybeSelectedValue = std::variant<X, Y, std::monostate>;

  template <typename T, size_t Index>
  class Receiver : public BufferedChannelState<T>::Receiver {
   public:
    Receiver(SelectState* selector)
        : selector_(selector) {
    }

    bool MaybeReceive(T& value) override {
      return selector_->template TrySet<T, Index>(value);
    }

   private:
    SelectState* selector_;
  };

  class Awaiter : public thread::ISuspendingAwaiter {
   public:
    Awaiter(SelectState* selector)
        : selector_(selector) {
    }

    void AwaitSuspend(thread::Handle f) {
      selector_->SetFiber(f);
    }

   private:
    SelectState* selector_;
  };

 public:
  SelectedValue Select(Channel<X>& xs, Channel<Y>& ys) {
    auto* xs_state = xs.GetState();
    auto* ys_state = ys.GetState();

    Receiver<X, 0> x_receiver(this);
    Receiver<Y, 1> y_receiver(this);

    if (GetSelectTwister() % 2 == 0) {
      xs_state->Receive(&x_receiver);
      ys_state->Receive(&y_receiver);
    } else {
      ys_state->Receive(&y_receiver);
      xs_state->Receive(&x_receiver);
    }

    Awaiter awaiter(this);
    Suspend(awaiter);

    // Resumed, value is ready!

    {
      auto guard = xs_state->Lock();
      if (x_receiver.IsLinked()) {
        x_receiver.Unlink();
      }
    }

    {
      auto guard = ys_state->Lock();
      if (y_receiver.IsLinked()) {
        y_receiver.Unlink();
      }
    }

    // TODO: ensure that channel does not hold pointer to x/y receiver at this
    // point

    return std::move(selected_value_);
  }

  MaybeSelectedValue TrySelectNonAtomic(Channel<X>& xs, Channel<Y>& ys) {
    MaybeSelectedValue selected_value{std::in_place_index<2>, std::monostate{}};

    if (auto x = xs.TryReceive()) {
      selected_value.template emplace<0>(std::move(*x));
    } else if (auto y = ys.TryReceive()) {
      selected_value.template emplace<1>(std::move(*y));
    }

    return selected_value;
  }

  MaybeSelectedValue TrySelect(Channel<X>& xs, Channel<Y>& ys) {
    // return TrySelectNonAtomic(xs, ys);

    auto* xs_state = xs.GetState();
    auto* ys_state = ys.GetState();

    using Lock = twist::ed::Locker<twist::ed::SpinLock>;

    std::array<std::optional<Lock>, 2> locks;

    // Lock order
    if (xs_state->GetOrderingKey() < ys_state->GetOrderingKey()) {
      locks[0].emplace(xs_state->Lock());
      locks[1].emplace(ys_state->Lock());
    } else {
      locks[0].emplace(ys_state->Lock());
      locks[1].emplace(xs_state->Lock());
    }

    MaybeSelectedValue selected_value{std::in_place_index<2>, std::monostate{}};

    if (GetSelectTwister() % 2 == 0) {
      if (auto x = xs_state->TryReceiveLocked()) {
        selected_value.template emplace<0>(std::move(*x));
      } else if (auto y = ys_state->TryReceiveLocked()) {
        selected_value.template emplace<1>(std::move(*y));
      }
    } else {
      if (auto y = ys_state->TryReceiveLocked()) {
        selected_value.template emplace<1>(std::move(*y));
      } else if (auto x = xs_state->TryReceiveLocked()) {
        selected_value.template emplace<0>(std::move(*x));
      }
    }

    return std::move(selected_value);

    // Release locks in `locks` dtor
  }

 private:
  template <typename T, size_t Index>
  bool TrySet(T& value) {
    std::unique_lock lock(mutex_);
    if (!set_) {
      selected_value_.template emplace<Index>(std::move(value));
      set_ = true;
      if (fiber_.IsValid()) {
        lock.unlock();
        fiber_.Resume(executors::SchedulerHint::Next);
      }
      return true;
    }
    return false;
  }

  void SetFiber(thread::Handle f) {
    std::unique_lock lock(mutex_);

    if (set_) {
      lock.unlock();
      f.Resume(executors::SchedulerHint::Next);
      return;
    }
    fiber_ = f;
  }

 private:
  twist::ed::SpinLock mutex_;
  bool set_{false};
  SelectedValue selected_value_;
  thread::Handle fiber_;
};

}  // namespace detail

/*
 * Example:
 *
 * Channel<X> xs;
 * Channel<Y> ys;
 * ...
 * // value - std::variant<X, Y>
 * auto value = Select(xs, ys);
 * switch (value.index()) {
 *   case 0:
 *     // Handle std::get<0>(value);
 *     break;
 *   case 1:
 *     // Handle std::get<1>(value);
 *     break;
 * }
 */

template <typename X, typename Y>
auto Select(Channel<X>& xs, Channel<Y>& ys) {
  AWAIT_I_AM_FIBER;

  detail::SelectState<X, Y> selector;
  return selector.Select(xs, ys);
}

/*
 * Example:
 *
 * Channel<X> xs;
 * Channel<Y> ys;
 * ...
 * // value - std::variant<X, Y>
 * auto value = TrySelect(xs, ys);
 * switch (value.index()) {
 *   case 0:
 *     // Handle std::get<0>(value);
 *     break;
 *   case 1:
 *     // Handle std::get<1>(value);
 *     break;
 *   default:
 *     // No value
 *     break;
 * }
 */

template <typename X, typename Y>
auto TrySelect(Channel<X>& xs, Channel<Y>& ys) {
  AWAIT_I_AM_FIBER;

  detail::SelectState<X, Y> selector;
  return selector.TrySelect(xs, ys);
}

}  // namespace await::fibers
