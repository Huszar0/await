#pragma once

#include <await/thread/suspend.hpp>
#include <await/fibers/core/check.hpp>

#include <refer/ref_counted.hpp>

#include <twist/ed/spin/lock.hpp>
#include <twist/ed/mutex/locker.hpp>

#include <wheels/intrusive/list.hpp>
#include <wheels/core/assert.hpp>

#include <deque>
#include <optional>

/*
 * https://www.youtube.com/watch?v=cdpQMDgQP8Y
 * https://github.com/golang/go/issues/8899
 */

namespace await::fibers {

namespace detail {

template <typename T>
class BufferedChannelStateBase {
 public:
  using OrderingKey = uintptr_t;

  struct IReceiver {
    virtual ~IReceiver() = default;

    // Returns false if receiver does not need value anymore
    virtual bool MaybeReceive(T& value) = 0;
  };

  struct Receiver : public IReceiver,
                    public wheels::IntrusiveListNode<Receiver> {};

 private:
  using Mutex = twist::ed::SpinLock;
  using Locker = twist::ed::Locker<Mutex>;

  // Receiver

  class FiberReceiver : public thread::ISuspendingAwaiter,
                        public Receiver {
   public:
    FiberReceiver(Locker lock)
        : lock_(std::move(lock)) {
    }

    // IAwaiter
    void AwaitSuspend(thread::Handle f) override {
      // NB: order matters
      fiber_ = f;
      lock_.unlock();
      // Now fiber f is ready to receive value from sender
    }

    // IReceiver
    bool MaybeReceive(T& value) override {
      WHEELS_VERIFY(fiber_.IsValid(), "Invalid fiber handle");
      value_.emplace(std::move(value));
      fiber_.Resume(executors::SchedulerHint::Next);
      return true;
    }

    T ExtractValue() {
      return std::move(*value_);
    }

   private:
    Locker lock_;
    std::optional<T> value_;
    thread::Handle fiber_;
  };

  // Sender

  class Sender : public thread::ISuspendingAwaiter,
                 public wheels::IntrusiveListNode<Sender> {
   public:
    Sender(T&& value, Locker lock)
        : value_(std::move(value)),
          lock_(std::move(lock)) {
    }

    void AwaitSuspend(thread::Handle f) override {
      fiber_ = f;
      lock_.unlock();
    }

    void Resume() {
      fiber_.Resume(executors::SchedulerHint::UpToYou);
    }

    thread::Handle Handle() {
      return fiber_;
    }

    T value_;

   private:
    Locker lock_;
    thread::Handle fiber_;
  };

 public:
  explicit BufferedChannelStateBase(size_t capacity)
      : capacity_(capacity) {
    WHEELS_VERIFY(capacity > 0, "Capacity == 0");
  }

  ~BufferedChannelStateBase() {
    WHEELS_VERIFY(receivers_.IsEmpty(), "Receivers list is not empty");
  }

  void Send(T value) {
    AWAIT_I_AM_FIBER;

    Locker lock(mutex_);

    CheckInvariants();

    if (TrySendLocked(value)) {
      return;
    }

    // 3) Suspend sender

    WHEELS_VERIFY(buffer_.size() == capacity_, "Broken channel");

    Sender sender(std::move(value), std::move(lock));
    senders_.PushBack(&sender);
    Suspend(sender);
  }

  bool TrySend(T value) {
    Locker lock(mutex_);

    CheckInvariants();
    return TrySendLocked(value);
  }

  // TODO: std::optional<T> + Close
  T Receive() {
    AWAIT_I_AM_FIBER;

    Locker lock(mutex_);

    CheckInvariants();

    if (!buffer_.empty()) {
      T result = std::move(buffer_.front());
      BufferPopFront();
      return result;
    }

    WHEELS_VERIFY(buffer_.empty(), "Broken channel");

    FiberReceiver receiver(std::move(lock));
    receivers_.PushBack(&receiver);
    Suspend(receiver);
    // NB: lock released
    return receiver.ExtractValue();
  }

  std::optional<T> TryReceive() {
    Locker lock(mutex_);

    CheckInvariants();

    if (buffer_.empty()) {
      return std::nullopt;
    }

    T value = std::move(buffer_.front());
    BufferPopFront();
    return value;
  }

  // For Select

  void Receive(Receiver* receiver) {
    Locker lock(mutex_);

    CheckInvariants();

    if (!buffer_.empty()) {
      if (receiver->MaybeReceive(buffer_.front())) {
        BufferPopFront();
      }
      return;
    }

    receivers_.PushBack(receiver);
  }

  Locker Lock() {
    return Locker{mutex_};
  }

  // With mutex_
  std::optional<T> TryReceiveLocked() {
    if (!buffer_.empty()) {
      T result = std::move(buffer_.front());
      BufferPopFront();
      return result;
    } else {
      return std::nullopt;
    }
  }

  OrderingKey GetOrderingKey() const {
    return (OrderingKey)this;
  }

 private:
  // With mutex_
  bool TrySendLocked(T& value) {
    // 1) Send `value` directly to the receiver

    while (!receivers_.IsEmpty()) {
      Receiver* receiver = receivers_.PopFront();
      // NB: lock should be acquired!
      if (receiver->MaybeReceive(value)) {
        return true;  // Received
      }
    }

    // 2) Put `value` to buffer

    if (buffer_.size() < capacity_) {
      buffer_.push_back(std::move(value));
      return true;
    }

    return false;
  }

  // With mutex_
  void BufferPopFront() {
    buffer_.pop_front();

    if (!senders_.IsEmpty()) {
      Sender* sender = senders_.PopFront();
      buffer_.push_back(std::move(sender->value_));
      sender->Resume();
    }
  }

  // With mutex_
  void CheckInvariants() {
    WHEELS_ASSERT(receivers_.IsEmpty() || senders_.IsEmpty(), "Broken channel");
    // !senders.IsEmpty() => buffer_.size() == capacity
    WHEELS_ASSERT(senders_.IsEmpty() ||
                      (!senders_.IsEmpty() && buffer_.size() == capacity_),
                  "Broken channel");
    // !receivers.IsEmpty() => buffer_.empty()
    WHEELS_ASSERT(
        receivers_.IsEmpty() || (!receivers_.IsEmpty() && buffer_.empty()),
        "Broken channel");
  }

 private:
  const size_t capacity_;

  twist::ed::SpinLock mutex_;
  std::deque<T> buffer_;  // TODO: ring buffer, no allocations
  wheels::IntrusiveList<Receiver> receivers_;
  wheels::IntrusiveList<Sender> senders_;
};

template <typename T>
class BufferedChannelState final
    : public BufferedChannelStateBase<T>,
      public refer::RefCounted<BufferedChannelState<T>> {
 public:
  BufferedChannelState(size_t capacity)
      : BufferedChannelStateBase<T>(capacity) {
  }
};

}  // namespace detail

}  // namespace await::fibers
