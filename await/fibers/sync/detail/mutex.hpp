#pragma once

#include <await/thread/suspend.hpp>

#include <twist/ed/stdlike/atomic.hpp>

namespace await::fibers {

namespace detail {

class LockFreeMutex {
  struct States {
    enum _ {
      Unlocked = 0,
      LockedNoWaiters = 1,
      // + WaitNode address
    };
  };

  enum class AcquireEither : bool {
    Acquired = true,
    Wait = false,
  };

  using State = uint64_t;

  struct WaitNode {
    thread::Handle fiber;
    WaitNode* next{nullptr};
  };

  struct LockAwaiter : thread::IMaybeSuspendingAwaiter {
    explicit LockAwaiter(LockFreeMutex& mutex)
        : mutex_(mutex) {
    }
 
    thread::AwaitEither AwaitMaybeSuspend(thread::Handle h) override {
      node_.fiber = h;

      auto either = mutex_.AcquireOrEnqueue(&node_);

      if (either == AcquireEither::Acquired) {
        return thread::AwaitEither::Resume;
      } else {
        return thread::AwaitEither::Suspend;
      }
    }

    LockFreeMutex& mutex_;
    WaitNode node_;
  };

  struct UnlockAwaiter : thread::IAwaiter {
    UnlockAwaiter(thread::Handle next)
        : next_(next) {
    }

    // Symmetric Transfer
    thread::Handle AwaitSymmetricSuspend(thread::Handle h) override {
      thread::Handle next =
          next_;  // This awaiter can be destroyed after next line!
      h.Resume();
      return next;
    }

    thread::Handle next_;
  };

 public:
  void Lock() {
    if (TryAcquire()) {
      return;  // Fast path
    }
    LockAwaiter locker{*this};
    Suspend(locker);
  }

  bool TryLock() {
    return TryAcquire();
  }

  void Unlock() {
    Release();
  }

 private:
  bool CasState(State from, State to, std::memory_order mo) {
    return state_.compare_exchange_strong(from, to, mo);
  }

  // Wait-free
  bool TryAcquire() {
    return CasState(States::Unlocked, States::LockedNoWaiters,
                    std::memory_order::acquire);
  }

  AcquireEither AcquireOrEnqueue(WaitNode* waiter) {
    while (true) {
      State state = state_.load();
      if (state == States::Unlocked) {
        if (TryAcquire()) {
          return AcquireEither::Acquired;
        }
        continue;
      } else {
        if (state == States::LockedNoWaiters) {
          waiter->next = nullptr;
        } else {
          waiter->next = (WaitNode*)state;
        }
        if (CasState(state, (State)waiter, std::memory_order::release)) {
          return AcquireEither::Wait;
        }
        continue;
      }
    }
  }

  void Release() {
    if (head_ != nullptr) {
      ResumeNextOwner(TakeNextOwner());
      return;
    } else {
      // head_ list is empty
      // state = LockNoWaiters | Waiters list
      while (true) {
        State state = state_.load();
        if (state == States::LockedNoWaiters) {
          if (CasState(States::LockedNoWaiters, States::Unlocked,
                       std::memory_order::release)) {
            return;
          }
          continue;
        } else {
          // Wait list
          WaitNode* waiters = (WaitNode*)state_.exchange(
              States::LockedNoWaiters, std::memory_order::acquire);
          head_ = Reverse(waiters);
          ResumeNextOwner(TakeNextOwner());
          return;
        }
      }
    }
  }

  static WaitNode* Reverse(WaitNode* head) {
    WaitNode* prev = head;
    WaitNode* curr = prev->next;
    while (curr != nullptr) {
      WaitNode* next = curr->next;
      curr->next = prev;
      prev = curr;
      curr = next;
    }
    head->next = nullptr;
    return prev;
  }

  thread::Handle TakeNextOwner() {
    WaitNode* next = head_;
    head_ = head_->next;
    return next->fiber;
  }

  void ResumeNextOwner(thread::Handle f) {
    // f.Schedule();

    // f.Resume();

    UnlockAwaiter unlocker{f};
    Suspend(unlocker);
  }

 private:
  // Lock state + wait queue tail
  twist::ed::stdlike::atomic<State> state_{States::Unlocked};
  // Wait queue head
  WaitNode* head_{nullptr};
};

}  // namespace detail

}  // namespace await::fibers
