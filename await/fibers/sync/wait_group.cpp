#include <await/fibers/sync/wait_group.hpp>

#include <await/await.hpp>

#include <wheels/core/assert.hpp>

namespace await::fibers {

WaitGroup::WaitGroup() {
  cancel_state_.Allocate();
}

WaitGroup::~WaitGroup() {
  [[maybe_unused]] auto curr = state_.load();
  WHEELS_ASSERT(GetCount(curr) == 0, "Invalid WaitGroup state");
}

void WaitGroup::Add(uint64_t delta) {
  State prev = state_.fetch_add(delta << kCountShift);

  WHEELS_ASSERT(GetWaiterCount(prev) == 0, "Add || Wait");
}

void WaitGroup::Done() {
  State curr = state_.fetch_sub(kAddCount) - kAddCount;

  if (GetCount(curr) == 0) {
    if (GetWaiterCount(curr) > 0) {
      // No more new waiters: count == 0
      // No more Add-s: every Add -hb-> any Wait
      // => Seal wait list

      Waiter* head = waiters_.Seal();
      ResumeWaiters(head);
    }
  }
}

// One-shot, before Wait
void WaitGroup::RequestCancel() {
  cancel_state_.AsSource().RequestCancel();
}

WaitGroup::Waiter WaitGroup::AsyncWait() {
  return Waiter(*this);
}

void WaitGroup::Wait() {
  if (IsReady()) {
    return;  // Fast path
  }

  Await(AsyncWait()).ExpectOk();
}

void WaitGroup::AddFuture(futures::EagerUnitFuture ack) {
  Add(1);

  std::move(ack).Start(this);
}

// futures::IConsumer<Unit>

cancel::Token WaitGroup::CancelToken() {
  return cancel_state_.MakeToken();
}

void WaitGroup::Consume(fallible::Status status) noexcept {
  Done();

  if (status.Failed()) {
    RequestCancel();
  }
}

void WaitGroup::Cancel() noexcept {
  Done();
}

bool WaitGroup::IsReady() const {
  return GetCount(state_.load()) == 0;
}

WaitGroup::WaitEither WaitGroup::AddWaiter(Waiter* waiter) {
  while (true) {
    State curr = state_.load();

    if (GetCount(curr) == 0) {
      return WaitEither::Ready;
    }

    // No more Add-s expected
    if (state_.compare_exchange_weak(curr, curr + kAddWaiter)) {
      return waiters_.TryAdd(waiter) ? WaitEither::Queued
                                     : WaitEither::Ready;
    }
  }
}

void WaitGroup::ResumeWaiters(Waiter* head) {
  Waiter* curr = head;

  while (curr != nullptr) {
    Waiter* next = curr->next;
    curr->Consume(fallible::Ok());
    curr = next;
  }
}

}  // namespace await::fibers
