#include <await/fibers/sync/condvar.hpp>

#include <await/fibers/core/check.hpp>

namespace await::fibers {

CondVar::CondVar() {
}

void CondVar::Wait(Lock& lock) {
  AWAIT_I_AM_FIBER;

  auto epoch = waiters_.PrepareWait();
  lock.unlock();
  waiters_.WaitIf(epoch);
  lock.lock();
}

void CondVar::NotifyOne() {
  AWAIT_I_AM_FIBER;

  waiters_.NotifyOne();
}

void CondVar::NotifyAll() {
  AWAIT_I_AM_FIBER;

  waiters_.NotifyAll();
}

}  // namespace await::fibers
