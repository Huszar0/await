#pragma once

#include <await/fibers/sync/detail/channel.hpp>

#include <refer/ref.hpp>

namespace await::fibers {

// Buffered channel (~Golang)

// Does not support void type
// Use wheels::Unit instead (from <wheels/core/unit.hpp>)

namespace detail {

template <typename X, typename Y>
class SelectState;

}  // namespace detail

template <typename T>
class Channel {
  using State = detail::BufferedChannelState<T>;

  template <typename X, typename Y>
  friend class detail::SelectState;

 public:
  // Bounded channel, `capacity` > 0
  explicit Channel(size_t capacity)
      : state_(refer::New<State>(capacity)) {
  }

  // Unbounded channel
  Channel()
      : Channel(std::numeric_limits<size_t>::max()) {
  }

  // Blocking
  void Send(T value) {
    state_->Send(std::move(value));
  }

  // Non-blocking
  bool TrySend(T value) {
    return state_->TrySend(std::move(value));
  }

  // Blocking
  T Receive() {
    return state_->Receive();
  }

  // Non-blocking
  std::optional<T> TryReceive() {
    return state_->TryReceive();
  }

 private:
  // For Select
  State* GetState() {
    return state_.Get();
  }

 private:
  // Ownership shared between producers and consumers
  refer::Ref<State> state_;
};

}  // namespace await::fibers
