#pragma once

#include <await/fibers/sync/detail/mutex.hpp>

#include <await/fibers/sched/self.hpp>

// std::lock_guard
#include <mutex>

namespace await::fibers {

template <typename TImpl>
class TMutex {
  using ScopeGuard = std::lock_guard<TMutex<TImpl>>;

 public:
  void Lock() {
    impl_.Lock();
#ifndef NDEBUG
    owner_id_ = self::GetId();
#endif
  }

  bool TryLock() {
    if (impl_.TryLock()) {
#ifndef NDEBUG
      owner_id_ = self::GetId();
#endif
      return true;
    }
    return false;
  }

  void Unlock() {
#ifndef NDEBUG
    WHEELS_VERIFY(OwnedByThisFiber(), "Current fiber != owner");
    owner_id_ = kInvalidFiberId;
#endif

    impl_.Unlock();
  }

  // Usage: auto guard = mutex.Guard();
  ScopeGuard Guard() {
    return ScopeGuard(*this);
  }

#ifndef NDEBUG
  bool OwnedByThisFiber() const {
    return owner_id_ == self::GetId();
  }
#endif

  // Lockable
  // https://en.cppreference.com/w/cpp/named_req/Lockable
  // Be compatible with std::lock_guard and std::unique_lock

  // NOLINTNEXTLINE
  void lock() {
    Lock();
  }

  // NOLINTNEXTLINE
  bool try_lock() {
    return TryLock();
  }

  // NOLINTNEXTLINE
  void unlock() {
    Unlock();
  }

 private:
  TImpl impl_;
#ifndef NDEBUG
  FiberId owner_id_{kInvalidFiberId};
#endif
};

using Mutex = TMutex<detail::LockFreeMutex>;

}  // namespace await::fibers
