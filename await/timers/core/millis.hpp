#pragma once

#include <chrono>

namespace await::timers {

using Millis = std::chrono::milliseconds;

template <typename Dur>
inline Millis ToMillis(Dur dur) {
  return std::chrono::duration_cast<Millis>(dur);
}

}  // namespace await::timers
