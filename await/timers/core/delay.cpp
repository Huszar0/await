#include <await/timers/core/delay.hpp>

#include <await/timers/core/timer_keeper.hpp>

#include <compass/locate.hpp>

namespace await::timers {

Delay::Delay(Millis ms)
  : Delay(ms, compass::Locate<ITimerKeeper>()) {
  //
}

}  // namespace await::timers
