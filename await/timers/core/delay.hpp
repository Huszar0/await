#pragma once

#include <await/timers/core/fwd.hpp>
#include <await/timers/core/millis.hpp>

#include <chrono>

namespace await::timers {

// Delay in milliseconds bound to ITimerKeeper instance

struct Delay {
  Millis millis;
  ITimerKeeper& timer_keeper;

  Delay(Millis ms, ITimerKeeper& tk)
      : millis(ms),
        timer_keeper(tk) {
    //
  }

  Delay(Millis ms);

  template <typename Dur>
  Delay(Dur dur)
    : Delay(ToMillis(dur)) {
    //
  }
};

}  // namespace await::timers
