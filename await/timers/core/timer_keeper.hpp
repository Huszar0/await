#pragma once

#include <await/timers/core/timer.hpp>
#include <await/timers/core/delay.hpp>

namespace await::timers {

struct ITimerKeeper {
  virtual ~ITimerKeeper() = default;

  virtual void AddTimer(TimerBase* timer) = 0;

  virtual Delay Delay(Millis millis) = 0;
};

}  // namespace await::timers
