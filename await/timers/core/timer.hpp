#pragma once

#include <await/executors/core/task.hpp>

#include <await/cancel/token.hpp>

#include <await/timers/core/millis.hpp>

namespace await::timers {

//////////////////////////////////////////////////////////////////////

struct ITimer {
  virtual ~ITimer() = default;

  virtual Millis Delay() const = 0;
  virtual bool Periodic() const = 0;

  // Cancellation
  virtual cancel::Token CancelToken() = 0;
};

//////////////////////////////////////////////////////////////////////

struct ITimerHandler {
  virtual ~ITimerHandler() = default;

  virtual void Ready() = 0;
};

//////////////////////////////////////////////////////////////////////

struct TimerBase : ITimer,
                   ITimerHandler,
                   wheels::IntrusiveForwardListNode<TimerBase> {
  // Metadata
  uint64_t deadline = 0;
};

}  // namespace await::timers
