#pragma once

#include <await/executors/core/executor.hpp>
#include <await/executors/impl/inline.hpp>
#include <await/timers/core/timer_keeper.hpp>

#include <twist/ed/stdlike/atomic.hpp>
#include <twist/ed/stdlike/mutex.hpp>
#include <twist/ed/stdlike/thread.hpp>

#include <chrono>
#include <queue>
#include <mutex>

namespace await::timers {

// Naive implementation!
// TODO: TimerWheel + MP/SC lock-free queue

class Queue : public ITimerKeeper {
  using Millis = std::chrono::milliseconds;

  using Clock = std::chrono::steady_clock;
  using TimePoint = Clock::time_point;

  struct TimerEntry {
    TimePoint deadline;
    TimerBase* timer;

    bool operator<(const TimerEntry& rhs) const {
      return deadline > rhs.deadline;
    }
  };

 public:
  Queue()
      : Queue(await::executors::Inline()) {
  }

  Queue(executors::IExecutor& /*executor*/)
      : worker_thread_([this]() {
          Work();
        }) {
  }

  void Stop() {
    stop_requested_.store(true);
    worker_thread_.join();
  }

  ~Queue() {
    if (!stop_requested_) {
      Stop();
    }
  }

  void AddTimer(TimerBase* timer) override {
    std::lock_guard guard(mutex_);
    queue_.push({ToDeadLine(timer->Delay()), timer});
  }

  struct Delay Delay(std::chrono::milliseconds millis) override {
    return {millis, *this};
  }

 private:
  void Work() {
    while (!stop_requested_.load()) {
      PollTimerQueue();
      twist::ed::stdlike::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
    Shutdown();
  }

  void PollTimerQueue() {
    auto timers = GrabReadyTimers();

    TimerBase* timer = timers;

    while (timer != nullptr) {
      TimerBase* next = (TimerBase*)timer->Next();
      timer->Ready();
      timer = next;
    }
  }

  TimerBase* GrabReadyTimers() {
    auto now = Clock::now();

    std::lock_guard guard(mutex_);

    TimerBase* ready_timers = nullptr;

    while (!queue_.empty()) {
      auto& next = queue_.top();

      if (next.deadline > now) {
        break;
      }

      {
        next.timer->SetNext(ready_timers);
        ready_timers = next.timer;
      }

      queue_.pop();
    }

    return ready_timers;
  }

  void Shutdown() {
    while (!queue_.empty()) {
      TimerEntry top = queue_.top();
      queue_.pop();
      // Is it ok???
      top.timer->Ready();
    }
  }

  static TimePoint ToDeadLine(Millis delay) {
    return Clock::now() + delay;
  }

 private:
  std::priority_queue<TimerEntry> queue_;
  twist::ed::stdlike::mutex mutex_;
  twist::ed::stdlike::atomic<bool> stop_requested_{false};
  twist::ed::stdlike::thread worker_thread_;
};

}  // namespace await::timers
