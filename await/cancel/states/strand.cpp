#include <await/cancel/states/strand.hpp>

#include <wheels/core/assert.hpp>

namespace await::cancel {

namespace detail {

// Single consumer

bool StrandStateBase::SetHandler(HandlerBase* handler) {
  State curr = State::Value(kInitial);

  // Initial -> Handler
  if (state_.compare_exchange_strong(curr, State::Pointer(handler),
                                     std::memory_order::release,
                                     std::memory_order::relaxed)) {
    return true;  // Linked
  }

  // Else

  // Paranoid
  WHEELS_VERIFY(!curr.IsPointer(), "Invalid state: > 1 handlers");

  WHEELS_ASSERT(curr.AsValue() == kCancelRequested, "Unexpected state");

  // Cancel requested
  handler->Forward(Signal::Cancel());

  return false;  // Cancelled
}

// * kInitial -> No work
// * kCancelRequested -> No work
// * Handler -> Strong Cas -> kInitial
//    * Handler -> Release, Ok
//    * Different handler? – No, need assertion
//    * kInitial -> No more work
//    * kCancelRequested -> No more work
//

void StrandStateBase::ResetHandler(HandlerBase* expected) {
  State curr = state_.load(std::memory_order::acquire);

  if (IsCancelRequested(curr)) {
    return;  // Terminal state
  }

  if (IsInitial(curr)) {
    return;  // No work
  }

  // Handler

  auto* handler = curr.AsPointerTo<HandlerBase>();

  if (expected != nullptr) {
    WHEELS_VERIFY(handler == expected, "Unexpected handler");
  }

  bool ok = state_.compare_exchange_strong(curr, State::Value(kInitial),
                                           std::memory_order::release,
                                           std::memory_order::relaxed);

  if (ok) {
    // Release handler
    handler->Forward(Signal::Release());
    return;
  } else {
    // Different handler
    // Paranoid
    WHEELS_VERIFY(!curr.IsPointer(), "Unexpected state: different handler");

    // Value

    if (IsInitial(curr)) {
      return;  // No more work
    }

    if (IsCancelRequested(curr)) {
      return;
    }

    WHEELS_PANIC("Unreachable");
  }
}

void StrandStateBase::ResetHandler() {
  ResetHandler(nullptr);
}

bool StrandStateBase::HasHandlers() const {
  return state_.load().IsPointer();
}

bool StrandStateBase::CancelRequested() const {
  return IsCancelRequested(state_.load(std::memory_order::acquire));
}

// Single producer

void StrandStateBase::Forward(Signal signal) {
  if (signal.CancelRequested()) {
    RequestCancel();
  } else {
    RemoveHandler(nullptr);
  }

  // Memory management!
  ReleaseRef();
}

void StrandStateBase::RequestCancel() {
  // Set terminal state
  State prev = state_.exchange(State::Value(kCancelRequested),
                               std::memory_order::acq_rel);

  if (prev.IsPointer()) {
    auto* handler = prev.AsPointerTo<HandlerBase>();
    handler->Forward(Signal::Cancel());
  }
}

}  // namespace detail

}  // namespace await::cancel
