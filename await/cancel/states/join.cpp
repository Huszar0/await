#include <await/cancel/states/join.hpp>

namespace await::cancel {

namespace detail {

// Wait-free
void JoinStateBase::Forward(Signal signal) {
  if (signal.CancelRequested()) {
    RequestCancel();
  } else if (bool last = CloseInput()) {
    ResetHandler();
  }

  ReleaseRef();
}

// Returns true if last one is closed
bool JoinStateBase::CloseInput() {
  return inputs_left_.fetch_sub(1) == 1;
}

}  // namespace detail

}  // namespace await::cancel
