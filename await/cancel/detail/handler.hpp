#pragma once

#include <await/cancel/detail/state.hpp>

#include <await/executors/core/executor.hpp>

#include <await/thread/this.hpp>

#include <refer/ref_counted.hpp>

#include <wheels/core/defer.hpp>

namespace await::cancel {

namespace detail {

// [Await.Task(Shared,User)]
template <typename F>
class Handler final : public HandlerBase,
                      public executors::TaskBase,
                      public refer::RefCounted<Handler<F>> {
  using Self = Handler<F>;

 public:
  Handler(F handler, executors::IExecutor& e, carry::Context ctx)
      : handler_(std::move(handler)),
        executor_(e),
        user_context_(std::move(ctx)) {
  }

  // IHandler

  // Release-reference operation
  void Forward(Signal signal) override {
    if (signal.CancelRequested()) {
      ExecuteHandler();
    } else {
      Self::ReleaseRef();
    }
  }

  // ITask

  void Run() noexcept override {
    RunHandler();

    // Memory management:
    Self::ReleaseRef();
  }

 private:
  void SetupContext(thread::IThread* carrier) {
    carrier->SetExecutor(executor_);
    carrier->SetUserContext(user_context_);
  }

  void RunHandler() {
    auto* carrier = thread::This();

    SetupContext(carrier);

    wheels::Defer cleanup([carrier]() {
      carrier->ResetContext();
    });

    // Run user code
    handler_();
  }

 private:
  void ExecuteHandler() {
    executor_.Submit(this, executors::SchedulerHint::UpToYou);
  }

 private:
  F handler_;

  executors::IExecutor& executor_;
  carry::Context user_context_;
};

}  // namespace detail

}  // namespace await::cancel
