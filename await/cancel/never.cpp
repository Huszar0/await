#include <await/cancel/never.hpp>

#include <refer/unmanaged.hpp>

#include <wheels/core/panic.hpp>

namespace await::cancel {

struct NeverState final : detail::StateBase,
                          refer::Unmanaged {
  void Forward(Signal signal) override {
    if (signal.CancelRequested()) {
      RequestCancel();
    }
  }

  void RequestCancel() override {
    WHEELS_PANIC("Unexpected cancel request");
  }

  bool Cancellable() const override {
    return false;
  }

  bool CancelRequested() const override {
    return false;
  }

  bool AddHandler(HandlerBase* handler) override {
    // Immediately release
    handler->Forward(cancel::Signal::Release());
    return false;  // Released
  }

  void RemoveHandler(HandlerBase* /*handler*/) override {
    // No-op
  }

  bool HasHandlers() const override {
    return false;
  }
};

NeverState never_state;

// Allocation-free

Token Never() {
  return Token::FromState(&never_state);
}

}  // namespace await::cancel
