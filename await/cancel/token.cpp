#include <await/cancel/token.hpp>

namespace await::cancel {

//////////////////////////////////////////////////////////////////////

class Link Token::Link(Source&& source) {
  // NB: Pass owning reference
  HandlerBase* handler = source.state_.Release();

  if (bool linked = state_->AddHandler(handler)) {
    return {state_, handler};
  } else {
    return Link::None();
  }
}

class Link Token::Link(Source& source) {
  auto state = source.state_;
  HandlerBase* handler = state.Release();

  if (bool linked = state_->AddHandler(handler)) {
    return {state_, handler};
  } else {
    return Link::None();
  }
}

void Token::Subscribe(HandlerBase* handler) {
  state_->AddHandler(handler);
}

//////////////////////////////////////////////////////////////////////

Link::Link()
    : state_(nullptr),
      handler_(nullptr) {
}

Link::Link(detail::StateBase* state, HandlerBase* handler)
    : state_(state),
      handler_(handler) {
}

Link Link::None() {
  return {};
}

Link::Link(Link&& that)
  : state_(std::exchange(that.state_, nullptr)),
    handler_(std::exchange(that.handler_, nullptr)) {
}

Link& Link::operator=(Link&& that) {
  Destroy();

  state_ = std::exchange(that.state_, nullptr);
  handler_ = std::exchange(that.handler_, nullptr);

  return *this;
}

Link::~Link() {
  Destroy();
}

void Link::Reset() {
  Destroy();
}

void Link::Destroy() {
  if (handler_ != nullptr) {
    state_->RemoveHandler(handler_);
  }

  state_ = nullptr;
  handler_ = nullptr;
}

}  // namespace await::cancel
