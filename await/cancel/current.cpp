#include <await/cancel/current.hpp>

#include <await/thread/this.hpp>

namespace await::cancel {

Token Current() {
  return thread::This()->GetCancelToken();
}

}  // namespace await::cancel
