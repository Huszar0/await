#pragma once

#include <await/cancel/detail/state.hpp>

namespace await::cancel {

// Asynchronous cooperative cancellation

//////////////////////////////////////////////////////////////////////

// Forward declarations

class Token;
class Source;
class Link;

//////////////////////////////////////////////////////////////////////

// Non-owning reference to cancel state

// Structured concurrency assumption:
// Consumer lifetime >= producer lifetime

class Token {
  friend class Source;

 public:
  // Movable
  Token(Token&&) = default;
  Token& operator=(Token&&) = default;

  // Copyable
  Token(const Token&) = default;
  Token& operator=(const Token&) = default;

  bool Cancellable() const {
    return state_->Cancellable();
  }

  // Polling
  bool CancelRequested() const {
    return state_->CancelRequested();
  }

  class Link Link(Source&& source);
  class Link Link(Source& source);

  void Subscribe(HandlerBase* handler);

  bool HasLinks() const {
    return state_->HasHandlers();
  }

  // Private!
  static Token FromState(detail::StateBase* state) {
    return {state};
  }

 private:
  Token(detail::StateBase* state)
      : state_(state) {
  }

 private:
  detail::StateBase* state_;
};

//////////////////////////////////////////////////////////////////////

// Owning reference to cancel state

class Source {
  friend class Token;
  friend class Token;

 public:
  // Non-copyable
  Source(const Source&) = delete;
  Source& operator=(const Source&) = delete;

  // Movable
  Source(Source&&) = default;
  Source& operator=(Source&&) = default;

  Token MakeToken() {
    return Token{state_.Get()};
  }

  // Issues cancel request for produced tokens
  // Asynchronous, cooperative
  void RequestCancel() {
    state_->RequestCancel();
  }

  // Private!
  static Source FromState(detail::StateRef state) {
    return Source{std::move(state)};
  }

 private:
  Source(detail::StateRef state)
      : state_(std::move(state)) {
  }

 private:
  detail::StateRef state_;
};

//////////////////////////////////////////////////////////////////////

class Link {
  friend class Token;

 public:
  // No link actually
  Link();

  // Non-copyable
  Link(const Link&) = delete;
  Link& operator=(const Link&) = delete;

  // Movable
  Link(Link&& that);
  Link& operator=(Link&& that);

  ~Link();

  void Reset();

  static Link None();

 private:
  Link(detail::StateBase* state, HandlerBase* handler);

  void Destroy();

 private:
  detail::StateBase* state_ = nullptr;
  HandlerBase* handler_ = nullptr;
};

}  // namespace await::cancel
