#pragma once

#include <await/cancel/token.hpp>

namespace await::cancel {

// Cancel token that will never be cancelled
Token Never();

}  // namespace await::cancel
