#pragma once

#include "await/await/algorithm.hpp"

// Well-known types
#include <await/await/types/future.hpp>
