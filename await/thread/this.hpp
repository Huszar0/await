#pragma once

#include <await/thread/thread.hpp>

namespace await::thread {

//////////////////////////////////////////////////////////////////////

IThread* This();

//////////////////////////////////////////////////////////////////////

class Scope {
 public:
  Scope(IThread* thread);

  Scope(IThread& thread)
      : Scope(&thread) {
  }

  ~Scope();

 private:
  IThread* restore_;
};

}  // namespace await::thread
