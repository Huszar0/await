#include <await/thread/system.hpp>

namespace await::thread {

void SystemThread::Suspend(IAwaiter* awaiter) {
  uint32_t epoch = parking_lot_.Prepare();

  auto next = awaiter->AwaitSymmetricSuspend({this});

  if (next.IsValid()) {
    WHEELS_ASSERT(next.GetHandle() == this,
                  "System threads do not support symmetric transfer");
    return;  // Returns immediately
  }

  parking_lot_.ParkIf(epoch);
}

void SystemThread::Resume() {
  parking_lot_.Wake();
}

void SystemThread::SetName(std::string) {
  // TODO
}

std::optional<std::string> SystemThread::GetName() const {
  return std::nullopt; // TODO
}

}  // namespace await::thread
