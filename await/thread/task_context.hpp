#pragma once

#include <await/executors/core/executor.hpp>
#include <await/cancel/token.hpp>

#include <carry/context.hpp>
#include <carry/carrier.hpp>

namespace await::thread {

struct TaskContext : carry::ICarrier {
  TaskContext();
  TaskContext(executors::IExecutor* executor);

  // Setup task context
  void SetExecutor(executors::IExecutor& executor);
  void SetUserContext(carry::Context context);
  void SetCancelToken(cancel::Token token);

  // Reset task context
  void ResetContext();

  // Access task context

  executors::IExecutor& GetExecutor() const;

  const carry::Context& GetUserContext() const {
    return user_context_;
  }

  cancel::Token GetCancelToken() const {
    return cancel_token_;
  }

  bool CancelRequested() const {
    return cancel_token_.CancelRequested();
  }

  // carry::ICarrier

  void Set(carry::Context context) override {
    SetUserContext(std::move(context));
  }

  const carry::Context& GetContext() override {
    return GetUserContext();
  }

 private:
  executors::IExecutor* executor_;
  carry::Context user_context_;
  cancel::Token cancel_token_;
};

}  // namespace await::thread
