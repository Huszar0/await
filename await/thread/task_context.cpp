#include <await/thread/task_context.hpp>

#include <await/thread/this.hpp>

// Defaults
#include <carry/empty.hpp>
#include <await/cancel/never.hpp>

#include <carry/carrier.hpp>

#include <compass/map.hpp>

namespace await::thread {

//////////////////////////////////////////////////////////////////////

struct Injector : compass::Locator<carry::ICarrier> {
  Injector() {
    compass::Map().Add(*this);
  }

  // rt::Locator
  carry::ICarrier* Locate(compass::Tag<carry::ICarrier>) override {
    return thread::This();
  }
};

static Injector injector;

//////////////////////////////////////////////////////////////////////

TaskContext::TaskContext(executors::IExecutor* executor)
    : executor_(executor),
      user_context_(carry::Empty()),
      cancel_token_(cancel::Never()) {
}

TaskContext::TaskContext()
    : TaskContext(nullptr) {
}

void TaskContext::SetExecutor(executors::IExecutor& executor) {
  executor_ = &executor;
}

void TaskContext::SetUserContext(carry::Context context) {
  user_context_ = std::move(context);
}

void TaskContext::SetCancelToken(cancel::Token token) {
  cancel_token_ = std::move(token);
}

void TaskContext::ResetContext() {
  executor_ = nullptr;
  user_context_ = carry::Empty();
  cancel_token_ = cancel::Never();
}

// Accessors

executors::IExecutor& TaskContext::GetExecutor() const {
  WHEELS_ASSERT(executor_ != nullptr,
                "Cannot access current executor: I'm not a task");
  return *executor_;
}

}  // namespace await::thread
