#include <await/thread/parking_lot.hpp>

#include <twist/ed/wait/sys.hpp>

namespace await::thread::system {

void ParkingLot::ParkIf(Epoch old) {
  twist::ed::Wait(epoch_, old);
}

ParkingLot::Epoch ParkingLot::Prepare() {
  return epoch_.load();
}

void ParkingLot::Wake() {
  auto wake_key = twist::ed::PrepareWake(epoch_);
  epoch_.fetch_add(1);
  twist::ed::WakeOne(wake_key);
}

}  // namespace await::thread::system
