#pragma once

#include <await/thread/awaiter.hpp>
#include <await/thread/task_context.hpp>

#include <optional>
#include <string>

namespace await::thread {

struct IThread : TaskContext {
  virtual ~IThread() = default;

  virtual void Suspend(IAwaiter*) = 0;
  virtual void Resume(executors::SchedulerHint) = 0;

  // Workaround for symmetric transfer
  virtual bool IsFiber() const = 0;

  virtual void SetName(std::string name) = 0;
  virtual std::optional<std::string> GetName() const = 0;
};

}  // namespace await::thread
