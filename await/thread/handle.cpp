#include <await/thread/handle.hpp>

#include <await/thread/thread.hpp>

#include <await/fibers/core/fiber.hpp>

#include <wheels/core/assert.hpp>

namespace await::thread {

void Handle::Validate() const {
  WHEELS_ASSERT(thread_ != nullptr, "Invalid thread handle");
}

IThread* Handle::Get() const {
  Validate();
  return thread_;
}

IThread* Handle::Release() {
  Validate();
  return std::exchange(thread_, nullptr);
}

bool Handle::IsFiber() const {
  return Get()->IsFiber();
}

static fibers::Fiber* AsFiber(IThread* thread) {
  return dynamic_cast<fibers::Fiber*>(thread);
}

fibers::Fiber* Handle::GetFiber() const {
  auto* fiber = AsFiber(Get());
  WHEELS_ASSERT(fiber != nullptr, "Not a fiber");
  return fiber;
}

void Handle::Resume(executors::SchedulerHint hint) {
  thread_->Resume(hint);
}

cancel::Token Handle::CancelToken() {
  return thread_->GetCancelToken();
}

}  // namespace await::thread
