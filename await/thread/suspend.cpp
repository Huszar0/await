#include <await/thread/suspend.hpp>

#include <await/thread/this.hpp>

namespace await::thread {

void Suspend(IAwaiter& awaiter) {
  This()->Suspend(&awaiter);
}

}  // namespace await::thread
