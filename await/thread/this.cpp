#include <await/thread/this.hpp>

#include <await/thread/system.hpp>

#include <twist/ed/local/var.hpp>
#include <twist/ed/local/ptr.hpp>

namespace await::thread {

//////////////////////////////////////////////////////////////////////

twist::ed::ThreadLocal<SystemThread> system_thread;

//////////////////////////////////////////////////////////////////////

TWIST_DECLARE_TL_PTR_INIT(IThread, this_thread, system_thread.Get());

//////////////////////////////////////////////////////////////////////

IThread* This() {
  return this_thread;
}

//////////////////////////////////////////////////////////////////////

Scope::Scope(IThread* thread)
  : restore_(this_thread) {
  this_thread = thread;
}

Scope::~Scope() {
  this_thread = restore_;
}

}  // namespace await::thread
