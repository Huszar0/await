#pragma once

#include <await/thread/thread.hpp>

#include <await/thread/parking_lot.hpp>

namespace await::thread {

class SystemThread : public IThread {
 public:
  bool IsFiber() const override {
    return false;
  }

  void Resume(executors::SchedulerHint) override {
    Resume();
  }

  void Suspend(IAwaiter* awaiter) override;

  void SetName(std::string) override;
  std::optional<std::string> GetName() const override;

 private:
  void Resume();

 protected:
  system::ParkingLot parking_lot_;
};

}  // namespace await::thread
