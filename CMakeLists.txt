cmake_minimum_required(VERSION 3.14)
project(await)

include(cmake/Logging.cmake)

include(cmake/CheckCompiler.cmake)
include(cmake/CompileOptions.cmake)
include(cmake/Sanitize.cmake)
include(cmake/PrintDiagnostics.cmake)

option(AWAIT_TESTS "Build Await tests" OFF)
option(AWAIT_EXAMPLES "Build Await examples" OFF)
option(AWAIT_WORKLOADS "Build Await workloads" OFF)
option(AWAIT_DEVELOPER "Await development mode" OFF)
option(AWAIT_MIMALLOC "Use mimalloc memory allocator" OFF)

add_subdirectory(third_party)

add_subdirectory(await)

if(AWAIT_TESTS OR AWAIT_DEVELOPER)
    add_subdirectory(tests)
endif()

if(AWAIT_WORKLOADS OR AWAIT_DEVELOPER)
    add_subdirectory(workloads)
endif()

if(AWAIT_EXAMPLES OR AWAIT_DEVELOPER)
    add_subdirectory(examples)
    add_subdirectory(tutorial)
endif()

if(AWAIT_DEVELOPER)
    add_subdirectory(dev)
endif()

add_subdirectory(demo)
