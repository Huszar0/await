#include <await/executors/impl/fibers/pool.hpp>

#include <await/futures/make/execute.hpp>
#include <await/futures/combine/seq/start.hpp>
#include <await/futures/combine/seq/yield.hpp>
#include <await/futures/combine/par/all.hpp>
#include <await/futures/run/go.hpp>

#include <await/fibers/sched/yield.hpp>

#include <await/threads/await/future.hpp>

#include <wheels/core/stop_watch.hpp>

#include <iostream>

using namespace await;

//////////////////////////////////////////////////////////////////////

void PrintMetrics(executors::pools::fast::PoolMetrics metrics) {
  std::cout << "tasks = " << metrics.tasks << std::endl;
  std::cout << "tasks_lifo = " << metrics.tasks_lifo << std::endl;
  std::cout << "tasks_local = " << metrics.tasks_local << std::endl;
  std::cout << "tasks_stolen = " << metrics.tasks_stolen << std::endl;
  std::cout << "tasks_grabbed = " << metrics.tasks_grabbed << std::endl;
  std::cout << "steals = " << metrics.steals << std::endl;
  std::cout << "grabs = " << metrics.global_grabs << std::endl;
  std::cout << "parks = " << metrics.parks << std::endl;
  std::cout << "wakes = " << metrics.wakes << std::endl;
}

void WorkLoad() {
  wheels::StopWatch sw;

  static const size_t kThreads = 4;

  executors::fibers::Pool pool{kThreads};

  static const size_t kPipelines = 100500;

  for (size_t j = 0; j < kThreads; ++j) {
    futures::Execute(pool, []() {
      for (size_t i = 0; i < kPipelines; ++i) {
        futures::Spawn([] {})
              | futures::Map([] {})
              | futures::Yield()
              | futures::Map([] {})
              | futures::Yield()
              | futures::Map([] {})
              | futures::Start()
              | futures::Go();

        if (i % 1024 == 0) {
          fibers::Yield();
        }
      }
    }) | futures::Go();
  }

  pool.WaitIdle();

  pool.Stop();

  const auto elapsed = sw.Elapsed();

  std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count() << "ms " << std::endl;

  PrintMetrics(pool.GetMetrics());
}

int main() {
  while (true) {
    WorkLoad();
  }
  return 0;
}
