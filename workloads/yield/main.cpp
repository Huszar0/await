#include <await/executors/impl/fibers/pool.hpp>
#include <await/executors/impl/pools/fast/task_counters/nop.hpp>

#include <await/fibers/sched/yield.hpp>

#include <await/futures/make/execute.hpp>

#include <await/await.hpp>

#include <wheels/core/stop_watch.hpp>

#include <iostream>

using namespace await;

int main() {
  executors::fibers::Pool pool{/*threads=*/1};

  pool.SetTaskCounter(executors::pools::fast::NopCounter());

  while (true) {
    wheels::StopWatch stop_watch;

    auto f = futures::Execute(pool, []() {
      for (size_t i = 0; i < 1'000'000; ++i) {
        fibers::YieldFuture();
      }
    });

    Await(std::move(f)).ExpectOk();

    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(stop_watch.Elapsed());

    std::cout << ms.count() << std::endl;
  }

  pool.Stop();

  return 0;
}
