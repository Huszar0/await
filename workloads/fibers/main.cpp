#include <await/executors/impl/pools/fast/thread_pool.hpp>
#include <await/executors/impl/pools/compute/thread_pool.hpp>

#include <await/fibers/boot/go.hpp>

#include <await/fibers/sched/yield.hpp>

#include <await/fibers/sync/mutex.hpp>
#include <await/fibers/sync/wait_group.hpp>
#include <await/fibers/sync/channel.hpp>
#include <await/fibers/sync/select.hpp>

#include <wheels/core/stop_watch.hpp>

#include <iostream>

using namespace await;

//////////////////////////////////////////////////////////////////////

void WorkLoadChannels() {
  fibers::Channel<int> xs{7};
  fibers::Channel<int> ys{9};

  std::atomic<size_t> produced{0};
  std::atomic<size_t> consumed{0};

  fibers::WaitGroup wg;

  for (size_t k = 0; k < 50; ++k) {
    wg.Add(2);

    const size_t kSends = 100'500;

    // Producer
    fibers::Go([xs, ys, &produced, &wg]() mutable {
      for (size_t i = 0; i < kSends; ++i) {
        if (i % 2 == 0) {
          xs.Send(i);
        } else {
          ys.Send(i);
        }
        produced += i;
      }
      wg.Done();
    });

    // Consumer
    fibers::Go([xs, ys, &consumed, &wg]() mutable {
      for (size_t i = 0; i < kSends; ++i) {
        auto selected = fibers::Select(xs, ys);
        ;
        if (selected.index() == 0) {
          consumed += std::get<0>(selected);
        } else {
          consumed += std::get<1>(selected);
        }
      }
      wg.Done();
    });
  }

  wg.Wait();

  std::cout << produced << std::endl;
  std::cout << consumed << std::endl;
}

//////////////////////////////////////////////////////////////////////

void WorkLoadYield() {
  fibers::WaitGroup wg;

  std::atomic<size_t> tasks = 0;

  wg.Add(100'000);
  for (size_t i = 0; i < 100'000; ++i) {
    fibers::Go([&]() {
      for (size_t j = 0; j < 10; ++j) {
        fibers::Yield();
        ++tasks;
      }
      wg.Done();
    });
  }

  wg.Wait();

  std::cout << tasks << std::endl;
}

//////////////////////////////////////////////////////////////////////

void WorkLoadMutex() {
  for (size_t k = 0; k < 13; ++k) {
    fibers::Go([&]() {
      fibers::WaitGroup wg;

      size_t cs1 = 0;
      fibers::Mutex mutex1;

      size_t cs2 = 0;
      fibers::Mutex mutex2;

      wg.Add(100);

      for (size_t i = 0; i < 100; ++i) {

        fibers::Go([&]() {
          for (size_t j = 0; j < 65536; ++j) {
            {
              std::lock_guard g(mutex1);
              ++cs1;
            }
            if (j % 17 == 0) {
              fibers::Yield();
            }
            {
              std::lock_guard g(mutex2);
              ++cs2;
            }
          }

          wg.Done();
        });
      }

      wg.Wait();
    });
  }
}

//////////////////////////////////////////////////////////////////////

void PrintMetrics(executors::pools::fast::PoolMetrics metrics) {
  std::cout << "tasks = " << metrics.tasks << std::endl;
  std::cout << "tasks_lifo = " << metrics.tasks_lifo << std::endl;
  std::cout << "tasks_local = " << metrics.tasks_local << std::endl;
  std::cout << "tasks_stolen = " << metrics.tasks_stolen << std::endl;
  std::cout << "tasks_grabbed = " << metrics.tasks_grabbed << std::endl;
  std::cout << "steals = " << metrics.steals << std::endl;
  std::cout << "grabs = " << metrics.global_grabs << std::endl;
  std::cout << "parks = " << metrics.parks << std::endl;
  std::cout << "wakes = " << metrics.wakes << std::endl;
}

void WorkLoad() {
  wheels::StopWatch sw;

  executors::pools::fast::ThreadPool scheduler{4};

  fibers::Go(scheduler, []() {
    WorkLoadChannels();
  });

  scheduler.WaitIdle();
  scheduler.Stop();

  const auto elapsed = sw.Elapsed();

  std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count() << "ms " << std::endl;

  //PrintMetrics(scheduler.GetMetrics());
}

int main() {
  while (true) {
    WorkLoad();
  }
  return 0;
}
