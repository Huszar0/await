#include <wheels/test/test_framework.hpp>

#include "../event/event.hpp"

#include <await/futures/combine/seq/map.hpp>

#include <await/futures/syntax/bang.hpp>

#include <await/await.hpp>

#include <thread>
#include <chrono>

using namespace std::chrono_literals;
using await::futures::Map;

TEST_SUITE(AsyncEvent) {
  SIMPLE_TEST(JustWorks) {
    tutorial::AsyncEvent event;

    auto f = !event.Wait();

    ASSERT_FALSE(f.HasResult());

    event.Fire();

    ASSERT_TRUE(f.HasResult());
  }

  SIMPLE_TEST(Async) {
    tutorial::AsyncEvent event;

    std::thread producer([&event]() {
      std::this_thread::sleep_for(1s);
      event.Fire();
    });

    auto done = false;

    auto f = event.Wait() | Map([&](wheels::Unit) {
      done = true;
    });

    await::Await(std::move(f)).ExpectOk();

    ASSERT_TRUE(done);

    producer.join();
  }
}

RUN_ALL_TESTS()
