#include <twist/test/test.hpp>

#include <twist/ed/stdlike/thread.hpp>

#include <await/await.hpp>

#include "../event/event.hpp"

//////////////////////////////////////////////////////////////////////

TEST_SUITE(AsyncEvent) {
  TWIST_TEST_TL(Stress, 5s) {
    while (twist::test::KeepRunning()) {
      int data = 0;
      tutorial::AsyncEvent ready;

      twist::ed::stdlike::thread producer([&]() {
        data = 1;
        ready.Fire();
      });

      await::Await(ready.Wait()).ExpectOk();
      ASSERT_EQ(data, 1);

      producer.join();
    }
  }
}

//////////////////////////////////////////////////////////////////////

RUN_ALL_TESTS()
