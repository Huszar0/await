#pragma once

#include <await/futures/types/future.hpp>

// Defaults
#include <await/executors/impl/inline.hpp>
#include <await/executors/core/hint.hpp>
#include <carry/empty.hpp>

// Mutual exclusion
#include <twist/ed/stdlike/mutex.hpp>

#include <fallible/result/make.hpp>

// Unit type
#include <wheels/core/unit.hpp>

namespace tutorial {

// Asynchronous one-shot event with a single waiter

// Implementation is intentionally suboptimal
// (mutex can be replaced with single atomic & wait-free state machine)
// in favour of simplicity

class AsyncEvent {

  // Future in Await is fundamentally a thunk

  // Thunk is an object representing an (asynchronous) operation
  // that has not been started yet.

  // More on thunks: https://wiki.haskell.org/Thunk

  // Thunk should implement await::futures::lazy::Thunk concept
  // See await/futures/impl/lazy/thunk.hpp

  // Thunk
  class Waiter {
   public:
    // Declare Future value type
    using ValueType = wheels::Unit;

   private:
    // Type alias for a Future consumer
    using IConsumer = await::futures::IConsumer<ValueType>;

   public:
    explicit Waiter(AsyncEvent* event)
        : event_(event) {
      ImplementsThunk();
    }

    // Lazy protocol: Start

    // Starts the asynchronous operation (= waiting on AsyncEvent)
    // Initiated by {fibers,threads}::Await algorithm

    // Asynchronous operation should eventually complete the Future by
    // * either producing a result (via IConsumer::Consume)
    // * or propagating cancellation (via IConsumer::Cancel)
    // to the provided consumer

    // IConsumer represents blocked thread, suspended fiber or asynchronous Future mapper
    // See await/futures/impl/consume/consumer.hpp

    void Start(IConsumer* consumer) {
      consumer_ = consumer;
      event_->Set(/*waiter=*/this);
    }

    // Execution context for continuation
    // (fiber, thread or asynchronous mapper)

    static await::executors::IExecutor& Executor() {
      // Default executor
      return await::executors::Inline();
    }

    static await::executors::SchedulerHint SchedulerHint() {
      // Default scheduling hint
      return await::executors::SchedulerHint::UpToYou;
    }

    static carry::Context UserContext() {
      // Default context
      return carry::Empty();
    }

    // Resume waiter

    void Resume() {
      // Produce result with a Unit value (fallible::Status)
      consumer_->Consume(fallible::Ok());
    }

   private:
    static void ImplementsThunk() {
      // Checks at compile time that Waiter class implements Thunk concept
      static_assert(await::futures::lazy::Thunk<Waiter>);
    }

   private:
    AsyncEvent* event_;
    IConsumer* consumer_;
  };

 public:
  // Concrete Future type produced by AsyncEvent::Wait
  using Future = Waiter;

  // Asynchronous
  Future Wait() {
    return Waiter{this};
  }

  // NB: Fire and Set(Waiter*) methods can be invoked concurrently
  // from different threads

  void Fire() {
    Locker locker(mutex_);

    // Critical section
    fired_ = true;
    Rendezvous(std::move(locker));
  }

 private:
  // Drop-in replacement for std::mutex, allows extensive testing
  using Mutex = twist::ed::stdlike::mutex;
  // RAII guard for Mutex
  using Locker = std::unique_lock<Mutex>;

 private:
  // threads::Await(AsyncEvent::Future)
  // -> AsyncEvent::Waiter::Start(thread consumer)
  // -> AsyncEvent::Set(waiter)

  // NB: Fire and Set(Waiter*) methods can be invoked concurrently
  // from different threads

  void Set(Waiter* waiter) {
    Locker locker(mutex_);

    // Critical section
    waiter_ = waiter;
    Rendezvous(std::move(locker));
  }

  // Producer (Fire) / Consumer (Wait) rendezvous
  void Rendezvous(Locker locker) {
    if (fired_ && (waiter_ != nullptr)) {
      locker.unlock();

      // Complete future
      waiter_->Resume();
    }
  }

 private:
  Mutex mutex_;
  // State guarded by mutex
  bool fired_ = false;
  Waiter* waiter_ = nullptr;
};

}  // namespace tutorial
