#include "../event/event.hpp"

#include <await/await.hpp>

#include <await/futures/combine/seq/and_then.hpp>

#include <iostream>
#include <thread>

using namespace await;
using namespace std::chrono_literals;

int main() {
  tutorial::AsyncEvent event;

  // Consumer

  auto f = event.Wait() | futures::AndThen([](wheels::Unit /*fired*/) {
    std::cout << "Fired" << std::endl;
  });

  // Asynchronous producer

  // Fire the event asynchronously from a separate thread
  // after a 3s delay
  std::thread producer([&event]() {
    std::this_thread::sleep_for(3s);

    std::cout << "Fire" << std::endl;
    event.Fire();
  });

  std::cout << "Await" << std::endl;

  // Start waiting on the event

  // Await(AsyncEvent::Future) ->
  // AsyncEvent::Waiter::Start(thread consumer) ->
  // AsyncEvent::Set(Waiter*)

  Await(std::move(f)).ExpectOk();

  producer.join();

  return 0;
}
